﻿execute spWFLib_AddSymbolicText '88ea433f-8bdb-4833-bc94-55bcf34b5318', 'I4SCADA_Signal_write_Signal_is_write_protected', 'Signal ist schreibgeschützt', 'Signal is write protected'
execute spWFLib_AddSymbolicText '0f37dd90-7706-447a-b2fe-25773363553f', 'I4SCADA_Signal_write_Unspecified_server_error', 'Serverfehler', 'Unspecified server error'
execute spWFLib_AddSymbolicText '8468be69-a838-45cc-8212-b05110050c34', 'I4SCADA_Signal_write_Insufficient_user_authorizations', 'Unzureichende Benutzerrechte', 'Insufficient user authorizations'
execute spWFLib_AddSymbolicText 'd2bfb2a1-518f-4dd9-be2d-2185cac13a3f', 'I4SCADA_Signal_write_No_user_logged_in', 'Kein Benutzer angemeldet', 'No user logged in'
