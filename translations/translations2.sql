execute spWFLib_AddSymbolicText '3b337c2e-2948-460c-81a3-2a56ed5365fd', 'I4SCADA_All', 'Alle', 'All'
execute spWFLib_AddSymbolicText 'e5a90ecc-f2f9-49b8-ad97-57def182cef2', 'I4SCADA_Not_acknowledged', 'Nicht quittiert', 'Not acknowledged'
execute spWFLib_AddSymbolicText '064a2960-a5ae-48d1-a607-4c1619649f1b', 'I4SCADA_Active', 'Aktiv', 'Active'
execute spWFLib_AddSymbolicText '2cc6bad2-649b-4a37-b140-f43631a5c3fd', 'I4SCADA_Active_and_or_not_acknowledged', 'Aktiv und / oder nicht quittiert', 'Active and / or not acknowledged'