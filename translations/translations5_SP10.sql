﻿execute spWFLib_AddSymbolicText '59a01248-0ea3-4477-9b62-6de11b0aeb03', 'I4SCADA_Acknowledge_All_Visible_Alarms', 'Alle sichtbaren Alarme quittieren', 'Acknowledge all visible alarms'
execute spWFLib_AddSymbolicText 'd25fd5c2-5245-4ee6-9a17-6a21432918f3', 'I4SCADA_Configuration_Manager', 'Konfigurations-Manager', 'Configuration Manager'
execute spWFLib_AddSymbolicText 'd9b00665-21cf-4cfb-a4c2-dd6b1a620be7', 'I4SCADA_Save', 'Speichern', 'Save'
execute spWFLib_AddSymbolicText 'deb874f2-cdc9-45f0-b310-c5711c96fece', 'I4SCADA_Modify', 'Ändern', 'Modify'
execute spWFLib_AddSymbolicText '77f9857b-5c6d-4ff7-8575-d827448d88ba', 'I4SCADA_Load', 'Laden', 'Load'
execute spWFLib_AddSymbolicText '544c25ed-581b-4c32-aa08-9ae94fa3b629', 'I4SCADA_Add_New', 'Neu hinzufügen', 'Add new'
execute spWFLib_AddSymbolicText 'ae83cd4f-04f0-487a-82eb-226061302b5e', 'I4SCADA_Acknowledge_Comment', 'Quittierkommentar', 'Acknowledge Comment'
execute spWFLib_AddSymbolicText 'd2daeaf8-c0f6-488b-bd65-6d638b479359', 'I4SCADA_Show_owned_configurations_only', 'Nur eigene Konfigurationen anzeigen', 'Show only own configurations'
execute spWFLib_AddSymbolicText 'b76ca4d1-50d1-4fe5-8f11-049b7c4521a8', 'I4SCADA_Delete', 'Löschen', 'Delete'
execute spWFLib_AddSymbolicText '983c0ce9-12ac-442b-bff8-39795ca77ef7', 'I4SCADA_Name', 'Name', 'Name'
execute spWFLib_AddSymbolicText '05f12a90-5a3a-4563-9dfb-1ce4a769defe', 'I4SCADA_Owner', 'Besitzer', 'Owner'
execute spWFLib_AddSymbolicText '8631eb64-3d63-466a-a68e-f5400aeb8814', 'I4SCADA_CreatedOn', 'Erstellt am', 'Created on'
execute spWFLib_AddSymbolicText '8170a947-7d3d-440e-817d-4b0cda6768f3', 'I4SCADA_Namespace', 'Namensraum', 'Namespace'
execute spWFLib_AddSymbolicText '2a8a15d9-b461-4c4a-9daa-591b9d69c346', 'I4SCADA_Yes', 'Ja', 'Yes'
execute spWFLib_AddSymbolicText '51ac0fd5-e55d-4bc8-ae5f-10e7c6faee5d', 'I4SCADA_No', 'Nein', 'No'
execute spWFLib_AddSymbolicText '98566a83-92cd-46e3-8e66-bd2bea26a52a', 'I4SCADA_Warning', 'Warnung', 'Warning'