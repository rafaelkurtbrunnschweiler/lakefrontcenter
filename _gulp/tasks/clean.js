// ReSharper disable once InconsistentNaming
var gulp = require("gulp");
/*
 * Builds the CSS, Font and JavaScript files for the site.
 */
gulp.task("clean", ["clean-app-bundles", "clean-content-bundles", "clean-script-bundles", "clean-index","clean-ts"]);
//# sourceMappingURL=clean.js.map