﻿ko.bindingHandlers.fullScreenByClick = {
	init: function (element) {

		if (fullScreenApi.supportsFullScreen) {
			element.addEventListener('click', function () {
				fullScreenApi.requestFullScreen(document.documentElement);
			}, true);
		}
	},
};