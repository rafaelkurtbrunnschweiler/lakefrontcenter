﻿var JsonNetDecycle = (function () {
    function JsonNetDecycle() { }
    JsonNetDecycle.decycle = function decycle(obj) {
        var catalog = [];
        var newObj = JsonNetDecycle.getDecycledCopy(obj, catalog);
        return newObj;
    }
    JsonNetDecycle.getDecycledCopy = function getDecycledCopy(obj, catalog) {
        var i;
        var name;
        var nu;
        switch (typeof obj) {
            case 'object': {
                if (obj === null || obj instanceof Boolean || obj instanceof Date || obj instanceof Number || obj instanceof RegExp || obj instanceof String) {
                    return obj;
                }
                for (i = 0; i < catalog.length; i += 1) {
                    if (catalog[i] === obj) {
                        return {
                            $ref: i.toString()
                        };
                    }
                }
                obj.$id = catalog.length.toString();
                catalog.push(obj);
                if (Object.prototype.toString.apply(obj) === '[object Array]') {
                    nu = [];
                    for (i = 0; i < obj.length; i += 1) {
                        nu[i] = JsonNetDecycle.getDecycledCopy(obj[i], catalog);
                    }
                } else {
                    nu = {
                    };
                    for (name in obj) {
                        if (Object.prototype.hasOwnProperty.call(obj, name)) {
                            nu[name] = JsonNetDecycle.getDecycledCopy(obj[name], catalog);
                        }
                    }
                }
                return nu;

            }
            case 'number':
            case 'string':
            case 'boolean': {
                return obj;

            }
        }
    }
    JsonNetDecycle.retrocycle = function retrocycle(obj) {
        var catalog = [];
        JsonNetDecycle.findReferences(obj, catalog);
        JsonNetDecycle.resolveReferences(obj, catalog);
    }
    JsonNetDecycle.findReferences = function findReferences(obj, catalog) {
        var i;
        if (obj && typeof obj === 'object') {
            var id = obj.$id;
            if (typeof id === 'string') {
                catalog[id] = obj;
            }
            if (Object.prototype.toString.apply(obj) === '[object Array]') {
                for (i = 0; i < obj.length; i += 1) {
                    JsonNetDecycle.findReferences(obj[i], catalog);
                }
            } else {
                for (name in obj) {
                    if (typeof obj[name] === 'object') {
                        JsonNetDecycle.findReferences(obj[name], catalog);
                    }
                }
            }
        }
    }
    JsonNetDecycle.resolveReferences = function resolveReferences(obj, catalog) {
        var i, item, name, id;
        if (obj && typeof obj === 'object') {
            if (Object.prototype.toString.apply(obj) === '[object Array]') {
                for (i = 0; i < obj.length; i += 1) {
                    item = obj[i];
                    if (item && typeof item === 'object') {
                        id = item.$ref;
                        if (typeof id === 'string') {
                            obj[i] = catalog[id];
                        } else {
                            JsonNetDecycle.resolveReferences(item, catalog);
                        }
                    }
                }
            } else {
                for (name in obj) {
                    if (typeof obj[name] === 'object') {
                        item = obj[name];
                        if (item) {
                            id = item.$ref;
                            if (typeof id === 'string') {
                                obj[name] = catalog[id];
                            } else {
                                JsonNetDecycle.resolveReferences(item, catalog);
                            }
                        }
                    }
                }
            }
        }
    }
    return JsonNetDecycle;
})();
//@ sourceMappingURL=JsonNetDecycle.js.map