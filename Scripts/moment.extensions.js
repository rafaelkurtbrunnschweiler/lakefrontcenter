﻿(function (moment) {
    moment.fn.toDateTimeOffset = function () {
        var clone = this.clone().utc();

        var dto = {
            dateTime: clone,
            offsetMinutes: this.utcOffset()  /* Offset 60 minutes added by AH */
        };

        return dto;
    };

    moment.fn.toMSDateTimeOffset = function () {
        var dateTimeOffset = this.toDateTimeOffset();

        var dto = {
            DateTime: dateTimeOffset.dateTime.toMSDate(),
            OffsetMinutes: dateTimeOffset.offsetMinutes
        };

        return dto;
    };

    moment.fn.toMSDate = function () {
        return '/Date(' + this.toDate().getTime() + ')/';
    };
})(moment);
