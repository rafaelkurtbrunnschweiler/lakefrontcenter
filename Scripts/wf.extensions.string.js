String.prototype.format = function () {
    var args = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        args[_i] = arguments[_i];
    }
    return this.replace(/{(\d+)}/g, function (match, placeholder) { return (typeof args[placeholder] != 'undefined' ? args[placeholder] : match); });
};
// Pad a string with defined characters to a certain length 
// For example: var str = "5";
// alert(str.lpad("0", 4)); //result "0005"
String.prototype.lpad = function (padString, length) {
    var str = this;
    while (str.length < length)
        str = padString + str;
    return str;
};
String.prototype.stringPlaceholderResolver = function (OIDvalue) {
    var str = ko.unwrap(this);
    if (str === undefined || str === null)
        str = "";
    if (ko.unwrap(OIDvalue))
        str = str.split('[OID]').join(ko.unwrap(OIDvalue));
    return str.valueOf();
};
//# sourceMappingURL=wf.extensions.string.js.map