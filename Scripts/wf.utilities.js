var wf = wf || {};
wf.utilities = (function () {
    String.prototype.replaceAll = function (search, replace) {
        if (replace === undefined) {
            return this.toString();
        }
        return this.split(search).join(replace);
    }

    function ResolveStringPlaceholders(url, content) {
        var searchString = url.split("?")[1];
        var $wrap = $("<div/>").html(content);
        var $hackDiv = $wrap.find("#defaultNavigationParameters");
        var hackDiv = $hackDiv.length > 0 ? $hackDiv[0] : null;

        return resolvePlaceholders(hackDiv, searchString, content);
    }

    function resolvePlaceholders(hackDiv, searchString, content, beforApplyCallback, afterApplyCallback) {
        var defNavParam = null;
        var property;

        if (hackDiv)
            try {
                defNavParam = $.parseJSON(hackDiv.textContent);
            } catch (e) {
                defNavParam = null;
            }

        //if exist parameters in url or default
        if (searchString && searchString.length || defNavParam) {
            var variableArray = searchString.split('&');

            //create object from url parameters
            var navParam = {};
            for (var i = 0; i < variableArray.length; i++) {
                var keyValuePair = variableArray[i].split('=');
                if (!keyValuePair[0].trim())
                    continue;

                keyValuePair[0] = decodeURIComponent(keyValuePair[0]);
                keyValuePair[1] = decodeURIComponent(keyValuePair[1]);
                navParam[keyValuePair[0]] = keyValuePair[1];
            }

            //if there is not default parametersin url
            for (property in defNavParam) {
                if (defNavParam.hasOwnProperty(property)) {
                    if (!navParam[property])
                        navParam[property] = defNavParam[property];
                }
            }

            if (Object.keys(navParam).length > 0) {
                if (beforApplyCallback && typeof beforApplyCallback === "function")
                    beforApplyCallback();

                for (property in navParam) {
                    if (navParam.hasOwnProperty(property)) {
                        content = content.replaceAll('[PC:' + property + ']', navParam[property]);
                    }
                }

                if (afterApplyCallback && typeof afterApplyCallback === "function")
                    afterApplyCallback();
            }
        }

        return content;
    }

    function ResolvePagePlaceholders(animatedClass, animatedClassApplyDuration) {
        var searchString = window.location.search.substring(1);
        var hackDiv = document.getElementById("defaultNavigationParameters"); //search special div with id. The Value should be JSON with default parameters. {"ParameterName":"ParameterValue"}
        var beforeApply = function () {
            if (animatedClass)
                $(document.body).addClass(animatedClass);
        }

        var afterApply = function() {
            if (animatedClass && animatedClassApplyDuration)
                var timer = setTimeout(function() {
                        $(document.body).removeClass(animatedClass);
                        clearTimeout(timer);
                    },
                    animatedClassApplyDuration);
        }

        document.body.innerHTML = resolvePlaceholders(hackDiv, searchString, document.body.innerHTML, beforeApply, afterApply);
    }

    function InitilizeConstants(version, remoteIISServerUrl) {
        var urlPath = window.document.location.href;
        var remoteIISServer = remoteIISServerUrl !== undefined && remoteIISServerUrl
                                  ? remoteIISServerUrl
                                  : window.document.location.hostname;

        //var remoteIISServer = "localhost"; 

        var lowerUrlPath = urlPath.toLowerCase();
        if (_.str.endsWith(lowerUrlPath, ".html")) {
            urlPath = urlPath.split("/").slice(0, -1).join("/");
        }

        if (!_.str.endsWith(urlPath, "/")) {
            urlPath += "/";
        }

        window.rootUrl = urlPath;
        window.rootUrlPrefix = window.document.location.origin.replace(window.document.location.hostname, remoteIISServer);
        window.appVersion = "{{appVersion}}".replace("-", ".");

        window.defaultTimeZone = "W. Europe Standard Time";

        window.resolveUrl = function (rootedUrl) {

            if (!rootedUrl) {
                return rootedUrl;
            }

            //  ~/_Services/WebServices/WCF/AlarmsService.svc
            if (rootedUrl.substring(0, 2) === "~/") {
                return rootedUrl.replace("~/", window.rootUrlPrefix + "/");
            }

            //  /_Services/WebServices/WCF/AlarmsService.svc
            if (rootedUrl.substring(0, 1) === "/") {
                return window.rootUrlPrefix + rootedUrl;
            }

            //  http://localhost/_Services/WebServices/WCF/AlarmsService.svc
            return rootedUrl.replace(window.document.location.hostName, remoteIISServer);
        }
    }

    function InitilizeModalLoading() {

        document.body.insertAdjacentHTML('beforeend',
            "<div id='wfModal' class='modal fade' style='display: none;'> <div class='modal-dialog'> <div class='modal-content' style='overflow-x: hidden;'> <div class='modal-header'> <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button> <div class='modal-title'>Test draggable</div> </div> <div class='modal-body p-a-0'></div> <div class='modal-footer'> <button type='button' class='btn btn-primary' data-dismiss='modal'> <i class='wf wf-small-x-round-o m-r-sm'></i> <wf-symbolictext params='symbolicText: \"I4SCADA_Close\"'></wf-symbolictext> </button> </div> </div> </div> </div>");

        $(document).ready(function () {
            $('#wfModal')
                .on('hidden.bs.modal',
                    function (event) {
                        var modal = $(this);
                        var modalBodyContent = $('#wf-modal-content')[0];

                        // Clean Knockput bindings and clear modal dialog body container
                        ko.cleanNode(modalBodyContent);
                        modal.find('.modal-body').empty();

                        // Remove the added class names on the modal dialog container  beside the default name
                        modal.find('.modal-dialog').removeClass().addClass('modal-dialog');
                        modal.find('.modal-header').removeClass().addClass('modal-header');
                    });


            $('#wfModal')
                .on('show.bs.modal',
                    function (event) {
                        var modal = $(this);
                        var button = $(event.relatedTarget); // Element that triggered the modal
                        var src = button.data('src'); // Extract info from data-* attributes
                        var placement = button.data('placement') || "center";
                        var bodyHeight = button.data('height');
                        var bodyWidth = button.data('width');
                        var title = button.data('title') || "";
                        var className = button.data('class') + " modal-" + placement;
                        var headerClass = button.data('header-class');

                        //modal.find('.modal-dialog').removeClass('modal-md modal-sm modal-lg').addClass(size);

                        modal.find('.modal-dialog').addClass(className);

                        if (title === "") {
                            modal.find('.modal-header').hide();
                        } else {
                            var $newWidget = $('<wf-symbolictext params="symbolicText:\'' + title + '\'"></wf-symbolictext>');
                            modal.find('.modal-title').html("").append($newWidget);

                            //Apply bindings 
                            ko.applyBindings({}, $newWidget[0]);

                            modal.find('.modal-header').addClass(headerClass);
                            modal.find('.modal-header').show();
                        }

                        modal.find('.modal-dialog').css("width", bodyWidth);
                        modal.find('.modal-body').css("height", bodyHeight);
                        modal.find('.modal-body').css("width", bodyWidth);


                        $.get(src, function (responseText) {
                            var $modalBody = modal.find('.modal-body');

                            // Resolve parameter placeholders in loaded html
                            responseText = wf.utilities.ResolveStringPlaceholders(src, responseText);
                            $modalBody.append('<div id="wf-modal-content" class="stretched overflow-auto"></div>');

                            var $modalBodyContent = modal.find('#wf-modal-content');

                            // ToDo: Stip the HTML in order to inject only the content of the body and CSS references
                            // console.log(responseText);
                            $modalBodyContent.html(responseText);

                            // Apply bindings in injected html
                            ko.applyBindings({}, $modalBodyContent[0]);
                        });
                    });
        });
    }

    function initializeConstants(version, remoteIISServerUrl) {
        InitilizeConstants(version, remoteIISServerUrl);
    }

    function initializeModalLoading() {
        InitilizeModalLoading();
    }

    return {
        ResolveStringPlaceholders: ResolveStringPlaceholders,
        ResolvePagePlaceholders: ResolvePagePlaceholders,
        initializeConstants: initializeConstants,
        initializeModalLoading: initializeModalLoading,
        //legacy
        InitilizeConstants: InitilizeConstants,
        InitilizeModalLoading: InitilizeModalLoading
    }
}());