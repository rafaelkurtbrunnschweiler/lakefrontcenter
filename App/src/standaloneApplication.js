define(["require", "exports", "./services/logger", "./componentRegistry", "./customComponentRegistry"], function (require, exports, Logger, ComponentRegistry, CustomComponentRegistry) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var StandaloneApplication = /** @class */ (function () {
        function StandaloneApplication() {
            this.setupLogging();
            Logger.info(this, "Application starting...");
            Logger.info(this, "Framework version " + window.appVersion);
            if (!this.checkProtocol()) {
                Logger.error(this, "Application was started outside the webserver. Please publish the application and run it from the server.");
                return;
            }
            this.registerComponents();
            CustomComponentRegistry.register();
            ko.applyBindings(this);
        }
        StandaloneApplication.prototype.setupLogging = function () {
            Q.longStackSupport = true;
            toastr.options.toastClass = 'toast';
            toastr.options.positionClass = 'toast-bottom-right';
            toastr.options.backgroundpositionClass = 'toast-bottom-right';
            Logger.info(this, "Logging support enabled: {0}", window.debug);
        };
        StandaloneApplication.prototype.checkProtocol = function () {
            Logger.info(this, 'Check used Protocol');
            switch (window.location.protocol) {
                case 'http:':
                case 'https:':
                    return true;
                case 'file:':
                    return false;
                default:
                    return false;
            }
        };
        StandaloneApplication.prototype.registerComponents = function () {
            return new ComponentRegistry("src/components")
                .mapComponent("wf-secured-container")
                .mapComponent("wf-value")
                .mapComponent("wf-value-display")
                .mapComponent("wf-gauge-1")
                .mapComponent("wf-arc")
                .mapComponent("wf-chart-1")
                .mapComponent("wf-log-table")
                .mapComponent("wf-bargraph")
                .mapComponent("wf-signal-information")
                .mapComponent("wf-state-text")
                .mapComponent("wf-state-indicator")
                .mapComponent("wf-state-symbol")
                .mapComponent("wf-input")
                .mapComponent("wf-button")
                .mapComponent("wf-switch")
                .mapComponent("wf-combobox")
                .mapComponent("wf-slider")
                .mapComponent("wf-symbolictext")
                .mapComponent("wf-language-selector")
                .mapComponent("wf-user-login")
                .mapComponent("wf-alarm-viewer")
                .mapComponent("wf-watchdog")
                .mapComponent("wf-alarm-ack-button")
                .mapComponent("wf-logbook")
                .mapComponent("wf-sensor")
                .mapComponent("wf-user-information")
                .mapComponent("wf-switch-3-states")
                .mapComponent("wf-logbook-viewer")
                .mapComponent("wf-signal-list")
                .mapComponent("wf-configuration")
                .mapComponent("wf-signal-browser")
                .mapComponent("wf-meter")
                .mapComponent("wf-logtag-analytics")
                .mapComponent("wf-visibility-container")
                .mapComponent("wf-rotation-container")
                .mapComponent("wf-enable-container")
                .mapComponent("wf-buffer-button")
                .mapComponent("wf-toggle-button")
                .mapComponent("wf-signals-buffer-viewer")
                .mapComponent("wf-write-secure-signal")
                .mapComponent("wf-radio-buttons")
                .mapComponent("wf-modal-dialog");
        };
        return StandaloneApplication;
    }());
    exports.StandaloneApplication = StandaloneApplication;
    exports.application = new StandaloneApplication();
});
//# sourceMappingURL=standaloneApplication.js.map