define(["require", "exports", "durandal/system", "./services/logger", "durandal/app", "durandal/viewLocator", "plugins/router", "./services/portableConsole", "./pluginRegistry", "./customComponentRegistry", "./customWidgetRegistry"], function (require, exports, system, Logger, app, viewLocator, router, PortableConsole, PluginRegistry, CustomComponentRegistry, CustomWidgetRegistry) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var SpaApplication = /** @class */ (function () {
        function SpaApplication() {
            var _this = this;
            this.setupLogging();
            Logger.info(this, 'Application starting...');
            Logger.info(this, "Framework version " + window.appVersion);
            if (!this.checkProtocol()) {
                Logger.error(this, "Application was started outside the webserver. Please publish the application and run it from the server.");
                return;
            }
            this.setupAsyncSupport();
            this.setupAppTitle();
            this.configurePlugins();
            this.registerComponents();
            CustomComponentRegistry.register();
            Q.all([])
                .then(function () {
                return app.start().then(function () {
                    Logger.info(_this, "Application started");
                    viewLocator.useConvention();
                    return _this.loadShell();
                });
            })
                .fail(Logger.handleError(this))
                .done();
        }
        SpaApplication.prototype.setupLogging = function () {
            //system.debug(window.debug);
            //if (window.debug) {
            var dynamicSystem = system;
            dynamicSystem.log = PortableConsole.info;
            dynamicSystem.warn = PortableConsole.warn;
            dynamicSystem.error = PortableConsole.error;
            Q.longStackSupport = true;
            //}
            toastr.options.toastClass = 'toast'; //hidden-xs
            toastr.options.positionClass = 'toast-bottom-right';
            toastr.options.backgroundpositionClass = 'toast-bottom-right';
            Logger.info(this, "Logging support enabled: {0}", window.debug);
        };
        SpaApplication.prototype.checkProtocol = function () {
            Logger.info(this, 'Check used Protocol');
            switch (window.location.protocol) {
                case 'http:':
                case 'https:':
                    return true;
                case 'file:':
                    return false;
                default:
                    return false;
            }
        };
        SpaApplication.prototype.setupAsyncSupport = function () {
            Logger.info(this, "Setting up support for async operations");
            // we need to patch in the Q promises instead of jQuery
            // unfortunately this doesn't work directly with TS so we need to hack it
            var dynamicSystem = system;
            dynamicSystem.defer = function (action) {
                var deferred = Q.defer();
                action.call(deferred, deferred);
                var promise = deferred.promise;
                deferred.promise = function () { return promise; };
                return deferred;
            };
            Q.longStackSupport = true;
        };
        SpaApplication.prototype.setupAppTitle = function () {
            app.title = null;
            router.updateDocumentTitle = function () {
            };
        };
        SpaApplication.prototype.configurePlugins = function () {
            Logger.info(this, "Configuring framework plugins");
            var standardWidgets = this.registerStandardWidgets();
            var customWidgets = CustomWidgetRegistry.register();
            var allWidgets = standardWidgets.concat(customWidgets);
            app.configurePlugins({
                router: true,
                dialog: true,
                widget: {
                    kinds: allWidgets
                }
            });
            var dynRouter = router;
            dynRouter.handleInvalidRoute = function (route) {
                Logger.error(router, "Route '{0}' not found", null, route);
            };
        };
        SpaApplication.prototype.registerStandardWidgets = function () {
            return new PluginRegistry("src/widgets")
                .map("wfValue")
                .map("wfValueDisplay")
                .map("wfValueBarGraph")
                .map("wfValueGauge")
                .map("wfValueArc")
                .map("wfSignalInformation")
                .map("wfInputValue")
                .map("wfSwitchValue")
                .map("wfSwitchValue3States")
                .map("wfWriteValueButton")
                .map("wfWriteValueCombobox")
                .map("wfStateIndicator")
                .map("wfStateCssClass")
                .map("wfSensorValue")
                .map("wfStateText")
                .map("wfStateDisplay")
                .map("wfLanguageDropdown")
                .map("wfSymbolicText")
                .map("wfLogTagTrend")
                .map("wfLogTagTable")
                .map("wfUserLogin")
                .map("wfUserAuthorizationsList")
                .map("wfAckAlarmButton")
                .map("wfAlarmViewer")
                .map("tmplAlarmList")
                .map("tmplAlarmTable")
                .map("wfSecuredContainer")
                .map("wfSlider")
                .map("wfWatchdog")
                .map("demoSignalWriteButtonGroup2")
                .map("demoProductionUnitInformation")
                .map("demoLightControl")
                .map("demoWidgetsList")
                .map("wfLogbook")
                .map("wfUserInformation")
                .map("wfLogbookViewer")
                .map("wfDateTimePicker")
                .map("wfMeter")
                .map("wfLogTagAnalytics")
                .map("wfVisibilityContainer")
                .map("wfEnableContainer")
                .map("wfBufferButton")
                .map("wfToggleButton")
                .map("wfSignalsBufferViewer")
                .map("wfRadioButtons")
                .kinds;
            //.map("demoAlarmList")
            //.map("demoAlarmTable")
        };
        SpaApplication.prototype.registerComponents = function () {
            return new PluginRegistry("src/components")
                .mapComponent("wf-secured-container")
                .mapComponent("wf-value")
                .mapComponent("wf-value-display")
                .mapComponent("wf-gauge-1")
                .mapComponent("wf-arc")
                .mapComponent("wf-chart-1")
                .mapComponent("wf-log-table")
                .mapComponent("wf-bargraph")
                .mapComponent("wf-signal-information")
                .mapComponent("wf-state-text")
                .mapComponent("wf-state-indicator")
                .mapComponent("wf-state-symbol")
                .mapComponent("wf-input")
                .mapComponent("wf-button")
                .mapComponent("wf-switch")
                .mapComponent("wf-combobox")
                .mapComponent("wf-slider")
                .mapComponent("wf-symbolictext")
                .mapComponent("wf-language-selector")
                .mapComponent("wf-user-login")
                .mapComponent("wf-alarm-viewer")
                .mapComponent("wf-watchdog")
                .mapComponent("wf-alarm-ack-button")
                .mapComponent("wf-logbook")
                .mapComponent("wf-sensor")
                .mapComponent("wf-user-information")
                .mapComponent("wf-switch-3-states")
                .mapComponent("wf-logbook-viewer")
                .mapComponent("wf-signal-list")
                .mapComponent("wf-configuration")
                .mapComponent("wf-signal-browser")
                .mapComponent("wf-meter")
                .mapComponent("wf-logtag-analytics")
                .mapComponent("wf-visibility-container")
                .mapComponent("wf-enable-container")
                .mapComponent("wf-buffer-button")
                .mapComponent("wf-toggle-button")
                .mapComponent("wf-signals-buffer-viewer")
                .mapComponent("wf-rotation-container")
                .mapComponent("wf-write-secure-signal")
                .mapComponent("wf-radio-buttons");
        };
        SpaApplication.prototype.loadShell = function () {
            Logger.info(this, "Starting shell");
            app.setRoot('src/viewModels/shell', 'entrance');
        };
        return SpaApplication;
    }());
    exports.SpaApplication = SpaApplication;
    exports.application = new SpaApplication();
});
//# sourceMappingURL=spaApplication.js.map