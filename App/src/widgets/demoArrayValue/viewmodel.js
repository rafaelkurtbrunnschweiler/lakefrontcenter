﻿define(
    ['../../services/connector', "../../services/securedService"],
    function (signalsConnector, securedService) {
        var ctor = function () {
            var self = this;
        };

        ctor.prototype = {
            activate: function (settings) {
                var self = this;
                self.objectID = ko.unwrap(settings.objectID);

                self.labelTexts = ko.observableArray([
                    { label: 'Act Diff Presure', unit: "m WS" },
                    { label: 'Flow Rate', unit: "m³/h" },
                    { label: 'Power Rating', unit: "W" },
                    { label: 'Operation Hours', unit: "h" },
                    { label: 'Speed', unit: "rpm" },
                    { label: 'Temperaure', unit: "°C" },
                    { label: 'Operation Mode', unit: "" },
                    { label: 'Max. Speed', unit: "rpm" },
                    { label: 'Min. Speed', unit: "rpm" }
                ]);

                self.projectAuthorization = (ko.unwrap(settings.projectAuthorization) || "").stringPlaceholderResolver(self.objectID);
                self.securedService = new securedService(self.projectAuthorization);
                self.hasAuthorization = self.securedService.hasAuthorization;

                self.settings = settings;

                self.signalName = (ko.unwrap(self.settings.signalName) || '').stringPlaceholderResolver(self.objectID);
                self.signalValue = "";
                self.valuesArray = ko.observableArray([]);

                // Stop here and return if no signalName was configured
                if (!self.signalName) {
                    return null;
                }

                self.connector = new signalsConnector();
                self.signal = self.connector.getSignal(self.signalName);
                self.signalValue = self.signal.value;

                // Subsribe for signal value updates and split the value string into array items
                self.signalValue.subscribe(function (newValue) {                  
                    var newArray = new Array();
                    var v;

                    newArray = newValue.split(",");                   
                    for (v in newArray) {
                        if (newArray.hasOwnProperty(v)) {
                            // Further manipulation / calculation of value could be done here below
                            newArray[v] = parseInt(newArray[v], 10);
                        }
                    }

                    self.valuesArray(newArray);
                });

                return self.connector.getOnlineUpdates().fail(self.connector.handleError(self));
            },

            detached: function () {
                var self = this;

                if (!self.signal) return null;

                return self.connector.unregisterSignals(self.signal);
            }
        };
        return ctor;
    });