﻿define([],
    function () {
        var ctor = function () {
            var self = this;
            self.defaultSettings = {};
        };

        ctor.prototype = {
            activate: function (settings) {
                var self = this;
                self.settings = settings;
                self.selectAlarm = self.settings.selectAlarm;
                self.dateTimeFormat = self.settings.dateTimeFormat || "";
            },

            isDateField: function (field) {
                return field === 'DateOn' || field === 'DateAck' || field === 'SysTime' || field === 'DateOff';
            }
        };
        return ctor;
    });
