﻿define(['../../services/connector',
        "../../services/models/signalDefinitionsFilter", "../../services/securedService", "../../services/visualSecurityService"],
    function (signalsConnector, signalDefinitionsFilter, securedService, visualSecurityService) {

        var ctor = function () {
            var self = this;
            self.selectedLanguageId = null;
            self.output = "";
        };

        ctor.prototype = {
            activate: function (settings) {
                var self = this;
                self.objectID = ko.unwrap(settings.objectID);
                self.connector = new signalsConnector();

                self.projectAuthorization = (ko.unwrap(settings.projectAuthorization) || "").stringPlaceholderResolver(self.objectID);
                self.securedService = new securedService(self.projectAuthorization);
                self.hasAuthorization = self.securedService.hasAuthorization;

                self.settings = settings;

                self.tooltipText = (ko.unwrap(self.connector.translate(self.settings.tooltipText)()) || "").stringPlaceholderResolver(self.objectID);
                self.signalName = (ko.unwrap(settings.signalName) || 'Unit').stringPlaceholderResolver(self.objectID);
                self.logTagName = (ko.unwrap(self.settings.logTagName) || '').stringPlaceholderResolver(self.objectID);
                // DEPRECATED PROPERTY
                // Property "property" is deprecated. Please use "propertyName" instead
                self.property = ko.unwrap(settings.property) || ko.unwrap(settings.propertyName) || null;

                if (settings.property) {
                    console.warn('Property "property" is deprecated and will be removed in future. Please use "propertyName" instead.');
                }

                self.visualSecurityService = new visualSecurityService(self.settings, self.connector);
                self.visualSecurityService.initialize();
                self.isVisible = self.visualSecurityService.isVisible;

                // Stop processing here if no signalname is defined
                if (!self.signalName) {
                    return;
                }

                self.signalDefinitions = ko.observable({});

                self.selectedLanguageId = self.connector.currentLanguageId;
                self.selectedLanguageId.subscribe(function () {
                    // Reload the signal information on language change
                    self.getSignalDefinition();
                });

                self.output = ko.computed(function () {
                    if (!self.settings.propertyName)
                        return "";

                    //Simple property of SignalDefinitionDTO
                    if (_.indexOf(self.settings.propertyName, '.') === -1)
                        return !isNullOrUndefined(self.signalDefinitions()[self.settings.propertyName]) ? self.signalDefinitions()[self.settings.propertyName] : "";

                    var options = self.settings.propertyName.split(".");
                    var logs = self.signalDefinitions()[options[0]];
                    if (!logs) return "";


                    if (_.isArray(logs)) {//Logs, Array property of SignalDefinitionDTO
                        if (options[0] !== "Logs") return "";

                        if (!self.logTagName)
                            return "";

                        var length = logs.length;
                        for (var j = 0; j < length; j++)
                            if (logs[j]["LogTag"] === self.logTagName)
                                return logs[j][options[1]] || "";
                    }

                    return logs[options[1]] || ""; //DTO property of SignalDefinitionDTO
                });

                // Get signal information
                self.getSignalDefinition();

            },

            getSignalDefinition: function () {
                var self = this;

                self.connector.getSignalsDefinitions([self.signalName])
                .then(function (definitions) {
                    self.signalDefinitions(definitions[0] || {});
                })
                .fail(self.connector.handleError(self));
            },

            detached: function () {
                var self = this;

                if (self.visualSecurityService)
                    self.visualSecurityService.dispose();
            },
        };

        return ctor;
    });