﻿define(
    ["../../services/connector", "../../services/securedService", "../../services/visualSecurityService"],
    function (signalsConnector, securedService, visualSecurityService) {

        var ctor = function () {
            var self = this;
        };

        ctor.prototype = {
            activate: function (settings) {
                var self = this;
                self.connector = new signalsConnector();

                self.settings = settings;

                self.objectID = ko.unwrap(settings.objectID);
                self.projectAuthorization = (ko.unwrap(self.settings.projectAuthorization) || "").stringPlaceholderResolver(self.objectID);
                self.securedService = new securedService(self.projectAuthorization);
                self.hasAuthorization = self.securedService.hasAuthorization;

               self.disableOverlayColor = ko.unwrap(self.settings.disableOverlayColor) || "rgba(255,255,255,.5)";

               self.visualSecurityService = new visualSecurityService(self.settings, self.connector);
               self.visualSecurityService.initialize();
               self.isDisabled = self.visualSecurityService.isDisabled;

                return self.connector.getOnlineUpdates().fail(self.connector.handleError(self));
            },

            detached: function () {
                var self = this;

                if (self.visualSecurityService)
                    self.visualSecurityService.dispose();

                //Skip unregister if no signal was configured
                if (!self.enableSignal)
                    return;

                self.connector.unregisterSignals(self.enableSignal);
            }

        };

        return ctor;
    });