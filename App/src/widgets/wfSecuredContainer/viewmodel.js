﻿define(
    ["../../services/connector", "../../services/securedService"],
    function (signalsConnector, securedService) {

        var ctor = function () {
            var self = this;
        };

        ctor.prototype = {
            activate: function (settings) {
                var self = this;
                var connector = self.connector = new signalsConnector();

                self.objectID = ko.unwrap(settings.objectID);

                self.projectAuthorization = (ko.unwrap(settings.projectAuthorization) || "").stringPlaceholderResolver(self.objectID);
                self.securedService = new securedService(self.projectAuthorization);
                self.hasAuthorization = self.securedService.hasAuthorization;


                //self.currentUserProjectAuthorizations = connector.currentUserProjectAuthorizations;
                ////self.currentUserSystemAuthorizations = connector.currentUserSystemAuthorizations;

                //self.loggedInUserName = connector.currentLoggedInUser;

                //self.hasAuthorization = ko.pureComputed(function () {
                //    return _.pluck(self.currentUserProjectAuthorizations(), "Name").indexOf(self.projectAuthorization) !== -1;
                //});

                //return connector.getCurrentLoggedInUser()
                //   .then(function () {
                //       return connector.getCurrentUserAuthorizations();
                //   })
                //   .fail(self.connector.handleError(self));
            }         
        };

        return ctor;
    });