﻿define(["../../services/connector"],
    function (connector) {
        var ctor = function () {
            var self = this;
            self.defaultSettings = {};
        };

        ctor.prototype = {
            activate: function (settings) {
                var self = this;
                self.connector = new connector();
                self.settings = settings;
                self.selectAlarm = self.settings.selectAlarm;
                self.dateTimeFormat = self.settings.dateTimeFormat || "";

                self.isShowAlarmTypeSymbolicTextTranslation = ko.computed(function () {
                    return self.settings.fields.data.indexOf('AlarmTypeSymbolicTextTranslation') > -1
                }, self);

                self.isShowPriority = ko.computed(function () {
                    return self.settings.fields.data.indexOf('Priority') > -1
                }, self);
            },

            isDateField: function (field) {
                return field === 'DateOn' || field === 'DateAck' || field === 'SysTime' || field === 'DateOff';
            }
        };
        return ctor;
    });
