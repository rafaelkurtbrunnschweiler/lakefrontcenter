﻿define(
    ["../../services/connector", "../../services/securedService"],
    function (signalsConnector, securedService) {

        var ctor = function () {
            var self = this;
        };

        ctor.prototype = {
            activate: function (settings) {
                var self = this;
                self.objectID = ko.unwrap(settings.objectID);

                self.projectAuthorization = (ko.unwrap(settings.projectAuthorization) || "").stringPlaceholderResolver(self.objectID);
                self.securedService = new securedService(self.projectAuthorization);
                self.hasAuthorization = self.securedService.hasAuthorization;

                var connector = self.connector = new signalsConnector();


                self.listClass = ko.unwrap(settings.listClass) || "list-group";
                self.listItemClass = ko.unwrap(settings.listItemClass) || "list-group-item";

                self.showSystemAuthorizations = ko.unwrap(settings.showSystemAuthorizations) !== undefined ? ko.unwrap(settings.showSystemAuthorizations) : false;
                self.showProjectAuthorizations = ko.unwrap(settings.showProjectAuthorizations) !== undefined ? ko.unwrap(settings.showProjectAuthorizations) : true;

                self.currentUserProjectAuthorizations = connector.currentUserProjectAuthorizations;
                self.currentUserSystemAuthorizations = connector.currentUserSystemAuthorizations;

                self.loggedInUserName = connector.currentLoggedInUser;


                return connector.getCurrentLoggedInUser()
                    .then(function () {
                        return connector.getCurrentUserAuthorizations();
                    })
                    .fail(self.connector.handleError(self));
            }
        };

        return ctor;
    });