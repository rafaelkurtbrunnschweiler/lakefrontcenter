﻿define(["../../services/connector", "../../services/statesService", "../../services/securedService", "../../services/visualSecurityService"],
    function (signalsConnector, statesService, securedService, visualSecurityService) {

        var ctor = function () {
            var self = this;

            self.defaultSettings = {
                isTipModeEnabled: ko.observable(false),
                writeUpValue: ko.observable(0),
                writeUpDelay: ko.observable(500)
            };
        };

        ctor.prototype = {
            activate: function (settings) {
                var self = this;
                self.connector = new signalsConnector();

                self.objectID = ko.unwrap(settings.objectID);

                self.projectAuthorization = (ko.unwrap(settings.projectAuthorization) || "").stringPlaceholderResolver(self.objectID);
                self.securedService = new securedService(self.projectAuthorization);
                self.hasAuthorization = self.securedService.hasAuthorization;

                self.settings = _.extend(self.defaultSettings, settings);
                self.isModalDialogsDraggable = self.settings.isModalDialogsDraggable !== undefined ? self.settings.isModalDialogsDraggable : true;
                self.tooltipText = (ko.unwrap(self.connector.translate(self.settings.tooltipText)()) || "").stringPlaceholderResolver(self.objectID);
                self.buttonText = (ko.unwrap(self.settings.buttonText) || '').stringPlaceholderResolver(self.objectID);

                self.cssClass = ko.unwrap(self.settings.cssClass) || '';
                self.iconClass = ko.unwrap(self.settings.iconClass) || '';
                self.buttonStyle = ko.unwrap(self.settings.buttonStyle) || '';
                self.iconStyle = ko.unwrap(self.settings.iconStyle) || '';
                self.textStyle = ko.unwrap(self.settings.textStyle) || '';

                var cssClassNames = [self.settings.cssClassNormalState || "btn-default normal"];

                if (_.any(self.settings.states)) {
                    _.each(self.settings.states, function (state) {
                        cssClassNames.push(state.cssClassName);
                    });
                } else if (!Array.isArray(self.settings.cssClassStates)) {
                    cssClassNames.push(self.settings.cssClassState1 || "state1");
                    cssClassNames.push(self.settings.cssClassState2 || "state2");
                    cssClassNames.push(self.settings.cssClassState3 || "state3");
                    cssClassNames.push(self.settings.cssClassState4 || "state4");
                    cssClassNames.push(self.settings.cssClassState5 || "state5");
                    cssClassNames.push(self.settings.cssClassState6 || "state6");
                    cssClassNames.push(self.settings.cssClassState7 || "state7");
                    cssClassNames.push(self.settings.cssClassState8 || "state8");
                } else {
                    cssClassNames.push.apply(cssClassNames, self.settings.cssClassStates);
                }

                self.states = new statesService(self.settings);

                self.statusCssClass = ko.computed(function () {
                    var stateNumber = ko.unwrap(self.states.currentStateIndex);

                    var cssClass = Number.isNaN(stateNumber) ||
                        stateNumber >= cssClassNames.length ?
                        cssClassNames[0] :
                        cssClassNames[stateNumber];

                    return cssClass;
                }, self);

                self.signalName = (ko.unwrap(self.settings.signalName) || '').stringPlaceholderResolver(self.objectID);

                self.writeSecure = ko.unwrap(self.settings.writeSecure) !== undefined ? ko.unwrap(self.settings.writeSecure) : false;
                self.writeSecureValue = ko.observable();
                self.showWriteSecure = ko.observable(false);

                self.visualSecurityService = new visualSecurityService(self.settings, self.connector);
                self.visualSecurityService.initialize();
                self.isVisible = self.visualSecurityService.isVisible;
                self.isDisabled = self.visualSecurityService.isDisabled;

                self.clearBuffer = ko.unwrap(self.settings.clearBuffer) !== undefined ? ko.unwrap(self.settings.clearBuffer) : false;
            },

            writeFromBuffer: function () {
                var self = this;

                if (self.writeSecure)
                    self.showWriteSecure(true);
                else if (self.clearBuffer)
                    self.connector.clearSignalBuffer()
                else
                    self.connector.writeSignalsFromBuffer();
            },
        };

        return ctor;
    });