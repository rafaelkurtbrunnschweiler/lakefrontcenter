﻿define(["../../services/connector", '../../services/usersService', "../../services/securedService"],
    function (signalsConnector, usersService, securedService) {

        var ctor = function() {
            var self = this;
            self.output = "";
        };

        ctor.prototype = {
            activate: function(settings) {
                var self = this;
                self.objectID = ko.unwrap(settings.objectID);

                self.projectAuthorization = (ko.unwrap(settings.projectAuthorization) || "").stringPlaceholderResolver(self.objectID);
                self.securedService = new securedService(self.projectAuthorization);
                self.hasAuthorization = self.securedService.hasAuthorization;

                self.settings = settings;
                self.connector = new signalsConnector();

                self.tooltipText = (ko.unwrap(self.connector.translate(self.settings.tooltipText)()) || "").stringPlaceholderResolver(self.objectID);
                self.connector.currentLoggedInUser.subscribe(function() {
                    self.getCurrentUserDetails();
                });

                self.property = ko.unwrap(settings.propertyName) || 'Name';
                self.usersService = new usersService();

                self.userDetails = ko.observable({});

                self.output = ko.computed(function() {
                    if (self.userDetails() && !isNullOrUndefined(self.userDetails()[self.property])) {
                        return self.userDetails()[self.property];
                    }
                    return "";
                });
                
                self.getCurrentUserDetails();
            },

            getCurrentUserDetails: function() {
                var self = this;

                usersService.getCurrentUserDetails()
                    .then(function(userDetails) {
                        self.userDetails(userDetails);
                    })
                    .fail(self.connector.handleError(self));
            }
        };

        return ctor;
    });