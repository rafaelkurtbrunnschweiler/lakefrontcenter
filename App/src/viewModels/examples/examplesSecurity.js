﻿define(
    function () {
        var ctor = function () {
            var self = this;

            self.items = [
                "Secured Item 1",
                "Secured Item 2",
                "Secured Item 3"
            ];    
        };

        ctor.prototype = {
            activate: function () {
                var self = this;
            }
        }

        //ctor.prototype = {
        //    activate: function(settings) {
        //        var self = this;
        //        self.connector = new signalsConnector();
        //        self.currentUserProjectAuthorizations = self.connector.currentUserProjectAuthorizations;
        //        self.currentUserSystemAuthorizations = self.connector.currentUserSystemAuthorizations;
        //    },

        //    clickLogin: function() {
        //        var self = this;
        //        self.login().then(function() {
        //            //alert(self.token());
        //            // self.securityToken = securityService.securityToken;
        //        });
        //    },

        //    clickLogout: function() {
        //        var self = this;
        //        self.logout().then(function() {
        //            //alert(self.token());
        //            // self.securityToken = securityService.securityToken;
        //        });
        //    },

        //    login: function() {
        //        var self = this;
        //        var promise = self.connector.login(self.userName, self.password, false);

        //        return promise;
        //    },

        //    logout: function() {
        //        var self = this;
        //        var promise = self.connector.logout();

        //        return promise;
        //    },

        //    getCurrentLoggedInUser: function() {
        //        var self = this;
        //        return self.connector.getCurrentLoggedInUser()
        //            .then(function(userName) {
        //                self.userNameLoggedIn(userName);
        //            });
        //    }
        //};

        return ctor;
    });