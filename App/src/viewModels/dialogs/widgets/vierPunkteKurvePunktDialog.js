﻿define(
    [
        'plugins/dialog'
    ],

    function (dialog) {

        var Dialog = function (widget) {
            var self = this;
            self.widget = widget;
        };


        Dialog.prototype = {
            activate: function (settings) {
                var self = this;
                ko.bindingHandlers.storeElement = {
                    init: function (element, valueAccessor) {
                        valueAccessor()(element);
                    }
                }
               
                self.zustand = ko.computed(function () {
                    return zustandText(self.widget.zustandValue());
                });

                self.myCanvas = ko.observable();
                self.myCanvas.subscribe(function initCanvas(element) {
                    self.myCanvas = ko.computed(function () {
                        var ordinate1 = (235 - calcOrdinate(self.widget.ordinate1Value(), self.widget.ordinate4Value()));
                        var ordinate2 = (235 - calcOrdinate(self.widget.ordinate2Value(), self.widget.ordinate4Value()));
                        var ordinate3 = (235 - calcOrdinate(self.widget.ordinate3Value(), self.widget.ordinate4Value()));
                        var ordinate4 = (235 - calcOrdinate(self.widget.ordinate4Value(), self.widget.ordinate4Value()));

                        var abszisse1 = calcAbszisse(self.widget.abszisse1Value(), self.widget.abszisse4Value());
                        var abszisse2 = calcAbszisse(self.widget.abszisse2Value(), self.widget.abszisse4Value());
                        var abszisse3 = calcAbszisse(self.widget.abszisse3Value(), self.widget.abszisse4Value());
                        var abszisse4 = calcAbszisse(self.widget.abszisse4Value(), self.widget.abszisse4Value());

                        // 4-Punkte-Kurve
                        var ctx = element.getContext("2d");
                        ctx.clearRect(0, 0, 535, 270);
                        ctx.strokeStyle = 'blue';
                        ctx.beginPath();
                        ctx.lineWidth = 2;
                        ctx.setLineDash([0, 0]);
                        ctx.moveTo(abszisse1, ordinate1);
                        ctx.lineTo(abszisse2, ordinate2);
                        ctx.moveTo(abszisse2, ordinate2);
                        ctx.lineTo(abszisse3, ordinate3);
                        ctx.moveTo(abszisse3, ordinate3);
                        ctx.lineTo(abszisse4, ordinate4);
                        ctx.stroke();

                        // Pfeil X-Achse
                        ctx.strokeStyle = '#404040';
                        ctx.beginPath();
                        ctx.lineWidth = 1;
                        ctx.setLineDash([0, 0]);
                        ctx.moveTo(10, 235);
                        ctx.lineTo(525, 235);
                        ctx.moveTo(525, 235);
                        ctx.lineTo(515, 230);
                        ctx.moveTo(525, 235);
                        ctx.lineTo(515, 240);
                        ctx.stroke();

                        // Pfeil Y-Achse
                        ctx.strokeStyle = '#404040';
                        ctx.beginPath();
                        ctx.lineWidth = 1;
                        ctx.setLineDash([0, 0]);
                        ctx.moveTo(200, 240);
                        ctx.lineTo(200, 14);
                        ctx.moveTo(200, 14);
                        ctx.lineTo(195, 24);
                        ctx.moveTo(200, 14);
                        ctx.lineTo(205, 24);
                        ctx.stroke();

                        // Punkt zu Beschriftung Linie
                        ctx.beginPath();
                        ctx.lineWidth = 1;
                        ctx.setLineDash([2, 2]);
                        ctx.strokeStyle = '#404040';
                        ctx.moveTo(abszisse1, ordinate1);
                        ctx.lineTo(15, ordinate1);
                        ctx.moveTo(abszisse2, ordinate2);
                        ctx.lineTo(15, ordinate2);
                        ctx.moveTo(abszisse3, ordinate3);
                        ctx.lineTo(15, ordinate3);
                        ctx.moveTo(abszisse4, ordinate4);
                        ctx.lineTo(15, ordinate4);

                        ctx.moveTo(abszisse1, ordinate1);
                        ctx.lineTo(abszisse1, 240);
                        ctx.moveTo(abszisse2, ordinate2);
                        ctx.lineTo(abszisse2, 240);
                        ctx.moveTo(abszisse3, ordinate3);
                        ctx.lineTo(abszisse3, 240);
                        ctx.moveTo(abszisse4, ordinate4);
                        ctx.lineTo(abszisse4, 240);
                        ctx.stroke();

                        // Text Punkt Beschriftung
                        ctx.font = "12px Arial";
                        ctx.textAlign = "center";
                        ctx.fillText("X1", abszisse1, 250);
                        ctx.fillText("X2", abszisse2, 250);
                        ctx.fillText("X3", abszisse3, 250);
                        ctx.fillText("X4", abszisse4, 250);
                        ctx.textAlign = "left";
                        ctx.fillText("Y1", 0, ordinate1);
                        ctx.fillText("Y2", 0, ordinate2);
                        ctx.fillText("Y3", 0, ordinate3);
                        ctx.fillText("Y4", 0, ordinate4);
                        
                        // Text Achsenbeschriftung
                        ctx.textAlign = "right";
                        ctx.fillText(self.widget.groesseX, 530, 265);
                        ctx.textAlign = "center";
                        ctx.fillText(self.widget.groesseY, 200, 10);
                        
                        return;
                    });
                });

                function calcOrdinate(value, max) {
                    //return (220 / (max) * value);
                    return (200 / (max) * value);
                }

                function calcAbszisse(value, max) {
                    // return ((325 / (max) * value) + 200);
                    return ((300 / (max) * value) + 200);
                }

                self.textFarbe = ko.computed(function () {
                    switch (self.widget.zustandValue()) {
                        case 0:
                            return '#333';
                        case 2:
                            return '#ff0000';
                        case 4:
                            return '#333';
                        case 8:
                            return '#333';
                        case 16:
                            return '#333';
                        case 31:
                            return '#333';
                        case 32:
                            return '#333';
                        case 64:
                            return '#333';
                        case 128:
                            return '#333';
                        default:
                            return '#333';
                    }
                });

                self.handbetrieb = ko.computed(function () {
                    if (self.widget.handuebersteuerungValue() != 0) {
                        return 'visible';
                    }
                    else {
                        return 'hidden';
                    }
                });

                function zustandText(zustand) {
                    switch (zustand) {
                        case 0:
                            return 'Initialisierend';
                        case 2:
                            return 'Fehler';
                        case 4:
                            return 'Passiv';
                        case 8:
                            return 'Aus';
                        case 9:
                            return 'Ausschaltend';
                        case 16:
                            return 'Notbedienung';
                        case 32:
                            return 'Ein';
                        case 67108864:
                            return 'Alarmunterdrückung aktiv';
                        default:
                            return 'Unbekannte Statusinformation erhalten'
                    }
                };

                self.alarms = ko.computed(function () {
                    self.alarm = ko.observable();
                    self.alarms = ko.observableArray();

                    var alarmvalue = self.widget.alarme();

                    if (alarmvalue == 0) {
                        self.meldung = 'Zur Zeit stehen keine Alarme an';
                        self.alarms().push({ alarm: self.meldung });
                    }

                    if ((alarmvalue & 1) == 1) {
                        self.meldung = 'Fehler von übergeordnetem Objekt ausgelöst';
                        self.alarms().push({ alarm: self.meldung });
                    }

                    if ((alarmvalue & 2) == 2) {
                        self.meldung = 'Interner Fehler';
                        self.alarms().push({ alarm: self.meldung });
                    }

                    if ((alarmvalue & 4) == 4) {
                        self.meldung = 'Fehler Feldebene';
                        self.alarms().push({ alarm: self.meldung });
                    }

                    if ((alarmvalue & 8) == 8) {
                        self.meldung = 'Falsche oder unmögliche Werteingabe';
                        self.alarms().push({ alarm: self.meldung });
                    }

                    if ((alarmvalue & 16) == 16) {
                        self.meldung = 'Obere- oder untere Begrenzung erreicht';
                        self.alarms().push({ alarm: self.meldung });
                    }

                    return self.alarms();
                });


                // -----------------------------------------
                var betriebswahl = function (wert, name) {
                    var self = this;
                    self.wert = wert;
                    self.name = name;
                }

                self.options = [
                    new betriebswahl(0, "Automatik"),
                    new betriebswahl(4, "Passiv"),
                    new betriebswahl(8, "Aus"),
                    new betriebswahl(32, "Ein")
                ];

                self.changed = function (obj, event) {
                    if (event.originalEvent) { //user changed
                        self.widget.setBetriebswahl(self.selectedOptionValue());
                    } else { // program changed

                    }
                };
                self.selectedOptionValue = ko.observable(self.widget.betriebswahlValue());

                self.quitBtn = ko.computed(function () {
                    if (self.widget.zustandValue() == 2) {
                        return 'btn btn-xs btn-danger';
                    }
                    else {
                        return 'btn btn-xs btn-primary normal';
                    }
                });

                self.alarmeunterdr = ko.observable(self.widget.getAlarmunterdr());
                self.changedAlarmeunterdr = function (obj, event) {
                    if (event.originalEvent) { //user changed
                        self.widget.changeBit(30);
                    } else { // program changed

                    }
                };
                // öffne / schliesse den Bereich "Erweitert"
                /* 
                dieser Code sollte am Ende stehen, damit Werte in den Bereich der 
                per default augeblendet ist, geladen werden können bevor 
                dieser ausgeblendet wird
                */
                self.bedienpanel = ko.observable('none');

                self.bedienung = function () {
                    var zustand = self.bedienpanel();
                    if (zustand == 'none') {
                        self.bedienpanel('inline');
                    }
                    else {
                        self.bedienpanel('none');
                    }
                };
            }
        }

        Dialog.prototype.close = function () {
            var self = this;
            dialog.close(self, '');
        };

        return Dialog;

    });