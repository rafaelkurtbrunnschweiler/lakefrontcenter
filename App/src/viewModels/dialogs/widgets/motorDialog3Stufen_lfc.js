﻿define(
    [
        'plugins/dialog',
        '../../../services/securedService'
    ],
    function (dialog, securedService) {

        var Dialog = function (widget) {
            var self = this;
            self.widget = widget;
        };

        Dialog.prototype = {
            activate: function (settings) {
                var self = this;

                self.projectAuthorization = self.widget.autorisierung || "none";
                self.securedService = new securedService(self.projectAuthorization);
                self.hasAuthorization = self.securedService.hasAuthorization;

                self.titel = ko.computed(function () {
                    return self.widget.dialogtitle;
                });

                // -----------------------------------------
                var betriebswahl = function (wert, name) {
                    var self = this;
                    self.wert = wert;
                    self.name = name;
                }

                self.options = [
                    new betriebswahl(0, "Aus"),
                    new betriebswahl(1, "Automatik"),
                    new betriebswahl(2, "Stufe 1"),
                    new betriebswahl(3, "Stufe 2"),
                    new betriebswahl(4, "Stufe 3")
                ];

                self.changed = function (obj, event) {
                    if (event.originalEvent) { //user changed
                        self.widget.setBetriebswahl(self.selectedOptionValue());
                    } else { // program changed

                    }
                };
                self.selectedOptionValue = ko.observable(self.widget.betriebswahlValue());


                self.zustand = ko.computed(function () {
                    switch (self.widget.objektZustand()) {
                        case 0:
                            return 'Aus';
                        case 1:
                            return 'Automatik';
                        case 2:
                            return 'Ein';
                        default:
                            return 'Unbekannte Statusinformation erhalten'
                    }
                });

                self.zustandmotor = ko.computed(function () {
                    switch (self.widget.objektZustandMotor()) {
                        case 0:
                            return 'Aus';
                        case 1:
                            return 'Notbetrieb';
                        case 2:
                            return 'Stufe 1';
                        case 3:
                            return 'Stufe 2';
                        case 4:
                            return 'Stufe 3';
                        case 5:
                            return 'Störung';
                        default:
                            return 'Unbekannte Statusinformation erhalten'
                    }
                });


                self.textFarbe = ko.computed(function () {
                    switch (self.widget.objektZustand()) {
                        case 0:
                            return '#333';
                        case 1:
                            return '#333';
                        case 2:
                            return '#333';
                        case 3:
                            return '#333';
                        case 4:
                            return '#333';
                        case 5:
                            return '#333';
                        default:
                            return '#333';
                    }
                });

                self.quitBtn = ko.computed(function () {
                    if (self.widget.objektZustand() == 2) {
                        return 'btn btn-xs btn-danger';
                    }
                    else {
                        return 'btn btn-xs btn-primary normal';
                    }
                });

                self.handbetrieb = ko.computed(function () {
                    if (self.widget.handbetriebValue() != 0) {
                        return 'visible';
                    }
                    else {
                        return 'hidden';
                    }
                });



                // öffne / schliesse den Bereich "Erweitert"
                /* 
                dieser Code sollte am Ende stehen, damit Werte in den Bereich der 
                per default augeblendet ist, geladen werden können bevor 
                dieser ausgeblendet wird
                */                
                self.erweitert = ko.observable('none');
                self.bedienung = function () {
                    var zustand = self.erweitert();
                    if (zustand == 'none') {
                        self.erweitert('inline');
                    }
                    else {
                        self.erweitert('none');
                    }
                };
            },
        },

        Dialog.prototype.close = function () {
            var self = this;
            dialog.close(self, '');
        };

        return Dialog;
    });