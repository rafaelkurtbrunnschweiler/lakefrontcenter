﻿define(
	[
		'plugins/dialog'
	],
	function (dialog) {

	    var Dialog = function (widget) {
	        var self = this;
	        self.widget = widget;
	    };

	    Dialog.prototype = {
	        activate: function (settings) {
	            var self = this;
	            // Copy

	            self.changed = function (obj, event) {
	                if (event.originalEvent) { //user changed
	                    self.widget.setBetriebswahl();
	                } else { // program changed

	                }
	            };
	            
	            self.alarms = ko.computed(function () {
	                self.alarm = ko.observable();
	                self.alarms = ko.observableArray();

	                var alarmvalue = self.widget.alarme();

	                if (alarmvalue == 0) {
	                    self.meldung = 'Zur Zeit stehen keine Alarme an';
	                    self.alarms().push({ alarm: self.meldung });
	                }
	                //Bit 0
	                if ((alarmvalue & 1) == 1) {
	                    self.meldung = 'undefinierter Zustand in Alarmobjekt';
	                    self.alarms().push({ alarm: self.meldung });
	                }
	                //Bit 1
	                if ((alarmvalue & 2) == 2) {
	                    self.meldung = 'undefinierter Zustand in Alarmobjekt';
	                    self.alarms().push({ alarm: self.meldung });
	                }
	                //Bit 2
	                if ((alarmvalue & 4) == 4) {
	                    self.meldung = 'undefinierter Zustand in Alarmobjekt';
	                    self.alarms().push({ alarm: self.meldung });
	                }
	                //Bit 3
	                if ((alarmvalue & 8) == 8) {
	                    self.meldung = 'interner Fehler';
	                    self.alarms().push({ alarm: self.meldung });
	                }
	                //Bit 4
	                if ((alarmvalue & 16) == 16) {
	                    self.meldung = 'undefinierter Zustand in Alarmobjekt';
	                    self.alarms().push({ alarm: self.meldung });
	                }
	                //Bit 5
	                if ((alarmvalue & 32) == 32) {
	                    self.meldung = 'interner Fehler';
	                    self.alarms().push({ alarm: self.meldung });
	                }
	                return self.alarms();
	            });

	            self.quitBtn = ko.computed(function () {
	                if (self.widget.plcZustandValue() == 2) {
	                    return 'btn btn-xs btn-danger';
	                } else {
	                    return 'btn btn-xs btn-primary normal';
	                }
	            });

	            
	            self.alarmeunterdr = ko.observable(self.widget.getAlarmunterdr());
	            self.changedAlarmeunterdr = function (obj, event) {
	                if (event.originalEvent) { //user changed
	                    self.widget.changeBit(30);
	                } else { // program changed

	                }
	            };

	            self.textFarbe = ko.computed(function () {
	                self.al = new Alarming(self.widget.plcZustandValue());
	                return self.al.textColor();
	            })
	            // öffne / schliesse den Bereich "Erweitert"
	            /*
				dieser Code sollte am Ende stehen, damit Werte in den Bereich der
				per default ausgeblendet ist, geladen werden können bevor
				dieser ausgeblendet wird
				*/
	            self.erweitert = ko.observable('none');
	            self.bedienung = function () {
	                var zustand = self.erweitert();
	                if (zustand == 'none') {
	                    self.erweitert('inline');
	                } else {
	                    self.erweitert('none');
	                }
	            };
	        }
	    };

	    Dialog.prototype.close = function () {
	        var self = this;
	        dialog.close(self, '');
	    };

	    return Dialog;

	});