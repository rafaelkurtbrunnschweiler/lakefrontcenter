﻿define(
    [
        'plugins/dialog'
    ],
    function (dialog) {

        var Dialog = function (widget) {
            var self = this;
            self.widget = widget;
        };

        Dialog.prototype = {
            activate: function (settings) {
                var self = this;


                self.zustandHws = ko.observable();
                self.hws = ko.computed(function () {
                    if (self.widget.modulnameHws === "empty") {
                        return 'none';
                    }
                    else {
                        self.zustandHws = ko.computed(function () {
                            return zustandText(self.widget.zustandHwsValue());
                        });
                        return 'inline';
                    }
                });

                self.zustand = ko.computed(function () {
                    return zustandText(self.widget.zustandValue());
                });

                self.textFarbe = ko.computed(function () {
                    switch (self.widget.zustandValue()) {
                        case 0:
                            return '#333';
                        case 1:
                            return '#333';
                        case 2:
                            return '#333';
                        case 3:
                            return '#333';
                        case 4:
                            return '#333';
                        default:
                            return '#333';
                    }
                });


                function zustandText(zustand) {
                    switch (zustand) {
                        case 0:
                            return 'Aus';
                        case 1:
                            return 'Automatik';
                        case 2:
                            return 'Ein Stufe 1';
                        case 3:
                            return 'Ein Stufe 2';
                        default:
                            return 'Unbekannte Statusinformation erhalten'
                    }
                };

                // -----------------------------------------
                //var betriebswahl = function (wert, name) {
                //    var self = this;
                //    self.wert = wert;
                //    self.name = name;
                //}

                //self.options = [
                //    new betriebswahl(0, "Aus"),
                //    new betriebswahl(1, "Automatik"),
                //    new betriebswahl(2, "Ein")
                //];

                //self.changed = function (obj, event) {
                //    if (event.originalEvent) { //user changed
                //        self.widget.setBetriebswahl(self.selectedOptionValue());
                //    } else { // program changed

                //    }
                //};
                //self.selectedOptionValue = ko.observable(self.widget.betriebswahlValue());

                // öffne / schliesse den Bereich "Erweitert"
                /* 
                dieser Code sollte am Ende stehen, damit Werte in den Bereich der 
                per default augeblendet ist, geladen werden können bevor 
                dieser ausgeblendet wird
                */
                self.erweitert = ko.observable('none');
                self.bedienung = function () {
                    var zustand = self.erweitert();
                    if (zustand == 'none') {
                        self.erweitert('inline');
                    }
                    else {
                        self.erweitert('none');
                    }
                };
            }
        }

        Dialog.prototype.close = function () {
            var self = this;
            dialog.close(self, '');
        };

        return Dialog;

    });