﻿define(
	[
		'plugins/dialog'
	],
	function (dialog) {

	    var Dialog = function (widget) {
	        var self = this;
	        self.widget = widget;
	    };

	    Dialog.prototype = {
	        activate: function (settings) {
	            var self = this;
	            // Copy

	            self.sperreheiz = ko.computed(function () {
	                if (self.widget.plcheizventillockedValue()) {
	                    return 'wf wf-lock';
	                } else {
	                    return 'wf wf-unlock';
	                }
	            });

	            self.sperrekuehl = ko.computed(function () {
	                if (self.widget.plckuehlventillockedValue()) {
	                    return 'wf wf-lock';
	                } else {
	                    return 'wf wf-unlock';
	                }
	            });


	        }
	    };

	    Dialog.prototype.close = function () {
	        var self = this;
	        dialog.close(self, '');
	    };

	    return Dialog;

	});