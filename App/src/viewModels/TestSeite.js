﻿define(
    [
        '../services/securedService'
    ],
    function (securedService) {

        var Dialog = function () {
            var self = this;
        };

        Dialog.prototype = {
            activate: function (settings) {
                var self = this;

                self.projectAuthorization = "Administration";
                self.securedService = new securedService(self.projectAuthorization);
                self.hasAuthorization = self.securedService.hasAuthorization;
            }

            
        };

        self.bedienung = function () {
            alert("Geklickt")
        };
        return Dialog;
    });