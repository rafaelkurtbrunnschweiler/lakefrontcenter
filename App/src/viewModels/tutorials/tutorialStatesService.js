define(
    ["../../services/statesService"],
    function (statesService) {
        var ctor = function () {
            var self = this;
        };

        ctor.prototype = {
            activate: function () {
                var self = this;

                self.settings = {
                    states: [{
                            conditionRule: "%Setpoint 1%==1"
                        },
                        {
                            conditionRule: "%Setpoint 1%==2"
                        }
                    ]
                }

                self.states = new statesService(self.settings);

            },
            detached: function () {
                var self = this;
                self.states.unregisterSignals();
            }

        };

        return ctor;
    });