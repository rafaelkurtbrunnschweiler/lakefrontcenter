define(
    ["../../services/securedService"],
    function (securedService) {
        var ctor = function () {
            var self = this;
        };

        ctor.prototype = {
            activate: function () {
                var self = this;

                self.projectAuthorization = "Administration";
                self.securedService = new securedService(self.projectAuthorization);
                self.hasAuthorization = self.securedService.hasAuthorization;
            }
        };

        return ctor;
    });