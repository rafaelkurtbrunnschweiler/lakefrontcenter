var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "plugins/router", "./viewModelBase"], function (require, exports, router, ViewModelBase) {
    "use strict";
    var Shell = /** @class */ (function (_super) {
        __extends(Shell, _super);
        function Shell() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this.router = router;
            _this.navbarClass = ko.observable("navbar-top"); //"navbar-left" for left sidebar navigation is also implemented
            return _this;
        }
        Shell.prototype.activate = function () {
            router.map([
                { route: "", title: "Start", moduleId: "src/viewModels/home_lfc", nav: false, hash: '#home_lfc', settings: { iconClass: "fa fa-home" } },
                { route: "demos", title: "Demos", moduleId: "src/viewModels/demosOverview", nav: true, settings: { iconClass: "fa fa-star" } },
                { route: "widgets", title: "HTML5 Widgets", moduleId: "src/viewModels/widgetsOverview", hash: "#widgets", nav: true, settings: { iconClass: "fa fa-cog" } },
                { route: "icons", title: "Icons", moduleId: "src/viewModels/icons", nav: true, settings: { iconClass: "wf wf-thumbs" } },
                // Main Navigation routes
                { route: "production", title: "Produktion", moduleId: "src/viewModels/demos/production", nav: false },
                { route: "building", title: "Building", moduleId: "src/viewModels/demos/building", nav: false },
                { route: "hvac", title: "HVAC", moduleId: "src/viewModels/demos/hvac", nav: false },
                { route: "bhkw", title: "BHKW", moduleId: "src/viewModels/demos/bhkw", nav: false },
                // Widget examples routes
                { route: "visualisation", title: "Widgets for Visualization", moduleId: "src/viewModels/examples/examplesVisualisation", nav: false, settings: { iconClass: "" } },
                { route: "control", title: "Control and Write Signal Values", moduleId: "src/viewModels/examples/examplesControl", nav: false, settings: { iconClass: "" } },
                { route: "charts", title: "Charts Widgets for historical Data", moduleId: "src/viewModels/examples/examplesCharts", nav: false, settings: { iconClass: "" } },
                { route: "alarms", title: "AlarmViewer for online and alarms history", moduleId: "src/viewModels/examples/examplesAlarms", nav: false, settings: { iconClass: "" } },
                { route: "security", title: "Security", moduleId: "src/viewModels/examples/examplesSecurity", nav: false, settings: { iconClass: "" } },
                { route: "components", title: "Misc Components and Decorative Elements", moduleId: "src/viewModels/examples/examplesGraphicalComponents", nav: false, settings: { iconClass: "" } },
                { route: "koComponents", title: "Overview UI Components", moduleId: "src/viewModels/examples/examplesComponentsOverview", nav: false, settings: { iconClass: "" } },
                { route: "bindings", title: "Knockout JS Binding Handler", moduleId: "src/viewModels/examples/examplesBindingHandlers", nav: false },
                // Tutorials and Workshops routes
                { route: "tutorialSignals", title: "Tutorial - Dynamic Signallist", moduleId: "src/viewModels/examples/tutorialSignals", nav: false, settings: { iconClass: "" } },
                { route: "tutorials", title: "WEBfactory HTML Spa Workshop", moduleId: "src/viewModels/tutorials/onlineValues", nav: false, settings: { iconClass: "wf wf-bell" } },
                { route: "ko", title: "Knockout JS Basics - WEBfactory Workshop", moduleId: "src/viewModels/tutorials/knockout", nav: false },
                { route: "tutorialsOverview", title: "WEBfactory HTML Spa Workshop", moduleId: "src/viewModels/tutorials/tutorialsOverview", nav: false },
                { route: "details(/:id)", title: "WEBfactory HTML Spa Workshop", moduleId: "src/viewModels/tutorials/detailsPage", hash: "#details", nav: false },
                { route: "tutorialLogTags", title: "WEBfactory HTML Spa Workshop", moduleId: "src/viewModels/tutorials/tutorialLogTags", hash: "#tutorialLogTags", nav: false },
                { route: "externalWevservice", title: "External Wevservice Example", moduleId: "src/viewModels/tutorials/externalWevservice", nav: false },
                { route: "tutorialDynamicProperties", title: "Dynamic properties", moduleId: "src/viewModels/tutorials/tutorialDynamicProperties", nav: false },
                // Widgets  routes
                { route: "scWfValue", title: "wfValue", moduleId: "src/viewModels/widgetsShowcase/scWfValue", nav: false, settings: { iconClass: "fa fa-cog" } },
                { route: "scWfValueArc", title: "wfValueArc", moduleId: "src/viewModels/widgetsShowcase/scWfValueArc", nav: false, settings: { iconClass: "fa fa-cog" } },
                { route: "scWfValueBarGraph", title: "wfValueBarGraph", moduleId: "src/viewModels/widgetsShowcase/scWfValueBarGraph", nav: false, settings: { iconClass: "fa fa-cog" } },
                { route: "scWfValueDisplay", title: "wfValueBarGraph", moduleId: "src/viewModels/widgetsShowcase/scWfValueDisplay", nav: false, settings: { iconClass: "fa fa-cog" } },
                { route: "scWfValueGauge", title: "wfValueGauge", moduleId: "src/viewModels/widgetsShowcase/scWfValueGauge", nav: false, settings: { iconClass: "fa fa-cog" } },
                { route: "scWfWriteValueButton", title: "wfWriteValueButton", moduleId: "src/viewModels/widgetsShowcase/scWfWriteValueButton", nav: false, settings: { iconClass: "fa fa-cog" } },
                { route: "scWfWriteValueCombobox", title: "wfWriteValueCombobox", moduleId: "src/viewModels/widgetsShowcase/scWfWriteValueCombobox", nav: false, settings: { iconClass: "fa fa-cog" } },
                { route: "scWfUserLogin", title: "wfUserLogin", moduleId: "src/viewModels/widgetsShowcase/scWfUserLogin", nav: false, settings: { iconClass: "fa fa-cog" } },
                { route: "scWfUserAuthorizationsList", title: "wfUserAuthorizationsList", moduleId: "src/viewModels/widgetsShowcase/scWfUserAuthorizationsList", nav: false, settings: { iconClass: "fa fa-cog" } },
                { route: "scWfSymbolicText", title: "wSymbolicText", moduleId: "src/viewModels/widgetsShowcase/scWfSymbolicText", nav: false, settings: { iconClass: "fa fa-cog" } },
                { route: "scWfSwitchValue", title: "wfSwitchValue", moduleId: "src/viewModels/widgetsShowcase/scWfSwitchValue", nav: false, settings: { iconClass: "fa fa-cog" } },
                { route: "scWfSwitchValue3States", title: "wfSwitchValue3States", moduleId: "src/viewModels/widgetsShowcase/scWfSwitchValue3States", nav: false, settings: { iconClass: "fa fa-cog" } },
                { route: "scWfStateText", title: "wfStateText", moduleId: "src/viewModels/widgetsShowcase/scWfStateText", nav: false, settings: { iconClass: "fa fa-cog" } },
                { route: "scWfStateIndicator", title: "wfStateIndicator", moduleId: "src/viewModels/widgetsShowcase/scWfStateIndicator", nav: false, settings: { iconClass: "fa fa-cog" } },
                { route: "scWfStateDisplay", title: "wfStateDisplay", moduleId: "src/viewModels/widgetsShowcase/scWfStateDisplay", nav: false, settings: { iconClass: "fa fa-cog" } },
                { route: "scWfStateCssClass", title: "wfStateCssClass", moduleId: "src/viewModels/widgetsShowcase/scWfStateCssClass", nav: false, settings: { iconClass: "fa fa-cog" } },
                { route: "scWfSignalInformation", title: "wfSignalInformation", moduleId: "src/viewModels/widgetsShowcase/scWfSignalInformation", nav: false, settings: { iconClass: "fa fa-cog" } },
                { route: "scWfSensorValue", title: "wfSensorValue", moduleId: "src/viewModels/widgetsShowcase/scWfSensorValue", nav: false, settings: { iconClass: "fa fa-cog" } },
                { route: "scWfSecuredContainer", title: "wfSecuredContainer", moduleId: "src/viewModels/widgetsShowcase/scWfSecuredContainer", nav: false, settings: { iconClass: "fa fa-cog" } },
                { route: "scWfLogTagTrend", title: "wfLogTagTrend", moduleId: "src/viewModels/widgetsShowcase/scWfLogTagTrend", nav: false, settings: { iconClass: "fa fa-cog" } },
                { route: "scWfLogTagTable", title: "wfLogTagTable", moduleId: "src/viewModels/widgetsShowcase/scWfLogTagTable", nav: false, settings: { iconClass: "fa fa-cog" } },
                { route: "scWfLanguageDropdown", title: "wfLanguageDropdown", moduleId: "src/viewModels/widgetsShowcase/scWfLanguageDropdown", nav: false, settings: { iconClass: "fa fa-cog" } },
                { route: "scWfInputValue", title: "wfInputValue", moduleId: "src/viewModels/widgetsShowcase/scWfInputValue", nav: false, settings: { iconClass: "fa fa-cog" } },
                { route: "scWfAlarmViewer", title: "scWfAlarmViewer", moduleId: "src/viewModels/widgetsShowcase/scWfAlarmViewer", nav: false, settings: { iconClass: "fa fa-cog" } },
                { route: "scWfSlider", title: "scWfSlider", moduleId: "src/viewModels/widgetsShowcase/scWfSlider", nav: false, settings: { iconClass: "fa fa-cog" } },
                { route: "scWfWatchdog", title: "scWfSlider", moduleId: "src/viewModels/widgetsShowcase/scWfWatchdog", nav: false, settings: { iconClass: "fa fa-cog" } },
                { route: "scWfLogbook", title: "scWfLogbook", moduleId: "src/viewModels/widgetsShowcase/scWfLogbook", nav: false, settings: { iconClass: "fa fa-cog" } },
                { route: "scWfUserInformation", title: "scWfLogbook", moduleId: "src/viewModels/widgetsShowcase/scWfUserInformation", nav: false, settings: { iconClass: "fa fa-cog" } },
                { route: "scWfLogbookViewer", title: "scWfLogbookViewer", moduleId: "src/viewModels/widgetsShowcase/scWfLogbookViewer", nav: false, settings: { iconClass: "fa fa-cog" } },
                { route: "scWfSignalList", title: "scWfSignalList", moduleId: "src/viewModels/widgetsShowcase/scWfSignalList", nav: false, settings: { iconClass: "fa fa-cog" } },
                { route: "scWfConfiguration", title: "scWfConfiguration", moduleId: "src/viewModels/widgetsShowcase/scWfConfiguration", nav: false, settings: { iconClass: "fa fa-cog" } },
                { route: "scWfMeter", title: "scWfMeter", moduleId: "src/viewModels/widgetsShowcase/scWfMeter", nav: false, settings: { iconClass: "fa fa-cog" } },
                { route: "scWfLogTagAnalytics", title: "scWfLogTagAnalytics", moduleId: "src/viewModels/widgetsShowcase/scWfLogTagAnalytics", nav: false, settings: { iconClass: "fa fa-cog" } },
                { route: "scWfVisibilityContainer", title: "wfVisibilityContainer", moduleId: "src/viewModels/widgetsShowcase/scWfVisibilityContainer", nav: false, settings: { iconClass: "fa fa-cog" } },
                { route: "scWfEnableContainer", title: "wfEnableContainer", moduleId: "src/viewModels/widgetsShowcase/scWfEnableContainer", nav: false, settings: { iconClass: "fa fa-cog" } },
                { route: "scWfToggleButton", title: "wfToggleButton", moduleId: "src/viewModels/widgetsShowcase/scWfToggleButton", nav: false, settings: { iconClass: "fa fa-cog" } },
                { route: "scWfRotationContainer", title: "wfRotationContainer", moduleId: "src/viewModels/widgetsShowcase/scWfRotationContainer", nav: false, settings: { iconClass: "fa fa-cog" } },
                { route: "scWfBufferButton", title: "wfBufferButton", moduleId: "src/viewModels/widgetsShowcase/scWfBufferButton", nav: false, settings: { iconClass: "fa fa-cog" } },
                { route: "scWfRadioButtons", title: "wfRadioButtons", moduleId: "src/viewModels/widgetsShowcase/scWfRadioButtons", nav: false, settings: { iconClass: "fa fa-cog" } },
                //{ route: "overview(/:id)", title: "Overview", moduleId: "src/viewModels/overview", nav: true, hash: '#overview', settings: { iconClass: "wf wf-pointer" } },
                //Add here your navigation routes: pdfSchematas
                // Frames Inkscape -------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                // ***********************************************************************************************************************************************************************************************
                // Frames Lueftungen -------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                { route: "business_lueftungen_laeden_zentrale_L02LueftungZentraleLaedenEGBA_SL01", title: "", moduleId: "src/viewModels/bs_sites/waermekaelte_lueftungen/business/lueftungen/business_lueftungen_laeden_zentrale_L02LueftungZentraleLaedenEGBA_SL01", nav: false, hash: '#business_lueftungen_laeden_zentrale_L02LueftungZentraleLaedenEGBA_SL01', settings: { iconClass: "" } },
                { route: "business_lueftungen_laeden_verteilung_L02LueftungVerteilungLaedenEGBA_SL01", title: "", moduleId: "src/viewModels/bs_sites/waermekaelte_lueftungen/business/lueftungen/business_lueftungen_laeden_verteilung_L02LueftungVerteilungLaedenEGBA_SL01", nav: false, hash: '#business_lueftungen_laeden_verteilung_L02LueftungVerteilungLaedenEGBA_SL01', settings: { iconClass: "" } },
                { route: "business_lueftungen_bueroschule_zentrale_L03LueftungZentraleBueroSchuleBA_SL01", title: "", moduleId: "src/viewModels/bs_sites/waermekaelte_lueftungen/business/lueftungen/business_lueftungen_bueroschule_zentrale_L03LueftungZentraleBueroSchuleBA_SL01", nav: false, hash: '#business_lueftungen_bueroschule_zentrale_L03LueftungZentraleBueroSchuleBA_SL01', settings: { iconClass: "" } },
                { route: "business_lueftungen_bueroschule_verteilung1_L03LueftungVerteilung1BueroSchuleBA_SL01", title: "", moduleId: "src/viewModels/bs_sites/waermekaelte_lueftungen/business/lueftungen/business_lueftungen_bueroschule_verteilung1_L03LueftungVerteilung1BueroSchuleBA_SL01", nav: false, hash: '#business_lueftungen_bueroschule_verteilung1_L03LueftungVerteilung1BueroSchuleBA_SL01', settings: { iconClass: "" } },
                { route: "business_lueftungen_bueroschule_verteilung2_L03LueftungVerteilung2BueroSchuleBA_SL01", title: "", moduleId: "src/viewModels/bs_sites/waermekaelte_lueftungen/business/lueftungen/business_lueftungen_bueroschule_verteilung2_L03LueftungVerteilung2BueroSchuleBA_SL01", nav: false, hash: '#business_lueftungen_bueroschule_verteilung2_L03LueftungVerteilung2BueroSchuleBA_SL01', settings: { iconClass: "" } },
                { route: "business_lueftungen_bueroschule_verteilung3_L03LueftungVerteilung3BueroSchuleBA_SL01", title: "", moduleId: "src/viewModels/bs_sites/waermekaelte_lueftungen/business/lueftungen/business_lueftungen_bueroschule_verteilung3_L03LueftungVerteilung3BueroSchuleBA_SL01", nav: false, hash: '#business_lueftungen_bueroschule_verteilung3_L03LueftungVerteilung3BueroSchuleBA_SL01', settings: { iconClass: "" } },
                { route: "business_lueftungen_kellerallgemein_zentralea2_L05LueftungenKellerAllgemeinA2BA_SL01", title: "", moduleId: "src/viewModels/bs_sites/waermekaelte_lueftungen/business/lueftungen/business_lueftungen_kellerallgemein_zentralea2_L05LueftungenKellerAllgemeinA2BA_SL01", nav: false, hash: '#business_lueftungen_kellerallgemein_zentralea2_L05LueftungenKellerAllgemeinA2BA_SL01', settings: { iconClass: "" } },
                { route: "business_lueftungen_kellerallgemein_zentralea3_L05LueftungenKellerAllgemeinA3BA_SL01", title: "", moduleId: "src/viewModels/bs_sites/waermekaelte_lueftungen/business/lueftungen/business_lueftungen_kellerallgemein_zentralea3_L05LueftungenKellerAllgemeinA3BA_SL01", nav: false, hash: '#business_lueftungen_kellerallgemein_zentralea3_L05LueftungenKellerAllgemeinA3BA_SL01', settings: { iconClass: "" } },
                { route: "business_lueftungen_heizraum_zentrale_L06LueftungHeizraumBA_SL01", title: "", moduleId: "src/viewModels/bs_sites/waermekaelte_lueftungen/business/lueftungen/business_lueftungen_heizraum_zentrale_L06LueftungHeizraumBA_SL01", nav: false, hash: '#business_lueftungen_heizraum_zentrale_L06LueftungHeizraumBA_SL01', settings: { iconClass: "" } },
                { route: "business_lueftungen_parking_parking_LueftungZentraleParkingBA_SL01", title: "", moduleId: "src/viewModels/bs_sites/waermekaelte_lueftungen/business/lueftungen/business_lueftungen_parking_parking_LueftungZentraleParkingBA_SL01", nav: false, hash: '#business_lueftungen_parking_parking_LueftungZentraleParkingBA_SL01', settings: { iconClass: "" } },
                { route: "business_lueftungen_parking_entrauchung_L08EntrauchungParkingBA_SL01", title: "", moduleId: "src/viewModels/bs_sites/waermekaelte_lueftungen/business/lueftungen/business_lueftungen_parking_entrauchung_L08EntrauchungParkingBA_SL01", nav: false, hash: '#business_lueftungen_parking_entrauchung_L08EntrauchungParkingBA_SL01', settings: { iconClass: "" } },
                { route: "business_lueftungen_kellerwohnungen_zentrale_L10LueftungKellerWohnungenBA_SL01", title: "", moduleId: "src/viewModels/bs_sites/waermekaelte_lueftungen/business/lueftungen/business_lueftungen_kellerwohnungen_zentrale_L10LueftungKellerWohnungenBA_SL01", nav: false, hash: '#business_lueftungen_kellerwohnungen_zentrale_L10LueftungKellerWohnungenBA_SL01', settings: { iconClass: "" } },
                { route: "hotel_lueftungen_hotellobbybar_zentrale_L11LueftungZentraleHotellobbyHA_SL01", title: "", moduleId: "src/viewModels/bs_sites/waermekaelte_lueftungen/hotel/lueftungen/hotel_lueftungen_hotellobbybar_zentrale_L11LueftungZentraleHotellobbyHA_SL01", nav: false, hash: '#hotel_lueftungen_hotellobbybar_zentrale_L11LueftungZentraleHotellobbyHA_SL01', settings: { iconClass: "" } },
                { route: "hotel_lueftungen_hotellobbybar_verteilung_L11LueftungHotellobbyBarHA_SL01", title: "", moduleId: "src/viewModels/bs_sites/waermekaelte_lueftungen/hotel/lueftungen/hotel_lueftungen_hotellobbybar_verteilung_L11LueftungHotellobbyBarHA_SL01", nav: false, hash: '#hotel_lueftungen_hotellobbybar_verteilung_L11LueftungHotellobbyBarHA_SL01', settings: { iconClass: "" } },
                { route: "hotel_lueftungen_restaurant_zentrale_L12LueftungZentraleRestaurantHA_SL01", title: "", moduleId: "src/viewModels/bs_sites/waermekaelte_lueftungen/hotel/lueftungen/hotel_lueftungen_restaurant_zentrale_L12LueftungZentraleRestaurantHA_SL01", nav: false, hash: '#hotel_lueftungen_restaurant_zentrale_L12LueftungZentraleRestaurantHA_SL01', settings: { iconClass: "" } },
                { route: "hotel_lueftungen_restaurant_verteilung_L12LueftungVerteilungRestaurantHA_SL01", title: "", moduleId: "src/viewModels/bs_sites/waermekaelte_lueftungen/hotel/lueftungen/hotel_lueftungen_restaurant_verteilung_L12LueftungVerteilungRestaurantHA_SL01", nav: false, hash: '#hotel_lueftungen_restaurant_verteilung_L12LueftungVerteilungRestaurantHA_SL01', settings: { iconClass: "" } },
                { route: "hotel_lueftungen_kueche_zentrale_L13LueftungZentraleKuecheHA_SL03", title: "", moduleId: "src/viewModels/bs_sites/waermekaelte_lueftungen/hotel/lueftungen/hotel_lueftungen_kueche_zentrale_L13LueftungZentraleKuecheHA_SL03", nav: false, hash: '#hotel_lueftungen_kueche_zentrale_L13LueftungZentraleKuecheHA_SL03', settings: { iconClass: "" } },
                { route: "hotel_lueftungen_kueche_verteilung_L13LueftungVerteilungKuecheHA_SL03", title: "", moduleId: "src/viewModels/bs_sites/waermekaelte_lueftungen/hotel/lueftungen/hotel_lueftungen_kueche_verteilung_L13LueftungVerteilungKuecheHA_SL03", nav: false, hash: '#hotel_lueftungen_kueche_verteilung_L13LueftungVerteilungKuecheHA_SL03', settings: { iconClass: "" } },
                { route: "hotel_lueftungen_konferenz_zentrale_L14LueftungZentraleKonferenzHA_SL01", title: "", moduleId: "src/viewModels/bs_sites/waermekaelte_lueftungen/hotel/lueftungen/hotel_lueftungen_konferenz_zentrale_L14LueftungZentraleKonferenzHA_SL01", nav: false, hash: '#hotel_lueftungen_konferenz_zentrale_L14LueftungZentraleKonferenzHA_SL01', settings: { iconClass: "" } },
                { route: "hotel_lueftungen_konferenz_verteilung1_L14LueftungVerteilung1KonferenzHA_SL01", title: "", moduleId: "src/viewModels/bs_sites/waermekaelte_lueftungen/hotel/lueftungen/hotel_lueftungen_konferenz_verteilung1_L14LueftungVerteilung1KonferenzHA_SL01", nav: false, hash: '#hotel_lueftungen_konferenz_verteilung1_L14LueftungVerteilung1KonferenzHA_SL01', settings: { iconClass: "" } },
                { route: "hotel_lueftungen_konferenz_verteilung2_L14LueftungVerteilung2KonferenzHA_SL01", title: "", moduleId: "src/viewModels/bs_sites/waermekaelte_lueftungen/hotel/lueftungen/hotel_lueftungen_konferenz_verteilung2_L14LueftungVerteilung2KonferenzHA_SL01", nav: false, hash: '#hotel_lueftungen_konferenz_verteilung2_L14LueftungVerteilung2KonferenzHA_SL01', settings: { iconClass: "" } },
                { route: "hotel_lueftungen_konferenz_verteilung3_L14LueftungVerteilung3KonferenzHA_SL01", title: "", moduleId: "src/viewModels/bs_sites/waermekaelte_lueftungen/hotel/lueftungen/hotel_lueftungen_konferenz_verteilung3_L14LueftungVerteilung3KonferenzHA_SL01", nav: false, hash: '#hotel_lueftungen_konferenz_verteilung3_L14LueftungVerteilung3KonferenzHA_SL01', settings: { iconClass: "" } },
                { route: "hotel_lueftungen_verwaltung_zentrale_L15LueftungVerteilungVerwaltungHA_SL01", title: "", moduleId: "src/viewModels/bs_sites/waermekaelte_lueftungen/hotel/lueftungen/hotel_lueftungen_verwaltung_zentrale_L15LueftungVerteilungVerwaltungHA_SL01", nav: false, hash: '#hotel_lueftungen_verwaltung_zentrale_L15LueftungVerteilungVerwaltungHA_SL01', settings: { iconClass: "" } },
                { route: "hotel_lueftungen_verwaltung_verteilung1_L15LueftungVerteilungVerwaltungHA_SL01", title: "", moduleId: "src/viewModels/bs_sites/waermekaelte_lueftungen/hotel/lueftungen/hotel_lueftungen_verwaltung_verteilung1_L15LueftungVerteilungVerwaltungHA_SL01", nav: false, hash: '#hotel_lueftungen_verwaltung_verteilung1_L15LueftungVerteilungVerwaltungHA_SL01', settings: { iconClass: "" } },
                { route: "hotel_lueftungen_hotelzimmer_zentrale_L16LueftungZentraleHotelzimmerHA_SL01", title: "", moduleId: "src/viewModels/bs_sites/waermekaelte_lueftungen/hotel/lueftungen/hotel_lueftungen_hotelzimmer_zentrale_L16LueftungZentraleHotelzimmerHA_SL01", nav: false, hash: '#hotel_lueftungen_hotelzimmer_zentrale_L16LueftungZentraleHotelzimmerHA_SL01', settings: { iconClass: "" } },
                { route: "hotel_lueftungen_hotelzimmer_verteilung1_L16LueftungVerteilung01HotelzimmerHA_SL01", title: "", moduleId: "src/viewModels/bs_sites/waermekaelte_lueftungen/hotel/lueftungen/hotel_lueftungen_hotelzimmer_verteilung1_L16LueftungVerteilung01HotelzimmerHA_SL01", nav: false, hash: '#hotel_lueftungen_hotelzimmer_verteilung1_L16LueftungVerteilung01HotelzimmerHA_SL01', settings: { iconClass: "" } },
                { route: "hotel_lueftungen_hotelzimmer_verteilung2_L16LueftungVerteilung02HotelzimmerHA_SL01", title: "", moduleId: "src/viewModels/bs_sites/waermekaelte_lueftungen/hotel/lueftungen/hotel_lueftungen_hotelzimmer_verteilung2_L16LueftungVerteilung02HotelzimmerHA_SL01", nav: false, hash: '#hotel_lueftungen_hotelzimmer_verteilung2_L16LueftungVerteilung02HotelzimmerHA_SL01', settings: { iconClass: "" } },
                { route: "hotel_lueftungen_hotelzimmer_verteilung3_L16LueftungVerteilung03HotelzimmerHA_SL01", title: "", moduleId: "src/viewModels/bs_sites/waermekaelte_lueftungen/hotel/lueftungen/hotel_lueftungen_hotelzimmer_verteilung3_L16LueftungVerteilung03HotelzimmerHA_SL01", nav: false, hash: '#hotel_lueftungen_hotelzimmer_verteilung3_L16LueftungVerteilung03HotelzimmerHA_SL01', settings: { iconClass: "" } },
                { route: "hotel_lueftungen_hotelzimmer_verteilung4_L16LueftungVerteilung04HotelzimmerHA_SL01", title: "", moduleId: "src/viewModels/bs_sites/waermekaelte_lueftungen/hotel/lueftungen/hotel_lueftungen_hotelzimmer_verteilung4_L16LueftungVerteilung04HotelzimmerHA_SL01", nav: false, hash: '#hotel_lueftungen_hotelzimmer_verteilung4_L16LueftungVerteilung04HotelzimmerHA_SL01', settings: { iconClass: "" } },
                { route: "hotel_lueftungen_hotelzimmer_verteilung5_L16LueftungVerteilung05HotelzimmerHA_SL01", title: "", moduleId: "src/viewModels/bs_sites/waermekaelte_lueftungen/hotel/lueftungen/hotel_lueftungen_hotelzimmer_verteilung5_L16LueftungVerteilung05HotelzimmerHA_SL01", nav: false, hash: '#hotel_lueftungen_hotelzimmer_verteilung5_L16LueftungVerteilung05HotelzimmerHA_SL01', settings: { iconClass: "" } },
                { route: "hotel_lueftungen_wcgarderobe_zentrale_L17LueftungZentraleWCGarderobeHA_SL01", title: "", moduleId: "src/viewModels/bs_sites/waermekaelte_lueftungen/hotel/lueftungen/hotel_lueftungen_wcgarderobe_zentrale_L17LueftungZentraleWCGarderobeHA_SL01", nav: false, hash: '#hotel_lueftungen_wcgarderobe_zentrale_L17LueftungZentraleWCGarderobeHA_SL01', settings: { iconClass: "" } },
                { route: "hotel_lueftungen_wcgarderobe_verteilung_L17LueftungVerteilungWCGarderobeHA_SL01", title: "", moduleId: "src/viewModels/bs_sites/waermekaelte_lueftungen/hotel/lueftungen/hotel_lueftungen_wcgarderobe_verteilung_L17LueftungVerteilungWCGarderobeHA_SL01", nav: false, hash: '#hotel_lueftungen_wcgarderobe_verteilung_L17LueftungVerteilungWCGarderobeHA_SL01', settings: { iconClass: "" } },
                { route: "hotel_lueftungen_kellerhotel_keller_L18LueftungZentraleKellerHotelHA_SL01", title: "", moduleId: "src/viewModels/bs_sites/waermekaelte_lueftungen/hotel/lueftungen/hotel_lueftungen_kellerhotel_keller_L18LueftungZentraleKellerHotelHA_SL01", nav: false, hash: '#hotel_lueftungen_kellerhotel_keller_L18LueftungZentraleKellerHotelHA_SL01', settings: { iconClass: "" } },
                { route: "hotel_lueftungen_kellerhotel_edvraum_L19LueftungZentraleEDVRaumHA_SL01", title: "", moduleId: "src/viewModels/bs_sites/waermekaelte_lueftungen/hotel/lueftungen/hotel_lueftungen_kellerhotel_edvraum_L19LueftungZentraleEDVRaumHA_SL01", nav: false, hash: '#hotel_lueftungen_kellerhotel_edvraum_L19LueftungZentraleEDVRaumHA_SL01', settings: { iconClass: "" } },
                { route: "hotel_lueftungen_usvraum_zentrale_L20LueftungZentraleUSVHA_SL01", title: "", moduleId: "src/viewModels/bs_sites/waermekaelte_lueftungen/hotel/lueftungen/hotel_lueftungen_usvraum_zentrale_L20LueftungZentraleUSVHA_SL01", nav: false, hash: '#hotel_lueftungen_usvraum_zentrale_L20LueftungZentraleUSVHA_SL01', settings: { iconClass: "" } },
                { route: "hotel_lueftungen_sturmlueftung_zentrale_L21LueftungZentraleSturmlueftungHA_SL01", title: "", moduleId: "src/viewModels/bs_sites/waermekaelte_lueftungen/hotel/lueftungen/hotel_lueftungen_sturmlueftung_zentrale_L21LueftungZentraleSturmlueftungHA_SL01", nav: false, hash: '#hotel_lueftungen_sturmlueftung_zentrale_L21LueftungZentraleSturmlueftungHA_SL01', settings: { iconClass: "" } },
                { route: "hotel_lueftungen_fitness_ruheraum_L23LueftungVerteilungFitnessH5_SL04", title: "", moduleId: "src/viewModels/bs_sites/waermekaelte_lueftungen/hotel/lueftungen/hotel_lueftungen_fitness_ruheraum_L23LueftungVerteilungFitnessH5_SL04", nav: false, hash: '#hotel_lueftungen_fitness_ruheraum_L23LueftungVerteilungFitnessH5_SL04', settings: { iconClass: "" } },
                { route: "hotel_lueftungen_fitness_fitness_L23LueftungVerteilungFitnessH5_SL04", title: "", moduleId: "src/viewModels/bs_sites/waermekaelte_lueftungen/hotel/lueftungen/hotel_lueftungen_fitness_fitness_L23LueftungVerteilungFitnessH5_SL04", nav: false, hash: '#hotel_lueftungen_fitness_fitness_L23LueftungVerteilungFitnessH5_SL04', settings: { iconClass: "" } },
                { route: "hotel_lueftungen_waescherei_zentrale_L24LueftungZentraleWaeschereiHASL05", title: "", moduleId: "src/viewModels/bs_sites/waermekaelte_lueftungen/hotel/lueftungen/hotel_lueftungen_waescherei_zentrale_L24LueftungZentraleWaeschereiHASL05", nav: false, hash: '#hotel_lueftungen_waescherei_zentrale_L24LueftungZentraleWaeschereiHASL05', settings: { iconClass: "" } },
                { route: "hotel_lueftungen_bankett_zentrale_L25LueftungZentraleBankettH1_SL06", title: "", moduleId: "src/viewModels/bs_sites/waermekaelte_lueftungen/hotel/lueftungen/hotel_lueftungen_bankett_zentrale_L25LueftungZentraleBankettH1_SL06", nav: false, hash: '#hotel_lueftungen_bankett_zentrale_L25LueftungZentraleBankettH1_SL06', settings: { iconClass: "" } },
                { route: "hotel_lueftungen_bankett_verteilung_L25LueftungVerteilungBankettH1_SL06", title: "", moduleId: "src/viewModels/bs_sites/waermekaelte_lueftungen/hotel/lueftungen/hotel_lueftungen_bankett_verteilung_L25LueftungVerteilungBankettH1_SL06", nav: false, hash: '#hotel_lueftungen_bankett_verteilung_L25LueftungVerteilungBankettH1_SL06', settings: { iconClass: "" } },
                // Frames Waerme/Kaelte -------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                { route: "business_heizung_erzeugung_H01WaermeerzeugungBASH01", title: "", moduleId: "src/viewModels/bs_sites/waermekaelte_lueftungen/business/heizung/business_heizung_erzeugung_H01WaermeerzeugungBASH01", nav: false, hash: '#business_heizung_erzeugung_H01WaermeerzeugungBASH01', settings: { iconClass: "" } },
                { route: "business_heizung_heizverteiler_H020304HeizverteilerBusiness", title: "", moduleId: "src/viewModels/bs_sites/waermekaelte_lueftungen/business/heizung/business_heizung_heizverteiler_H020304HeizverteilerBusiness", nav: false, hash: '#business_heizung_heizverteiler_H020304HeizverteilerBusiness', settings: { iconClass: "" } },
                { route: "business_heizung_wohnungen67OG_4H0506BWWFBHWohnungen", title: "", moduleId: "src/viewModels/bs_sites/waermekaelte_lueftungen/business/heizung/business_heizung_wohnungen67OG_4H0506BWWFBHWohnungen", nav: false, hash: '#business_heizung_wohnungen67OG_4H0506BWWFBHWohnungen', settings: { iconClass: "" } },
                { route: "business_kaelteklima_zentrale_K14KaelteverteilerBusiness", title: "", moduleId: "src/viewModels/bs_sites/waermekaelte_lueftungen/business/kaelteklima/business_kaelteklima_zentrale_K14KaelteverteilerBusiness", nav: false, hash: '#business_kaelteklima_zentrale_K14KaelteverteilerBusiness', settings: { iconClass: "" } },
                { route: "business_kaelteklima_kaelteverteilung_K14KaelteverteilerBusiness", title: "", moduleId: "src/viewModels/bs_sites/waermekaelte_lueftungen/business/kaelteklima/business_kaelteklima_kaelteverteilung_K14KaelteverteilerBusiness", nav: false, hash: '#business_kaelteklima_kaelteverteilung_K14KaelteverteilerBusiness', settings: { iconClass: "" } },
                { route: "hotel_heizung_heizverteiler_H0708HeizverteilerHotelHA_SK01", title: "", moduleId: "src/viewModels/bs_sites/waermekaelte_lueftungen/hotel/heizung/hotel_heizung_heizverteiler_H0708HeizverteilerHotelHA_SK01", nav: false, hash: '#hotel_heizung_heizverteiler_H0708HeizverteilerHotelHA_SK01', settings: { iconClass: "" } },
                { route: "hotel_heizung_bwwladung_WarmwasserbereitungHotelHA_SK01", title: "", moduleId: "src/viewModels/bs_sites/waermekaelte_lueftungen/hotel/heizung/hotel_heizung_bwwladung_WarmwasserbereitungHotelHA_SK01", nav: false, hash: '#hotel_heizung_bwwladung_WarmwasserbereitungHotelHA_SK01', settings: { iconClass: "" } },
                { route: "hotel_kaelteklima_klimadecken_KlimadeckenKonferenzHA_SK01", title: "", moduleId: "src/viewModels/bs_sites/waermekaelte_lueftungen/hotel/kaelteklima/hotel_kaelteklima_klimadecken_KlimadeckenKonferenzHA_SK01", nav: false, hash: '#hotel_kaelteklima_klimadecken_KlimadeckenKonferenzHA_SK01', settings: { iconClass: "" } },
                { route: "hotel_kaelteklima_ULKEG1OG_K04K16K17ULKBackofficeEDVEGEDV1OG_SK01", title: "", moduleId: "src/viewModels/bs_sites/waermekaelte_lueftungen/hotel/kaelteklima/hotel_kaelteklima_ULKEG1OG_K04K16K17ULKBackofficeEDVEGEDV1OG_SK01", nav: false, hash: '#hotel_kaelteklima_ULKEG1OG_K04K16K17ULKBackofficeEDVEGEDV1OG_SK01', settings: { iconClass: "" } },
                // Frames Raumautomation -------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                { route: "hotel_h02", title: "", moduleId: "src/viewModels/bs_sites/Raumautomation/hotel_h02", nav: false, hash: '#hotel_h02', settings: { iconClass: "" } },
                { route: "hotel_h03", title: "", moduleId: "src/viewModels/bs_sites/Raumautomation/hotel_h03", nav: false, hash: '#hotel_h03', settings: { iconClass: "" } },
                { route: "hotel_h04", title: "", moduleId: "src/viewModels/bs_sites/Raumautomation/hotel_h04", nav: false, hash: '#hotel_h04', settings: { iconClass: "" } },
                { route: "hotel_h05", title: "", moduleId: "src/viewModels/bs_sites/Raumautomation/hotel_h05", nav: false, hash: '#hotel_h05', settings: { iconClass: "" } },
                { route: "business_h03", title: "", moduleId: "src/viewModels/bs_sites/Raumautomation/business_h03", nav: false, hash: '#business_h03', settings: { iconClass: "" } },
                { route: "business_h04", title: "", moduleId: "src/viewModels/bs_sites/Raumautomation/business_h04", nav: false, hash: '#business_h04', settings: { iconClass: "" } },
                { route: "business_h05", title: "", moduleId: "src/viewModels/bs_sites/Raumautomation/business_h05", nav: false, hash: '#business_h05', settings: { iconClass: "" } },
                { route: "business_vzh056", title: "", moduleId: "src/viewModels/bs_sites/Raumautomation/business_vzh056", nav: false, hash: '#business_vzh056', settings: { iconClass: "" } },
                { route: "fussbodenheizung", title: "", moduleId: "src/viewModels/bs_sites/Raumautomation/fussbodenheizung", nav: false, hash: '#fussbodenheizung', settings: { iconClass: "" } },
                { route: "schaltuhren", title: "", moduleId: "src/viewModels/bs_sites/Raumautomation/schaltuhren", nav: false, hash: '#schaltuhren', settings: { iconClass: "" } },
                { route: "alarmmeldungen", title: "", moduleId: "src/viewModels/bs_sites/Raumautomation/alarmmeldungen", nav: false, hash: '#alarmmeldungen', settings: { iconClass: "" } },
                { route: "alarmaktivierung", title: "", moduleId: "src/viewModels/bs_sites/Raumautomation/alarmaktivierung", nav: false, hash: '#alarmaktivierung', settings: { iconClass: "" } },
                { route: "energiedaten", title: "", moduleId: "src/viewModels/bs_sites/Raumautomation/energiedaten", nav: false, hash: '#energiedaten', settings: { iconClass: "" } },
                // Frames Systemübersicht -------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                { route: "uebersicht", title: "", moduleId: "src/viewModels/bs_sites/systemuebersicht/uebersicht", nav: false, hash: '#uebersicht', settings: { iconClass: "" } },
                { route: "uebersicht_alt", title: "", moduleId: "src/viewModels/bs_sites/systemuebersicht/uebersicht_alt", nav: false, hash: '#uebersicht_alt', settings: { iconClass: "" } },
                { route: "widgets_bs", title: "", moduleId: "src/viewModels/bs_sites/Raumautomation/widgets_bs", nav: false, hash: '#widgets_bs', settings: { iconClass: "" } },
                //{ route: "", title: "", moduleId: "", nav: true, settings: { iconClass: "" } }
                { route: "testsite", title: "Test Site", moduleId: "src/viewModels/TestSite", nav: false, settings: { iconClass: "wf wf-chart-sankey" } },
                { route: "trend", title: "Trending", moduleId: "src/viewModels/Trend", nav: false, settings: { iconClass: "wf wf-analysis" } },
                { route: "logbook", title: "Logbuch", moduleId: "src/viewModels/Logbook", nav: false, settings: { iconClass: "wf-lg wf-book-opened-o" } },
                { route: "alarme", title: "Alarme", moduleId: "src/viewModels/Alarme", nav: false, settings: { iconClass: "wf-lg wf-bell-o" } },
                { route: "pdfSchematas", title: "Elektroschemas", moduleId: "src/viewModels/pdfSchematas", nav: false, settings: { iconClass: "wf-lg wf-tree-structure_5" } },
                { route: "topologie", title: "topologie", moduleId: "src/viewModels/topologie", nav: false, settings: { iconClass: "wf-lg wf-tree-structure_5" } },
                { route: "kontakt", title: "", moduleId: "src/viewModels/kontakt", nav: false, hash: '#kontakt', settings: { iconClass: "" } },
            ]).buildNavigationModel();
            this.checkLocalAppSettings();
            this.info("Application loaded!");
            return router.activate();
        };
        Shell.prototype.setNavbarType = function (typeClass) {
            if (typeClass) {
                this.navbarClass(typeClass);
                $.cookie("wf_navbarClass", typeClass);
            }
            else {
                this.navbarClass(null);
                $.cookie("wf_navbarClass", null);
            }
        };
        // Check if navigation bar class is stored in cookies and use the class it is present
        Shell.prototype.checkLocalAppSettings = function () {
            var storedClassName = $.cookie("wf_navbarClass");
            if (!storedClassName) {
                $.cookie("wf_navbarClass", this.navbarClass(), { expires: 7 });
            }
            else {
                this.navbarClass(storedClassName);
            }
        };
        return Shell;
    }(ViewModelBase));
    return Shell;
});
//# sourceMappingURL=shell.js.map