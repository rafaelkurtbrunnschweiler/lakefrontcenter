define(["require", "exports", "./componentRegistry"], function (require, exports, ComponentRegistry) {
    "use strict";
    var CustomComponentRegistry = (function () {
        function CustomComponentRegistry() {
        }
        CustomComponentRegistry.register = function () {
            // Add here custom component registrations following the pattern:
            // return new ComponentRegistry("src/customComponents")
            //              .mapComponent("componentName")
            return new ComponentRegistry("src/customComponents");
        };
        return CustomComponentRegistry;
    }());
    return CustomComponentRegistry;
});
