var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "./api", "./alarmSource", "./sessionService", "./connectorService"], function (require, exports, Api, AlarmSource, SessionService, ConnectorService) {
    "use strict";
    var OfflineAlarmSource = /** @class */ (function (_super) {
        __extends(OfflineAlarmSource, _super);
        function OfflineAlarmSource(filter, updateRate) {
            var _this = _super.call(this, filter, updateRate) || this;
            _this.resetIdentity = true;
            _this.identityNumber = 0;
            _this.rowNumber = 0;
            _this.hasMore = true;
            return _this;
        }
        OfflineAlarmSource.prototype.clearPolling = function () {
            if (ko.unwrap(this.isPollingEnabled)) {
                return;
            }
            this.alarms([]);
            this.identityNumber = 0;
            this.rowNumber = 0;
            this.hasMore = true;
        };
        OfflineAlarmSource.prototype.startPolling = function () {
            if (ko.unwrap(this.isPollingEnabled) || !this.hasMore) {
                return;
            }
            _super.prototype.startPolling.call(this);
        };
        OfflineAlarmSource.prototype.getAlarms = function () {
            var _this = this;
            this.filterDto.IdentityNumber = this.identityNumber;
            this.filterDto.RowNumber = this.rowNumber;
            ConnectorService.connect()
                .then(function () { return Api.alarmsService.getOfflineAlarms(SessionService.sessionId, SessionService.getClientId(), SessionService.currentLoggedInUser(), SessionService.currentLoggedInUserIsDomainUser(), _this.filterDto, SessionService.timeOut); })
                .then(function (alarm) {
                _this.identityNumber = alarm.IdentityNumber;
                _this.rowNumber = alarm.RowNumber;
                _this.hasMore = alarm.HasMore;
                Array.prototype.push.apply(_this.alarms(), alarm.Alarms); // this works faster as js concat
                _this.alarms.valueHasMutated();
                _this.stopPolling();
            });
        };
        OfflineAlarmSource.prototype.pollData = function (immediate) {
            if (immediate === void 0) { immediate = true; }
            if (!ko.unwrap(this.isPollingEnabled)) {
                return;
            }
            this.getAlarms();
        };
        return OfflineAlarmSource;
    }(AlarmSource));
    return OfflineAlarmSource;
});
//# sourceMappingURL=offlineAlarmSource.js.map