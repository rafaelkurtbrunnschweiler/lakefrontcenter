﻿import OnlineAlarmSource = require("./onlineAlarmSource");
import OfflineAlarmSource = require("./offlineAlarmSource");
import AlarmSource = require("./alarmSource");
import Api = require("./api");
import Logger = require("./logger");
import AlarmsFilter = require("./models/alarmsFilter");
import SessionService = require("./sessionService");
import ConnectorService = require("./connectorService");
import Moment = moment.Moment;

class AlarmsService {
    public static onlineAlarmsUpdateRate = 2000;
    public static offlineAlarmsUpdateRate = 1000;
    public static timeOut = 10000;

    public static getOnlineAlarms(filter: AlarmsFilter, updateRate: number): AlarmSource {
        return new OnlineAlarmSource(filter, updateRate || AlarmsService.onlineAlarmsUpdateRate);
    }


    public static getOfflineAlarms(filter: AlarmsFilter): AlarmSource {
        return new OfflineAlarmSource(filter, AlarmsService.offlineAlarmsUpdateRate);
    }

    public static getAlarmGroups(languageId: number) {
        return ConnectorService.connect()
            .then(() => Api.alarmsService.getAlarmGroups(SessionService.sessionId, SessionService.getClientId(), SessionService.currentLoggedInUser(), SessionService.currentLoggedInUserIsDomainUser(), languageId, SessionService.timeOut));
    }

    public static getAlarmTypes(languageId: number) {
        return ConnectorService.connect()
            .then(() => Api.alarmsService.getAlarmTypes(SessionService.sessionId, SessionService.getClientId(), SessionService.currentLoggedInUser(), SessionService.currentLoggedInUserIsDomainUser(), languageId, SessionService.timeOut));
    }

    public static getAlarms(alarmIds: string[], languageId: number) {
        return ConnectorService.connect()
            .then(() => Api.alarmsService.getAlarms(SessionService.sessionId, SessionService.getClientId(), SessionService.currentLoggedInUser(), SessionService.currentLoggedInUserIsDomainUser(), alarmIds, languageId, window.defaultTimeZone, SessionService.timeOut, true));
    }

    public static acknowledgeAlarms(alarmIds: string[], comment: string): Q.Promise<AcknowledgeResultDTO> {
        Logger.info(AlarmsService, `Acknowledging alarms (${comment})`);

        var securityToken = SessionService.getSecurityToken();
        if (securityToken) {
            return Api.alarmsService.acknowledgeAlarmsByToken(securityToken, alarmIds, comment, SessionService.timeOut);
        } else {
            return ConnectorService.connect()
                .then(() => Api.alarmsService.acknowledgeAlarms(SessionService.sessionId, SessionService.getClientId(), SessionService.currentLoggedInUser(), SessionService.currentLoggedInUserIsDomainUser(), alarmIds, comment, SessionService.timeOut));
        }
    }

    public static acknowledgeAllAlarms(comment: string) {
        Logger.info(AlarmsService, `Acknowledging all alarms ${comment}`);

        var securityToken = SessionService.getSecurityToken();
        if (securityToken) {
            return Api.alarmsService.acknowledgeAllAlarmsByToken(securityToken, comment, SessionService.timeOut);
        } else {
            return ConnectorService.connect()
                .then(() => Api.alarmsService.acknowledgeAllAlarms(SessionService.sessionId, SessionService.getClientId(), SessionService.currentLoggedInUser(), SessionService.currentLoggedInUserIsDomainUser(), comment, SessionService.timeOut));
        }
    }

    public static acknowledgeAllGoneAlarms(comment: string) {
        Logger.info(AlarmsService, `Acknowledging all gone alarms ${comment}`);

        var securityToken = SessionService.getSecurityToken();
        if (securityToken) {
            return Api.alarmsService.acknowledgeAllGoneAlarmsByToken(SessionService.getSecurityToken(), comment, SessionService.timeOut);
        } else {
            return ConnectorService.connect()
                .then(() => Api.alarmsService.acknowledgeAllGoneAlarms(SessionService.sessionId, SessionService.getClientId(), SessionService.currentLoggedInUser(), SessionService.currentLoggedInUserIsDomainUser(), comment, SessionService.timeOut));
        }
    }

    public static acknowledgeAlarmsByGroup(groupName: string, comment: string) {
        Logger.info(AlarmsService, `Acknowledging  in group '${groupName}' (${comment})`);

        var securityToken = SessionService.getSecurityToken();
        if (securityToken) {
            return Api.alarmsService.acknowledgeAlarmsByGroupByToken(SessionService.getSecurityToken(), groupName, comment, SessionService.timeOut);
        } else {
            return ConnectorService.connect()
                .then(() => Api.alarmsService.acknowledgeAlarmsByGroup(SessionService.sessionId, SessionService.getClientId(), SessionService.currentLoggedInUser(), SessionService.currentLoggedInUserIsDomainUser(), groupName, comment, SessionService.timeOut));
        }
    }

    public static setAlarmState(alarmId: string, state: AlarmProcessingAndDisplayState, reactivation: Moment) {
        return Api.alarmsService.setAlarmState(SessionService.getSecurityToken(), alarmId, state, reactivation.toMSDate(), SessionService.timeOut);
    }

    public static setAlarmStates(alarmIds: string[], states: AlarmProcessingAndDisplayState[], reactivations: DateTime[]) {
        return Api.alarmsService.setAlarmStates(SessionService.getSecurityToken(), alarmIds, states, reactivations, SessionService.timeOut);
    }
}

export = AlarmsService;