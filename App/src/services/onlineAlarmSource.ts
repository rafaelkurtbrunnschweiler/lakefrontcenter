﻿import Api = require("./api");
import AlarmSource = require("./alarmSource");
import AlarmsFilter = require("./models/alarmsFilter");
import AlarmsService = require("./alarmsService");
import SessionService = require("./sessionService");
import ConnectorService = require("./connectorService");

class OnlineAlarmSource extends AlarmSource {
    

    constructor(filter: AlarmsFilter, updateRate: number) {
        super(filter, updateRate);
    }

    public clearPolling() {
        throw "Not implemented";
    }

    protected getAlarms(): any {
        ConnectorService.connect()
            .then(() => Api.alarmsService.getOnlineAlarms(SessionService.sessionId, SessionService.getClientId(), SessionService.currentLoggedInUser(), SessionService.currentLoggedInUserIsDomainUser(), this.filterDto, SessionService.timeOut))
            .then((alarm: any) => {
                this.alarms(alarm.Alarms);
                this.pollData(false);
            });
    }
}

export = OnlineAlarmSource;