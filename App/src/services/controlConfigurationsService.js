define(["require", "exports", "./logger", "./api"], function (require, exports, Logger, Api) {
    "use strict";
    var ControlConfigurationsService = /** @class */ (function () {
        function ControlConfigurationsService() {
        }
        ControlConfigurationsService.getControlConfigurationByName = function (name, configurationNamespace, type) {
            Logger.info(ControlConfigurationsService, "getControlConfigurationByName");
            return Api.controlConfigurationsService.getControlConfigurationByName(name, configurationNamespace, type);
        };
        ControlConfigurationsService.getControlConfigurationThatStartWithName = function (name, configurationNamespace, type) {
            Logger.info(ControlConfigurationsService, "getControlConfigurationThatStartWithName");
            return Api.controlConfigurationsService.getControlConfigurationThatStartWithName(name, configurationNamespace, type);
        };
        ControlConfigurationsService.getControlConfigurationsByNamespace = function (configurationNamespace, type) {
            Logger.info(ControlConfigurationsService, "getControlConfigurationsByNamespace");
            return Api.controlConfigurationsService.getControlConfigurationsByNamespace(configurationNamespace, type);
        };
        ControlConfigurationsService.insertControlConfiguration = function (controlConfiguration) {
            Logger.info(ControlConfigurationsService, "insertControlConfiguration");
            return Api.controlConfigurationsService.insertControlConfiguration(controlConfiguration);
        };
        ControlConfigurationsService.updateControlConfiguration = function (controlConfiguration) {
            Logger.info(ControlConfigurationsService, "updateControlConfiguration");
            return Api.controlConfigurationsService.updateControlConfiguration(controlConfiguration);
        };
        ControlConfigurationsService.deleteControlConfiguration = function (id) {
            Logger.info(ControlConfigurationsService, "deleteControlConfiguration");
            return Api.controlConfigurationsService.deleteControlConfiguration(id);
        };
        return ControlConfigurationsService;
    }());
    return ControlConfigurationsService;
});
//# sourceMappingURL=controlConfigurationsService.js.map