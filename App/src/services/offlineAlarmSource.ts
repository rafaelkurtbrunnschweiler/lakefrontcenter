﻿import Api = require("./api");
import AlarmSource = require("./alarmSource");
import AlarmsFilter = require("./models/alarmsFilter");
import AlarmsService = require("./alarmsService");
import SessionService = require("./sessionService");
import ConnectorService = require("./connectorService");

class OfflineAlarmSource extends AlarmSource {
    private resetIdentity = true;

    private identityNumber = 0;
    private rowNumber = 0;
    private hasMore = true;
    
    constructor(filter: AlarmsFilter, updateRate: number) {
        super(filter, updateRate);
    }

    public clearPolling() {
        if (ko.unwrap(this.isPollingEnabled)) {
            return;
        }

        this.alarms([]);
        this.identityNumber = 0;
        this.rowNumber = 0;
        this.hasMore = true;
    }

    public startPolling() {
        if (ko.unwrap(this.isPollingEnabled) || !this.hasMore) {
            return;
        }

        super.startPolling();
    }

    protected getAlarms(): any {
        this.filterDto.IdentityNumber = this.identityNumber;
        this.filterDto.RowNumber = this.rowNumber;

        ConnectorService.connect()
            .then(() => Api.alarmsService.getOfflineAlarms(SessionService.sessionId, SessionService.getClientId(), SessionService.currentLoggedInUser(), SessionService.currentLoggedInUserIsDomainUser(), this.filterDto, SessionService.timeOut))
            .then((alarm) => {

                this.identityNumber = alarm.IdentityNumber;
                this.rowNumber = alarm.RowNumber;
                this.hasMore = alarm.HasMore;

                Array.prototype.push.apply(this.alarms(), alarm.Alarms); // this works faster as js concat
                this.alarms.valueHasMutated();

                this.stopPolling();
            });
    }

    protected pollData(immediate = true): any {
        if (!ko.unwrap(this.isPollingEnabled)) {
            return;
        }

        this.getAlarms();
    }
}

export = OfflineAlarmSource;