define(["require", "exports"], function (require, exports) {
    "use strict";
    var VisualSecurityService = /** @class */ (function () {
        function VisualSecurityService(settings, connector) {
            this.settings = settings;
            this.connector = connector;
        }
        VisualSecurityService.prototype.initialize = function () {
            var _this = this;
            var objectID = ko.unwrap(this.settings.objectID);
            this.enableSignalName = (ko.unwrap(this.settings.enableSignalName) || "").stringPlaceholderResolver(objectID);
            this.visibilitySignalName = (ko.unwrap(this.settings.visibilitySignalName) || "").stringPlaceholderResolver(objectID);
            this.enableSignalValue = ko.unwrap(this.settings.enableSignalValue);
            this.visibilitySignalValue = ko.unwrap(this.settings.visibilitySignalValue);
            this.enableOperator = ko.unwrap(this.settings.enableOperator) || "==";
            this.visibilityOperator = ko.unwrap(this.settings.visibilityOperator) || "==";
            this.visibilitySignal = this.visibilitySignalName ? this.connector.getSignal(this.visibilitySignalName) : null;
            this.enableSignal = this.enableSignalName ? this.connector.getSignal(this.enableSignalName) : null;
            this.isExistVisibleSignalDefinition = ko.observable(false);
            this.isExistEnableSignalDefinition = ko.observable(false);
            this.isVisible = ko.computed(function () {
                if (!_this.visibilitySignal)
                    return true;
                if (!_this.isExistVisibleSignalDefinition())
                    return false;
                if (_this.visibilitySignalValue === undefined || _this.visibilitySignalValue === null)
                    return true;
                return evaluateCondition(_this.visibilitySignal.value(), _this.visibilitySignalValue, _this.visibilityOperator);
            });
            this.isDisabled = ko.computed(function () {
                if (!_this.enableSignal)
                    return false;
                if (!_this.isExistEnableSignalDefinition())
                    return true;
                if (_this.enableSignalValue === undefined || _this.enableSignalValue === null)
                    return false;
                return !evaluateCondition(_this.enableSignal.value(), _this.enableSignalValue, _this.enableOperator);
            });
            if (this.visibilitySignalName)
                this.connector.getSignalsDefinitions([this.visibilitySignalName])
                    .then(function (definitions) {
                    _this.isExistVisibleSignalDefinition(definitions && definitions.length > 0);
                });
            if (this.enableSignalName)
                this.connector.getSignalsDefinitions([this.enableSignalName])
                    .then(function (definitions) {
                    _this.isExistEnableSignalDefinition(definitions && definitions.length > 0);
                });
        };
        VisualSecurityService.prototype.dispose = function () {
            if (this.visibilitySignal !== null) {
                this.connector.unregisterSignals(this.visibilitySignal);
            }
            if (this.enableSignal !== null) {
                this.connector.unregisterSignals(this.enableSignal);
            }
        };
        return VisualSecurityService;
    }());
    return VisualSecurityService;
});
//# sourceMappingURL=visualSecurityService.js.map