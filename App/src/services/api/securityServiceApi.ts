﻿import HttpApi = require("./httpApi");

class SecurityServiceApi extends HttpApi {
    public connectWithToken = (securityToken: string) => this.post<SecuritySessionDTO>("SecurityService", "ConnectWithToken", { securityToken: securityToken });

    public login = (sessionId: string, clientId: string, userName: string, password: string, isDomainUser: boolean, timeOut: number) => this.post<string>("SecurityService", "Login", {
        sessionId: sessionId,
        clientId: clientId,
        userName: userName,
        password: password,
        isDomainUser: isDomainUser,
        millisecondsTimeOut: timeOut
    });
    public isUserLoggedIn = (securityToken: string, timeOut: number) => this.post<boolean>("SecurityService", "IsUserLoggedIn", {
        securityToken: securityToken,
        millisecondsTimeOut: timeOut
    });
    public getCurrentLoggedInUser = (securityToken: string, timeOut: number) => this.post<UserDTO>("SecurityService", "GetCurrentLoggedInUser", {
        securityToken: securityToken,
        millisecondsTimeOut: timeOut
    });
    public logout = (securityToken: string, timeOut: number) => this.post<boolean>("SecurityService", "LogoutByToken", {
        securityToken: securityToken,
        millisecondsTimeOut: timeOut
    });
    public getCurrentUserAuthorizations = (securityToken: string, timeOut: number) => this.post<UserAuthorizationInfo>("SecurityService", "GetCurrentUserAuthorizations", {
        securityToken: securityToken,
        millisecondsTimeOut: timeOut
    });
    public checkProjectAuthorizations = (securityToken: string, authorizations: string[], timeOut: number) => this.post<{[key: string]: boolean}[]>("SecurityService", "CheckProjectAuthorizations", {
        securityToken: securityToken,
        projectAuthorizations: authorizations,
        millisecondsTimeOut: timeOut
    });
    public checkSystemAuthorizations = (securityToken: string, authorizations: string[], timeOut: number) => this.post<{ [key: string]: boolean }[]>("SecurityService", "CheckSystemAuthorizations", {
        securityToken: securityToken,
        projectAuthorizations: authorizations,
        millisecondsTimeOut: timeOut
    });
    public loginWindowsUser = (sessionId: string, clientId: string, timeOut: number) => this.post<string>("NtlmService", "LoginWindowsUser", {
        sessionId: sessionId,
        clientId: clientId,
        millisecondsTimeOut: timeOut
    });
}

export = SecurityServiceApi;