var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "./httpApi"], function (require, exports, HttpApi) {
    "use strict";
    var ControlConfigurationsServiceApi = /** @class */ (function (_super) {
        __extends(ControlConfigurationsServiceApi, _super);
        function ControlConfigurationsServiceApi() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this.getControlConfigurationByName = function (name, configurationNamespace, type) { return _this.post("ConfigurationsService", "GetControlConfigurationByName", {
                name: name,
                configurationNamespace: configurationNamespace,
                type: type
            }); };
            _this.getControlConfigurationThatStartWithName = function (name, configurationNamespace, type) { return _this.post("ConfigurationsService", "GetControlConfigurationThatStartWithName", {
                name: name,
                configurationNamespace: configurationNamespace,
                type: type
            }); };
            _this.getControlConfigurationsByNamespace = function (configurationNamespace, type) { return _this.post("ConfigurationsService", "GetControlConfigurationsByNamespace", {
                configurationNamespace: configurationNamespace,
                type: type
            }); };
            _this.insertControlConfiguration = function (configuration) { return _this.post("ConfigurationsService", "InsertControlConfiguration", {
                configuration: configuration
            }); };
            _this.updateControlConfiguration = function (configuration) { return _this.post("ConfigurationsService", "UpdateControlConfiguration", {
                configuration: configuration
            }); };
            _this.deleteControlConfiguration = function (id) { return _this.post("ConfigurationsService", "DeleteControlConfiguration", {
                id: id
            }); };
            return _this;
        }
        return ControlConfigurationsServiceApi;
    }(HttpApi));
    return ControlConfigurationsServiceApi;
});
//# sourceMappingURL=controlConfigurationsServiceApi.js.map