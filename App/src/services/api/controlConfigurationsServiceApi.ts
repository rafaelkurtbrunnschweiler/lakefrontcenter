﻿import HttpApi = require("./httpApi");

class ControlConfigurationsServiceApi extends HttpApi {

    public getControlConfigurationByName = (name: string, configurationNamespace: string, type: number) => this.post<ControlConfigurationDTO>("ConfigurationsService", "GetControlConfigurationByName", {
        name: name,
        configurationNamespace: configurationNamespace,
        type: type
    });

    public getControlConfigurationThatStartWithName = (name: string, configurationNamespace: string, type: number) => this.post<ControlConfigurationDTO[]>("ConfigurationsService", "GetControlConfigurationThatStartWithName", {
        name: name,
        configurationNamespace: configurationNamespace,
        type: type
    });

    public getControlConfigurationsByNamespace = (configurationNamespace: string, type: number) => this.post<ControlConfigurationDTO[]>("ConfigurationsService", "GetControlConfigurationsByNamespace", {
        configurationNamespace: configurationNamespace,
        type: type
    });

    public insertControlConfiguration = (configuration: ControlConfigurationDTO) => this.post<ControlConfigurationDTO[]>("ConfigurationsService", "InsertControlConfiguration", {
        configuration: configuration
    });

    public updateControlConfiguration = (configuration: ControlConfigurationDTO) => this.post<ControlConfigurationDTO[]>("ConfigurationsService", "UpdateControlConfiguration", {
        configuration: configuration
    });

    public deleteControlConfiguration = (id: string) => this.post("ConfigurationsService", "DeleteControlConfiguration", {
        id: id
    });

}

export = ControlConfigurationsServiceApi;