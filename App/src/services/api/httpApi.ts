﻿import HttpService = require("../http");

class HttpApi {
    protected post<T>(serviceName: string, methodName: string, args: any): Q.Promise<T> {
        var url = `/_SERVICES/WebServices/WCF/${serviceName}.svc/js/${methodName}`;

        return HttpService.post(url, args).then((response : { d: T})  => {
            return response.d;
        });
    }
}

export = HttpApi;