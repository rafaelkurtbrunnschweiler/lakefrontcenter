﻿import HttpApi = require("./httpApi");

class OperationDiaryServiceApi extends HttpApi {

    public getWFEventsByToken = (securityToken: string, wFEventFilter: WFEventFilter, timeOut: number) => this.post<WFEvent[]>("SilverlightTools", "GetWFEventsByToken", {
        securityToken: securityToken,
        filter: wFEventFilter,
        millisecondsTimeOut: timeOut
    });

    public getWFEvents = (sessionId: string, clientId: string, userName: string, isDomainUser: boolean, wFEventFilter: WFEventFilter, timeOut: number) => this.post<WFEvent[]>("SilverlightTools", "GetWFEvents", {
        sessionId: sessionId,
        clientId: clientId,
        userName: userName,
        isDomainUser: isDomainUser,
        filter: wFEventFilter,
        millisecondsTimeOut: timeOut
    });

}

export = OperationDiaryServiceApi;
