define(["require", "exports", "./api/securityServiceApi", "./api/signalsServiceApi", "./api/alarmServiceApi", "./api/symbolicTextsServiceApi", "./api/logbookServiceApi", "./api/operationDiaryServiceApi", "./api/controlConfigurationsServiceApi", "./api/usersServiceApi"], function (require, exports, SecurityServiceApi, ServiceApi, AlarmServiceApi, SymbolicTextsServiceApi, LogbookServiceApi, OperationDiaryServiceApi, ControlConfigurationsServiceApi, UsesServiceApi) {
    "use strict";
    var Api = /** @class */ (function () {
        function Api() {
        }
        Api.securityService = new SecurityServiceApi();
        Api.signalsService = new ServiceApi();
        Api.alarmsService = new AlarmServiceApi();
        Api.symbolicTextsService = new SymbolicTextsServiceApi();
        Api.logbookService = new LogbookServiceApi();
        Api.operationDiaryService = new OperationDiaryServiceApi();
        Api.controlConfigurationsService = new ControlConfigurationsServiceApi();
        Api.usersService = new UsesServiceApi();
        return Api;
    }());
    return Api;
});
//# sourceMappingURL=api.js.map