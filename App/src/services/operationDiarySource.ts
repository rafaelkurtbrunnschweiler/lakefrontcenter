﻿import LogbookFilter = require("./models/logbookFilter");
import Api = require("./api");
import AlarmsService = require("./alarmsService");
import SessionService = require("./sessionService");
import ConnectorService = require("./connectorService");

class OperationDiarySource {
    protected isPollingEnabled = false;
    public logsEntries: KnockoutObservableArray<WFEvent> = ko.observableArray<WFEvent>([])
    protected filterDto: WFEventFilter;
    constructor(public filter: WFEventFilter, public updateRate: number) {
        this.filterDto = filter;
        this.updateRate = updateRate;
    }

    public startPolling() {
        if (this.isPollingEnabled) {
            return;
        }
        this.filterDto = this.filter;
        this.isPollingEnabled = true;
        this.pollData(true);
    }

    public stopPolling() {
        this.isPollingEnabled = false;
    }

    public pollData(immediate = true): any {
        if (!this.isPollingEnabled) {
            return;
        }

        var timeOut = immediate ? 0 : this.updateRate;
        _.delay(() => this.getWFEvents(), timeOut);
    }

    protected getWFEvents() {
        ConnectorService.connect()
            .then(() => Api.operationDiaryService.getWFEvents(SessionService.sessionId, SessionService.getClientId(), SessionService.currentLoggedInUser(), SessionService.currentLoggedInUserIsDomainUser(), this.filterDto, AlarmsService.timeOut))
            .then((logsEntries: any) => {
                this.logsEntries(logsEntries);
                this.pollData(false);
            });
    }



}
export = OperationDiarySource;