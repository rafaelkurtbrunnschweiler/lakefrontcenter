var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
define(["require", "exports", "./models/signal", "./models/nullSignal", "./sessionService", "./connectorService", "./api", "./logger", "./errorCodeService", "./symbolicTextsService"], function (require, exports, Signal, NullSignal, SessionService, ConnectorService, Api, Logger, ErrorCodeService, SymbolicTextsService) {
    "use strict";
    var SignalsService = /** @class */ (function () {
        function SignalsService() {
        }
        SignalsService.getSignal = function (name) {
            if (!name) {
                Logger.warn(SignalsService, "No signal name specified");
                return this.noSignal;
            }
            var signal = SignalsService.registeredSignals[name];
            if (!signal) {
                signal = new Signal(name);
                SignalsService.registeredSignals[name] = signal;
                SignalsService.signalsToRegister.push(signal.signalName());
            }
            signal.addSubscriber();
            return signal;
        };
        SignalsService.unregisterSignals = function (signals) {
            for (var _i = 0, signals_1 = signals; _i < signals_1.length; _i++) {
                var signal = signals_1[_i];
                SignalsService.signalsToUnRegister.push(signal);
            }
            return Q([]);
        };
        SignalsService.unregisterQueuedSignals = function (signals) {
            var signalsToRemove = SignalsService.unregisterClientSignals(signals);
            if (signalsToRemove.length === 0) {
                return Q([]);
            }
            return ConnectorService.connect()
                .then(function () {
                return Api.signalsService.unregisterSignals(SessionService.sessionId, SessionService.getClientId(), signalsToRemove)
                    .then(function () { return SignalsService.signalsToUnRegister.removeAll(); });
            });
        };
        SignalsService.unregisterClientSignals = function (signals) {
            var signalsToRemove = [];
            for (var i = 0; i < signals.length; i++) {
                var requestedSignal = signals[i];
                if (!requestedSignal) {
                    continue;
                }
                var signal;
                if (_.isString(requestedSignal)) {
                    signal = SignalsService.registeredSignals[requestedSignal];
                }
                else {
                    signal = requestedSignal;
                }
                if (!signal) {
                    continue;
                }
                signal.releaseSubscriber();
                var signalName = signal.signalName();
                if (!signal.hasSubscribers() && SignalsService.registeredSignals[signalName]) {
                    if (signalName && signalName.length) {
                        signalsToRemove.push(signalName);
                    }
                    delete SignalsService.registeredSignals[signalName];
                }
            }
            return signalsToRemove;
        };
        SignalsService.getOnlineUpdates = function () {
            return ConnectorService.connect()
                .then(function (value) {
                return value !== undefined ? SignalsService.registerSignals() : Q(false);
            })
                .then(function (value) {
                if (value)
                    SignalsService.startGettingUpdates();
            });
        };
        SignalsService.readSignals = function (signalNames) {
            return ConnectorService.connect()
                .then(function () { return Api.signalsService.readSignals(SessionService.sessionId, SessionService.getClientId(), signalNames); })
                .fail(Logger.handleError(SignalsService));
        };
        SignalsService.writeSignals = function (signalValues) {
            var securityToken = SessionService.getSecurityToken();
            var values = _.map(signalValues, function (value, key) {
                return {
                    key: key,
                    value: value
                };
            });
            var currentUser = SessionService.currentLoggedInUser();
            return ConnectorService.connect()
                .then(function () {
                if (securityToken && currentUser) {
                    return Api.signalsService.writeSecuredSignals(values, securityToken, SessionService.getClientId());
                }
                else {
                    return Api.signalsService.writeUnsecuredSignals(values, SessionService.sessionId, SessionService.getClientId());
                }
            })
                .then(SignalsService.handleWriteResponse)
                .fail(Logger.handleError(SignalsService));
        };
        SignalsService.getLogIds = function (signalLogTags) {
            var logTags = _.map(signalLogTags, function (tag) {
                return {
                    SignalID: tag.signalId(),
                    LogTag: tag.logTag()
                };
            });
            return Api.signalsService.getLogIds(SessionService.sessionId, SessionService.getClientId(), SessionService.currentLoggedInUser(), SessionService.currentLoggedInUserIsDomainUser(), logTags, SessionService.timeOut)
                .fail(Logger.handleError(SignalsService));
        };
        SignalsService.getLogValues = function (filter) {
            return Api.signalsService.getLogValues(SessionService.sessionId, SessionService.getClientId(), SessionService.currentLoggedInUser(), SessionService.currentLoggedInUserIsDomainUser(), filter.toDto(), SessionService.timeOut)
                .then(function (values) {
                for (var i = 0; i < values.length; i++) {
                    values[i].EntriesDate = moment(values[i].EntriesDate).toDate();
                }
                return values;
            })
                .fail(Logger.handleError(SignalsService));
        };
        //public static getDateWithoutTimezoneOffset(str: string): Date {
        //    var res = str.match(/\/Date\((\d+)(?:([+-])(\d\d)(\d\d))?\)\//);
        //    if (res == null)
        //        return new Date(NaN); // or something that indicates it was not a DateString
        //    var time = parseInt(res[1], 10);
        //    //if (res[2] && res[3] && res[4]) {
        //    //    var dir = res[2] === "+" ? -1 : +1,
        //    //        h = parseInt(res[3], 10),
        //    //        m = parseInt(res[4], 10);
        //    //    time += dir * (h * 60 + m) * 60000;
        //    //}
        //    return new Date(time);
        //}
        SignalsService.updateLogValue = function (logId, entryDate, value, value2) {
            return Api.signalsService.updateLogValue(SessionService.sessionId, SessionService.getClientId(), SessionService.currentLoggedInUser(), SessionService.currentLoggedInUserIsDomainUser(), logId, entryDate, value, value2, SessionService.timeOut);
        };
        SignalsService.getLastValuesBeforeDate = function (logTags, date) {
            return Api.signalsService.getLastValuesBeforeDate(SessionService.sessionId, SessionService.getClientId(), SessionService.currentLoggedInUser(), SessionService.currentLoggedInUserIsDomainUser(), logTags, date.toMSDateTimeOffset(), SessionService.timeOut);
        };
        SignalsService.createUpdateRequest = function (prevRequestId, prevResponseId) {
            if (prevRequestId === void 0) { prevRequestId = 0; }
            if (prevResponseId === void 0) { prevResponseId = 0; }
            var requestId = SignalsService.getNextRequestId(prevRequestId, prevResponseId);
            SignalsService.updateRequest = {
                sessionId: SessionService.sessionId,
                clientId: SessionService.getClientId(),
                requestId: requestId
            };
        };
        SignalsService.getNextRequestId = function (prevRequestId, prevResponseId) {
            if (prevResponseId === 0) {
                return 1;
            }
            if (prevResponseId === prevRequestId) {
                return prevRequestId % 1000 + 1;
            }
            return 0;
        };
        SignalsService.handleWriteResponse = function (response) {
            var errorCode = response[0];
            if (!errorCode) {
                return 0;
            }
            else {
                var errorKey = errorCode.toString();
                var translation = SymbolicTextsService.translate(ErrorCodeService.signalWriteErrorCodes[errorKey])();
                // ReSharper disable once ImplicitAnyTypeWarning
                throw "Code " + errorCode + ": " + translation;
            }
        };
        SignalsService.registerSignals = function () {
            var signalNames = SignalsService.signalsToRegister;
            SignalsService.signalsToRegister = [];
            if (!signalNames.length) {
                Logger.info(SignalsService, "Signals are already registered, skipping");
                return Q(true);
            }
            Logger.info(SignalsService, "Registering signals: " + signalNames);
            var sessionId = SessionService.sessionId;
            var clientId = SessionService.getClientId();
            return Api.signalsService.registerSignals(sessionId, clientId, signalNames)
                .then(function (results) { return SignalsService.onSignalsRegistered(signalNames, results); })
                .fail(Logger.handleError(SignalsService));
        };
        SignalsService.onSignalsRegistered = function (signalNames, results) {
            var successfull = [];
            var warnings = [];
            var errors = [];
            for (var i = 0; i < signalNames.length; i++) {
                if (results[i] > 0) {
                    warnings.push({
                        signalName: signalNames[i],
                        code: results[i]
                    });
                }
                else if (results[i] < 0) {
                    errors.push({
                        signalName: signalNames[i],
                        code: results[i]
                    });
                }
                else {
                    successfull.push({
                        signalName: signalNames[i],
                        code: results[i]
                    });
                }
            }
            if (successfull.length) {
                Logger.info(SignalsService, SignalsService.buildSignalRegistrationMessage("Successfully registered", successfull));
            }
            if (warnings.length) {
                Logger.warn(SignalsService, SignalsService.buildSignalRegistrationMessage("Encountered warnings when registering", warnings));
            }
            if (errors.length) {
                var error = SignalsService.buildSignalRegistrationMessage("Failed to register", errors);
                throw error;
            }
            return true;
        };
        SignalsService.buildSignalRegistrationMessage = function (message, results) {
            var result = message + " " + results.length + " signals:";
            var signalCodes = _.map(results, function (r) { return r.signalName + " (" + r.code + ")"; }).join("\n");
            if (signalCodes.length > 0) {
                result += "\n";
                result += signalCodes;
            }
            return result;
        };
        SignalsService.startGettingUpdates = function () {
            if (!SignalsService.getUpdates) {
                SignalsService.createUpdateRequest();
                SignalsService.doUpdate();
            }
            return SignalsService.getUpdates;
        };
        SignalsService.doUpdate = function () {
            var request = SignalsService.updateRequest;
            SignalsService.getUpdates = Api.signalsService.getUpdates(request.sessionId, request.clientId, request.requestId)
                .then(SignalsService.updateSignals)
                .fail(function (error) {
                if (!SignalsService.lastUpdateError()) {
                    SignalsService.lastUpdateError(error);
                    Logger.handleError(SignalsService)(error);
                }
                return _.delay(function () { return SignalsService.doUpdate(); }, SignalsService.updateInterval);
            });
        };
        SignalsService.updateSignals = function (update) {
            SignalsService.lastUpdateError(null);
            if (!update) {
                return;
            }
            var responseId = update.ResponseId;
            var updates = update.Updates;
            for (var i = 0; i < updates.length; i++) {
                var item = updates[i];
                var signal = SignalsService.registeredSignals[item.key];
                if (!signal) {
                    continue;
                }
                var value = item.value;
                signal.value(value);
            }
            _.delay(function () {
                SignalsService.createUpdateRequest(SignalsService.updateRequest.requestId, responseId);
                SignalsService.doUpdate();
            }, SignalsService.updateInterval);
        };
        SignalsService.getLogStatistics = function (filter) {
            return Api.signalsService.getLogStatistics(SessionService.sessionId, SessionService.getClientId(), SessionService.currentLoggedInUser(), SessionService.currentLoggedInUserIsDomainUser(), filter, SessionService.timeOut)
                .fail(function () {
                Logger.handleError(SignalsService);
                return [];
            });
        };
        SignalsService.writeSignalsSecure = function (userPassword, signalValues) {
            return __awaiter(this, void 0, void 0, function () {
                var securityToken, values;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            securityToken = SessionService.getSecurityToken();
                            values = _.map(signalValues, function (value, key) {
                                return {
                                    Name: key,
                                    Value: value
                                };
                            });
                            return [4 /*yield*/, ConnectorService.connect()];
                        case 1:
                            _a.sent();
                            return [2 /*return*/, Api.signalsService.writeSignalsSecure(securityToken, userPassword, values)];
                    }
                });
            });
        };
        SignalsService.updateInterval = 500;
        SignalsService.registeredSignals = [];
        SignalsService.signalsToRegister = [];
        SignalsService.signalsToUnRegister = ko.observableArray([]);
        SignalsService.queuedSignalsToUnRegister = ko.computed(function () { return SignalsService.signalsToUnRegister(); }).extend({ throttle: 250 });
        SignalsService.subscription = SignalsService.queuedSignalsToUnRegister.subscribe(function () { return SignalsService.unregisterQueuedSignals(SignalsService.queuedSignalsToUnRegister()); }, SignalsService);
        SignalsService.noSignal = new NullSignal();
        SignalsService.lastUpdateError = ko.observable(null);
        return SignalsService;
    }());
    return SignalsService;
});
//# sourceMappingURL=signalsService.js.map