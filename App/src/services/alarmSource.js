define(["require", "exports"], function (require, exports) {
    "use strict";
    var AlarmSource = /** @class */ (function () {
        function AlarmSource(filter, updateRate) {
            var _this = this;
            this.filter = filter;
            this.updateRate = updateRate;
            this.isPollingEnabled = ko.observable(false);
            this.alarms = ko.observableArray([]);
            this.isPolling = ko.computed(function () {
                return _this.isPollingEnabled();
            });
        }
        AlarmSource.prototype.startPolling = function () {
            if (ko.unwrap(this.isPollingEnabled)) {
                return;
            }
            this.filterDto = this.filter.toDto();
            this.isPollingEnabled(true);
            this.pollData(true);
        };
        AlarmSource.prototype.stopPolling = function () {
            this.isPollingEnabled(false);
        };
        AlarmSource.prototype.pollData = function (immediate) {
            var _this = this;
            if (immediate === void 0) { immediate = true; }
            if (!ko.unwrap(this.isPollingEnabled)) {
                return;
            }
            var timeOut = immediate ? 0 : this.updateRate;
            _.delay(function () { return _this.getAlarms(); }, timeOut);
        };
        AlarmSource.prototype.getAlarms = function () {
            throw "Not implemented";
        };
        AlarmSource.prototype.getFilterDto = function () {
            var filter = this.filter;
            var timeZone = window.defaultTimeZone;
            var dto = {
                LanguageID: filter.languageId(),
                AlarmGroups: filter.alarmGroups(),
                AlarmTypes: filter.alarmTypes(),
                MinimumPriority: filter.minimumPriority(),
                MaximumPriority: filter.maximumPriority(),
                SortOrder: filter.sortOrder(),
                MaxRowCount: filter.maxRowCount(),
                AlarmStatusFilter: filter.alarmStatusFilter(),
                StartTime: moment(filter.startTime()).toMSDateTimeOffset(),
                EndTime: moment(filter.endTime()).toMSDateTimeOffset(),
                Column: filter.column(),
                ColumnFilters: filter.columnFilters(),
                FilterAlarmGroupsByUser: filter.filterAlarmGroupsByUser(),
                UserName: filter.userName(),
                TimeZoneID: timeZone
            };
            return dto;
        };
        return AlarmSource;
    }());
    return AlarmSource;
});
//# sourceMappingURL=alarmSource.js.map