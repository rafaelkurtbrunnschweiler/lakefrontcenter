define(["require", "exports", "./sessionService", "./logger", "./api", "./connectorService"], function (require, exports, SessionService, Logger, Api, ConnectorService) {
    "use strict";
    var UsersService = /** @class */ (function () {
        function UsersService() {
        }
        UsersService.getCurrentUserDetails = function () {
            Logger.info(UsersService, "getCurrentUserDetails");
            return ConnectorService.connect()
                .then(function () { return Api.usersService.getCurrentUserDetails(SessionService.sessionId, SessionService.getClientId()); });
        };
        UsersService.changeUserPassword = function (affectedUserId, newPassword) {
            Logger.info(UsersService, "changeUserPassword");
            return ConnectorService.connect()
                .then(function () { return Api.usersService.changeUserPasswordByToken(SessionService.getSecurityToken(), affectedUserId, newPassword, UsersService.timeOut); });
        };
        UsersService.changeCurrentUserPassword = function (currentPassword, newPassword) {
            Logger.info(UsersService, "changeCurrentUserPassword");
            return ConnectorService.connect()
                .then(function () { return Api.usersService.changeCurrentUserPasswordByToken(SessionService.getSecurityToken(), currentPassword, newPassword, UsersService.timeOut); });
        };
        UsersService.timeOut = 10000;
        return UsersService;
    }());
    return UsersService;
});
//# sourceMappingURL=usersService.js.map