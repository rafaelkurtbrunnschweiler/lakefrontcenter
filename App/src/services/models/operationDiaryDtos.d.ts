﻿interface WFEventFilter {
    AlarmFilter: WFAlarmEventFilter,
    ServerFilter: WFEventCategoryFilter,
    UserFilter: WFUserEventFilter,
    AffectedUserFilter: WFAffectedUserEventFilter,
    LanguageID: number,
    TimePeriod: TimePeriod,
    MaxEvents: number
}

interface WFAlarmEventFilter extends WFEventCategoryFilter {
    AlarmTypeFilter: GenericItemListFilter<AlarmType>,
    AlarmGroupFilter: GenericItemListFilter<AlarmGroup>,
    AlarmPriorityFilter: AlarmPriorityFilter
}

interface WFEventCategoryFilter extends AbstractFilter {
    EventResultFilter: GenericItemListFilter<WFEventResult>,
    EventTypeFilter: GenericItemListFilter<WFEventType>
}

interface WFUserEventFilter extends WFEventCategoryFilter {
    UserFilter: GenericItemListFilter<User>
}

interface WFAffectedUserEventFilter extends WFEventCategoryFilter {
    UserFilter: GenericItemListFilter<User>,
    CustomUserNameFilter: string
}

interface AlarmPriorityFilter extends AbstractFilter {
    From: number,
    To: number
}

interface AbstractFilter {
    Action: FilterAction
}

interface GenericItemListFilter<T> extends AbstractFilter {
    Items: T[]
}

//DTOs
interface TimePeriod {
    Start: moment.MSDateTimeOffset,
    End: moment.MSDateTimeOffset
}

interface DomainObject {
    ID: string
    Version: number
}

interface User extends DomainObject {
    Name: string
}

interface SymbolicTextBasedObject extends DomainObject {
    Text: SymbolicText
}

interface SymbolicText extends DomainObject {
    Name: string,
    Translation: string
}

interface AlarmGroup extends SymbolicTextBasedObject {
}

interface AlarmType extends SymbolicTextBasedObject {
}

interface WFEvent extends DomainObject {
    Type: WFEventType,
    Time: DateTime,
    SystemTime: DateTime,
    Alarm: Alarm,
    ClientMachine: string,
    Server: Server,
    Signal: Signal,
    User: User,
    SignalOldValue: string,
    SignalNewValue: string,
    AlarmAcknoledgeText: string,
    ResultCode: number,
    AffectedUserName: string,
    EventData: string
}


interface Alarm extends SymbolicTextBasedObject {
    Type: AlarmType,
    Group: AlarmGroup,
    Signal: Signal,
    Symbol: number,
    Priority: number,
    Value1?: number,
    Value2?: number,
    Value3?: number,
    Value4?: number,
    Value5?: number,
    Value6?: number,
    Value7?: number,
    Value8?: number,
    Value9?: number,
    Value10?: number,
    Value11?: number,
    Value12?: number,
    Value13?: number,
    Value14?: number,
    Value15?: number,
    Value16?: number,
    Value17?: number,
    Value18?: number,
    Value19?: number,
    Value20?: number
}

interface Signal extends DomainObject {
    AliasName: string,
    Description: string,
    Unit: string,
    Name: string,
    Group: SignalGroup
}

interface SignalGroup extends DomainObject {
    Name: string,
    Connector: Connector
}

interface Connector extends DomainObject {
    Descriptio: string,
    Server: Server
}

interface Server extends DomainObject {
    Name: string
}

//Enums
declare enum EventType {
    AlarmOn = 1,
    AlarmOff = 2,
    AlarmAcknowledged = 3,
}

declare enum WFEventType {
    AlarmOn = 1,
    AlarmOff = 2,
    AlarmAcknowledged = 3,
    ServerStarted = 4,
    ServerStopped = 5,
    UserLoggedIn = 6,
    UserLoggedOut = 7,
    UserWroteSignal = 8,
    AlarmActivated = 9,
    AlarmDeactivated = 10,
    UserCreated = 11,
    UserModified = 12,
    UserDeleted = 13,
    UserPasswordChanged = 14
}

declare enum FilterAction {
    IncludeAll,
    IncludeSome,
    IncludeNone,
}

declare enum WFEventResult {
    Successfull,
    Unsuccessfull
}

declare enum ExceptionType {
    Concurrency,
    Domain,
    Other
}
