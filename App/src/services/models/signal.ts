﻿import Logger = require("../logger");

class Signal {
    public signalName = ko.observable<string>();
    public value = ko.observable<any>();
    public definition = ko.observable<any>();

    private referenceCount: number = 0;

    constructor(name: string, defaultValue: any = "n/a") {
        this.signalName(name);
        this.value(defaultValue);
    }

    public addSubscriber() {
        Logger.info(this, `Subscribed to ${this.signalName()} [total subscriptions: ${this.referenceCount}]`);
        this.referenceCount++;
    }

    public releaseSubscriber() {
        this.referenceCount = Math.max(this.referenceCount - 1, 0);
        Logger.info(this, `Unsubscribed from ${this.signalName() } [total subscriptions: ${this.referenceCount}]`);
    }

    public hasSubscribers() {
        return this.referenceCount > 0;
    }
}

export = Signal;