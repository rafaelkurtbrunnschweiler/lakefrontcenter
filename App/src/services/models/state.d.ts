﻿interface IState {
    stateSignalName1?: string | KnockoutObservable<string>;
    stateSignalName2?: string | KnockoutObservable<string>;
    stateSignalName3?: string | KnockoutObservable<string>;
    stateSignalName4?: string | KnockoutObservable<string>;
    stateSignalName5?: string | KnockoutObservable<string>;
    stateSignalName6?: string | KnockoutObservable<string>;
    stateSignalName7?: string | KnockoutObservable<string>;
    stateSignalName8?: string | KnockoutObservable<string>;
    stateSignalNames?: string[] | KnockoutObservable<string>[];

    conditionRule1?: string;
    conditionRule2?: string;
    conditionRule3?: string;
    conditionRule4?: string;
    conditionRule5?: string;
    conditionRule6?: string;
    conditionRule7?: string;
    conditionRule8?: string;
    conditionRules?: string[];

    maskSignal1?: string;
    maskSignal2?: string;
    maskSignal3?: string;
    maskSignal4?: string;
    maskSignal5?: string;
    maskSignal6?: string;
    maskSignal7?: string;
    maskSignal8?: string;
    maskSignals?: string[];

    operator1?: string;
    operator2?: string;
    operator3?: string;
    operator4?: string;
    operator5?: string;
    operator6?: string;
    operator7?: string;
    operator8?: string;
    operators?: string[];

    objectID?: string;
    writeToBuffer: boolean;
    states?: IStateModel[];
}

interface IStateModel {
    signalName?: string | KnockoutObservable<string>;
    maskSignal?: string;
    conditionRule?: string;
    operator?: string;
    cssClassName?: string;
}