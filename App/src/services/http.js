define(["require", "exports", "./serialization"], function (require, exports, SerializationService) {
    "use strict";
    var SysHttp = /** @class */ (function () {
        function SysHttp() {
        }
        /**
         * Converts the data to JSON.
         * @method toJSON
         * @param {object} data The data to convert to JSON.
         * @return {string} JSON.
         */
        SysHttp.toJSON = function (data) {
            return ko.toJSON(data);
        };
        /**
         * Makes an HTTP GET request.
         * @method get
         * @param {string} url The url to send the get request to.
         * @param {object} [query] An optional key/value object to transform into query string parameters.
         * @param {object} [headers] The data to add to the request header.  It will be converted to JSON. If the data contains Knockout observables, they will be converted into normal properties before serialization.
         * @return {Promise} A promise of the get response data.
         */
        SysHttp.get = function (url, query, headers) {
            return $.ajax(url, { data: query, headers: ko.toJS(headers) });
        };
        /**
         * Makes an JSONP request.
         * @method jsonp
         * @param {string} url The url to send the get request to.
         * @param {object} [query] An optional key/value object to transform into query string parameters.
         * @param {string} [callbackParam] The name of the callback parameter the api expects (overrides the default callbackParam).
         * @param {object} [headers] The data to add to the request header.  It will be converted to JSON. If the data contains Knockout observables, they will be converted into normal properties before serialization.
         * @return {Promise} A promise of the response data.
         */
        SysHttp.jsonp = function (url, query, callbackParam, headers) {
            if (url.indexOf('=?') == -1) {
                callbackParam = callbackParam || SysHttp.callbackParam;
                if (url.indexOf('?') == -1) {
                    url += '?';
                }
                else {
                    url += '&';
                }
                url += callbackParam + '=?';
            }
            return $.ajax({
                url: url,
                dataType: 'jsonp',
                data: query,
                headers: ko.toJS(headers)
            });
        };
        /**
         * Makes an HTTP PUT request.
         * @method put
         * @param {string} url The url to send the put request to.
         * @param {object} data The data to put. It will be converted to JSON. If the data contains Knockout observables, they will be converted into normal properties before serialization.
         * @param {object} [headers] The data to add to the request header.  It will be converted to JSON. If the data contains Knockout observables, they will be converted into normal properties before serialization.
         * @return {Promise} A promise of the response data.
         */
        SysHttp.put = function (url, data, headers) {
            return $.ajax({
                url: url,
                data: this.toJSON(data),
                type: 'PUT',
                contentType: 'application/json',
                dataType: 'json',
                headers: ko.toJS(headers)
            });
        };
        /**
         * Makes an HTTP POST request.
         * @method post
         * @param {string} url The url to send the post request to.
         * @param {object} data The data to post. It will be converted to JSON. If the data contains Knockout observables, they will be converted into normal properties before serialization.
         * @param {object} [headers] The data to add to the request header.  It will be converted to JSON. If the data contains Knockout observables, they will be converted into normal properties before serialization.
         * @return {Promise} A promise of the response data.
         */
        SysHttp.post = function (url, data, headers) {
            return $.ajax({
                url: url,
                data: this.toJSON(data),
                type: 'POST',
                contentType: 'application/json',
                dataType: 'json',
                headers: ko.toJS(headers)
            });
        };
        /**
         * Makes an HTTP DELETE request.
         * @method remove
         * @param {string} url The url to send the delete request to.
         * @param {object} [query] An optional key/value object to transform into query string parameters.
         * @param {object} [headers] The data to add to the request header.  It will be converted to JSON. If the data contains Knockout observables, they will be converted into normal properties before serialization.
         * @return {Promise} A promise of the get response data.
         */
        SysHttp.remove = function (url, query, headers) {
            return $.ajax({
                url: url,
                data: query,
                type: 'DELETE',
                headers: ko.toJS(headers)
            });
        };
        /**
             * The name of the callback parameter to inject into jsonp requests by default.
             * @property {string} callbackParam
             * @default callback
             */
        SysHttp.callbackParam = 'callback';
        return SysHttp;
    }());
    var HttpService = /** @class */ (function () {
        function HttpService() {
        }
        HttpService.get = function (url, query, headers) {
            url = window.resolveUrl(url);
            //query = _.extend(query || {}, {
            //    __c: window.cacheKey
            //});
            return Q(SysHttp.get(url, query, headers))
                .then(function (result) { return SerializationService.retrocycle(result); });
        };
        HttpService.post = function (url, query, headers) {
            url = window.resolveUrl(url);
            //query = _.extend(query || {}, {
            //    __c: window.cacheKey
            //});
            return Q(SysHttp.post(url, query, headers))
                .then(function (result) { return SerializationService.retrocycle(result); });
        };
        HttpService.put = function (url, query, headers) {
            url = window.resolveUrl(url);
            //query = _.extend(query || {}, {
            //    __c: window.cacheKey
            //});
            return Q(SysHttp.put(url, query, headers))
                .then(function (result) { return SerializationService.retrocycle(result); });
        };
        HttpService.remove = function (url, query, headers) {
            url = window.resolveUrl(url);
            //query = _.extend(query || {}, {
            //    __c: window.cacheKey
            //});
            return Q(SysHttp.remove(url, query, headers))
                .then(function (result) { return SerializationService.retrocycle(result); });
        };
        HttpService.jsonp = function (url, query, callbackParam, headers) {
            url = window.resolveUrl(url);
            //query = _.extend(query || {}, {
            //    __c: window.cacheKey
            //});
            return Q(SysHttp.jsonp(url, query, callbackParam, headers))
                .then(function (result) { return SerializationService.retrocycle(result); });
        };
        return HttpService;
    }());
    return HttpService;
});
//# sourceMappingURL=http.js.map