﻿import SignalsService = require("./signalsService");

class SignalsBufferService {
    private static signalsToWrite: any = ko.observable({});

    public static bufferIsEmpty = ko.computed(() => {
        return Object.keys(SignalsBufferService.signalsToWrite()).length === 0;
    });

    public static writeSignalsToBuffer(signalValues: SignalValue[]) {
        _.forEach(Object.keys(signalValues), (key: any, index: any, array: any) => {
            var value = SignalsBufferService.signalsToWrite();
            value[key] = signalValues[key]
            SignalsBufferService.signalsToWrite(value);
        });
    }

    public static writeSignalsFromBuffer() {
        SignalsService.writeSignals(SignalsBufferService.signalsToWrite())
            .then((a) => {
                SignalsBufferService.signalsToWrite({});
            });
    }

    public static async writeSignalsFromBufferSecure(userPassword: string) {
        const result = await SignalsService.writeSignalsSecure(userPassword, SignalsBufferService.signalsToWrite())
        if (result !== undefined) {
            SignalsBufferService.signalsToWrite({});
            return Q(true);
        }
        else
            return Q(false);
    }

    public static existSignalInBuffer(signalName: string) {
        return _.contains(Object.keys(SignalsBufferService.signalsToWrite()), signalName);
    }

    public static existSignalsInBuffer(signalName: string[]) {
        return _.intersection(Object.keys(SignalsBufferService.signalsToWrite()), signalName).length > 0;
    }

    public static clearSignalBuffer() {
        SignalsBufferService.signalsToWrite({});
    }

    public static getSignalsFromBuffer() {
        return _.map(SignalsBufferService.signalsToWrite(), (value: any, key: any) => {
            return <KeyValuePair<string, any>>{
                key: key,
                value: value
            };
        });
    }

    public static readSignals(signalNames: string[]) {

        var result = [];

        for (var i = 0; i < signalNames.length; i++)
            if (this.existSignalInBuffer(signalNames[i]))
                result.push(SignalsBufferService.signalsToWrite()[signalNames[i]]);

        return result;
    }
}
export = SignalsBufferService;