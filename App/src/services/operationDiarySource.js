define(["require", "exports", "./api", "./alarmsService", "./sessionService", "./connectorService"], function (require, exports, Api, AlarmsService, SessionService, ConnectorService) {
    "use strict";
    var OperationDiarySource = /** @class */ (function () {
        function OperationDiarySource(filter, updateRate) {
            this.filter = filter;
            this.updateRate = updateRate;
            this.isPollingEnabled = false;
            this.logsEntries = ko.observableArray([]);
            this.filterDto = filter;
            this.updateRate = updateRate;
        }
        OperationDiarySource.prototype.startPolling = function () {
            if (this.isPollingEnabled) {
                return;
            }
            this.filterDto = this.filter;
            this.isPollingEnabled = true;
            this.pollData(true);
        };
        OperationDiarySource.prototype.stopPolling = function () {
            this.isPollingEnabled = false;
        };
        OperationDiarySource.prototype.pollData = function (immediate) {
            var _this = this;
            if (immediate === void 0) { immediate = true; }
            if (!this.isPollingEnabled) {
                return;
            }
            var timeOut = immediate ? 0 : this.updateRate;
            _.delay(function () { return _this.getWFEvents(); }, timeOut);
        };
        OperationDiarySource.prototype.getWFEvents = function () {
            var _this = this;
            ConnectorService.connect()
                .then(function () { return Api.operationDiaryService.getWFEvents(SessionService.sessionId, SessionService.getClientId(), SessionService.currentLoggedInUser(), SessionService.currentLoggedInUserIsDomainUser(), _this.filterDto, AlarmsService.timeOut); })
                .then(function (logsEntries) {
                _this.logsEntries(logsEntries);
                _this.pollData(false);
            });
        };
        return OperationDiarySource;
    }());
    return OperationDiarySource;
});
//# sourceMappingURL=operationDiarySource.js.map