﻿import SessionService = require("./sessionService");
import Logger = require("./logger");
import Api = require("./api");


class ConnectorService {
    private static connected: Q.Promise<any>;

    public static connect() {
        if (!ConnectorService.connected) {
            var securityToken = SessionService.getSecurityToken();
            if (securityToken) {
                Logger.info(ConnectorService, "Updating session");
                ConnectorService.connected = Api.securityService.connectWithToken(securityToken)
                    .then(ConnectorService.updateSession)
                    .fail(Logger.handleError(ConnectorService));
            } else {
                Logger.info(ConnectorService, "Creating session");
                ConnectorService.connected = Api.signalsService.connect()
                    .then(ConnectorService.validateLicense)
                    .then(ConnectorService.initializeSession)
                    .fail(Logger.handleError(ConnectorService));
            }

        }

        return ConnectorService.connected;
    }


    private static updateSession(sessionData: SecuritySessionDTO) {
        ConnectorService.validateLicense(sessionData.Session);
        ConnectorService.initializeSession(sessionData.Session);
        SessionService.setSecurityToken(sessionData.SecurityToken);
        SessionService.updateSessionInformation();
        Logger.info(ConnectorService, `Session updated, sessionId: '${SessionService.sessionId}'`);

        return sessionData.Session;
    }

    private static validateLicense(session: SessionDTO) {
        Logger.info(ConnectorService, "Checking license");

        if (!session.IsValidLicense) {
            throw "Invalid license";
        }

        return session;
    }

    private static initializeSession(session: SessionDTO) {
        Logger.info(ConnectorService, `Initializing session, sessionId: ${session.SessionId}`);
        SessionService.sessionId = session.SessionId;
        return session;
    }
}

export = ConnectorService;