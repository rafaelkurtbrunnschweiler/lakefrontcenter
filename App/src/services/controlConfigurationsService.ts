﻿import SessionService = require("./sessionService");
import Logger = require("./logger");
import Api = require("./api");

class ControlConfigurationsService {

    public static getControlConfigurationByName(name: string, configurationNamespace: string, type: number) {
        Logger.info(ControlConfigurationsService, `getControlConfigurationByName`);
        return Api.controlConfigurationsService.getControlConfigurationByName(name, configurationNamespace, type);
    }

    public static getControlConfigurationThatStartWithName(name: string, configurationNamespace: string, type: number) {
        Logger.info(ControlConfigurationsService, `getControlConfigurationThatStartWithName`);
        return Api.controlConfigurationsService.getControlConfigurationThatStartWithName(name, configurationNamespace, type);
    }

    public static getControlConfigurationsByNamespace(configurationNamespace: string, type: number) {
        Logger.info(ControlConfigurationsService, `getControlConfigurationsByNamespace`);
        return Api.controlConfigurationsService.getControlConfigurationsByNamespace(configurationNamespace, type);
    }

    public static insertControlConfiguration(controlConfiguration: ControlConfigurationDTO) {
        Logger.info(ControlConfigurationsService, `insertControlConfiguration`);
        return Api.controlConfigurationsService.insertControlConfiguration(controlConfiguration);
    }

    public static updateControlConfiguration(controlConfiguration: ControlConfigurationDTO) {
        Logger.info(ControlConfigurationsService, `updateControlConfiguration`);
        return Api.controlConfigurationsService.updateControlConfiguration(controlConfiguration);
    }

    public static deleteControlConfiguration(id: string) {
        Logger.info(ControlConfigurationsService, `deleteControlConfiguration`);
        return Api.controlConfigurationsService.deleteControlConfiguration(id);
    }

}

export = ControlConfigurationsService;