define(["require", "exports"], function (require, exports) {
    "use strict";
    var ConvertToCsvService = /** @class */ (function () {
        function ConvertToCsvService(settings) {
            this.settings = settings;
        }
        ConvertToCsvService.prototype.initialize = function () {
            var objectId = ko.unwrap(this.settings.objectID);
            this.fileName = (ko.unwrap(this.settings.exportFileName) || "export.csv").stringPlaceholderResolver(objectId);
            this.columnDelimiter = (ko.unwrap(this.settings.exportColumnDelimiter) || ";");
            this.lineDelimiter = ko.unwrap(this.settings.exportLineDelimiter) || "\r\n";
            this.dateTimeFormat = ko.unwrap(this.settings.exportDateTimeFormat) || "DD.MM.YYYY HH:mm:ss.SSS";
            this.numeralFormat = ko.unwrap(this.settings.format) || "0,0.[00]";
            this.isAlphanumeric = ko.unwrap(this.settings.isAlphanumeric) !== undefined ? ko.unwrap(this.settings.isAlphanumeric) : false;
            this.columnTitleTemplate = ko.unwrap(this.settings.columnTitleTemplate) || "";
        };
        ConvertToCsvService.prototype.convertChartData = function (data) {
            var _this = this;
            if (data == null || !data.length) {
                return null;
            }
            //build header
            var headerCallback = function () {
                var columns = ["Timestamp"];
                for (var i = 1; i < data.length; i++) { //0 is x axis
                    columns.push(data[i][0]);
                }
                return columns;
            };
            var contentCallback = function () {
                var csv = "";
                for (var l = 0; l < data.length; l++) {
                    data[l] = data[l].reverse();
                }
                for (var j = 0; j < data[0].length - 1; j++) {
                    for (var k = 0; k < data.length; k++) {
                        var item = k === 0
                            ? moment(data[k][j]).format(_this.dateTimeFormat)
                            : data[k][j] ? numeral(data[k][j]).format(_this.numeralFormat) : "";
                        csv += item;
                        if (k !== data.length - 1)
                            csv += _this.columnDelimiter;
                    }
                    csv += _this.lineDelimiter;
                }
                return csv;
            };
            return this.convert(headerCallback, contentCallback);
        };
        ConvertToCsvService.prototype.convertLogTableData = function (values, logTags) {
            var _this = this;
            if (values == null || !values.length || logTags == null || !logTags.length) {
                return null;
            }
            //build header
            var headerCallback = function () {
                var headers = ["Timestamp"];
                for (var i = 0; i < logTags.length; i++) {
                    headers.push(ko.unwrap(logTags[i].columnTitle));
                }
                return headers;
            };
            var contentCallback = function () {
                var csv = "";
                for (var j = 0; j < values.length; j++) {
                    for (var k = 0; k < values[j].length; k++) {
                        var item = !values[j][k]
                            ? ""
                            : k === 0
                                ? moment(values[j][k]).format(_this.dateTimeFormat)
                                : _this.isAlphanumeric
                                    ? values[j][k]
                                    : numeral(values[j][k]).format(_this.numeralFormat);
                        csv += item;
                        if (k !== values.length - 1)
                            csv += _this.columnDelimiter;
                    }
                    csv += _this.lineDelimiter;
                }
                return csv;
            };
            return this.convert(headerCallback, contentCallback);
        };
        ConvertToCsvService.prototype.convert = function (getHeaderCallback, getContentCallback) {
            this.initialize();
            var csv = "";
            var headers = getHeaderCallback ? getHeaderCallback() : null;
            if (headers !== null && headers !== undefined) {
                csv += headers.join(this.columnDelimiter);
                csv += this.lineDelimiter;
            }
            if (getContentCallback)
                csv += getContentCallback();
            this.csv = { fileName: this.fileName, fileContent: csv };
            return this.csv;
        };
        ConvertToCsvService.prototype.download = function () {
            var binaryData = new Blob([this.csv.fileContent], { type: 'text/csv' });
            if (window.navigator.msSaveOrOpenBlob) {
                window.navigator.msSaveOrOpenBlob(binaryData, this.csv.fileName);
            }
            else {
                var link = window.document.createElement('a');
                link.setAttribute('href', window.URL.createObjectURL(binaryData));
                link.setAttribute('download', this.csv.fileName);
                // Append anchor to body.
                document.body.appendChild(link);
                link.click();
                // Remove anchor from body
                document.body.removeChild(link);
            }
        };
        return ConvertToCsvService;
    }());
    return ConvertToCsvService;
});
//# sourceMappingURL=convertToCsvService.js.map