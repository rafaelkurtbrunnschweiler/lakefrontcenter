/**
 * This interface contains the parameters for the VisualSecurityService.
 * 
 * @interface IVisualSecurityParams
 * @extends {IComponentBaseParams}
 */
interface IVisualSecurityParams extends IComponentBaseParams {
    enableSignalName: string;
    enableSignalValue: any;
    enableOperator: string;
    visibilitySignalName: string;
    visibilitySignalValue: string;
    visibilityOperator: string;
}