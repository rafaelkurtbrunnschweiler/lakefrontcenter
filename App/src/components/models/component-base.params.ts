/**
 * This interface contains the HTML parameters for the abstract ComponentBase Model.
 * 
 * @interface IComponentBaseParams
 */
interface IComponentBaseParams {
    objectID?: string;
    projectAuthorization?: string;
    tooltipText?: string;
}