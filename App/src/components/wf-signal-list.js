﻿define(["../services/connector", "../services/visualSecurityService"],
    function (signalsConnector, visualSecurityService) {
        var wfSignalList = function (params) {
            var self = this;
            self.settings = params;

            self.id = ko.observable(uuid.v4());
            self.objectID = ko.unwrap(self.settings.objectID);
            self.isModalDialogsDraggable = self.settings.isModalDialogsDraggable !== undefined ? self.settings.isModalDialogsDraggable : true;

            self.configurationButtonVisibility = ko.observable(ko.unwrap(self.settings.configurationButtonVisibility) !== undefined ? ko.unwrap(self.settings.configurationButtonVisibility) : true);
            self.initialConfiguration = (ko.unwrap(self.settings.initialConfiguration) || "").stringPlaceholderResolver(self.objectID);
            self.configurationNamespace = (ko.unwrap(self.settings.configurationNamespace) || "").stringPlaceholderResolver(self.objectID);
            self.controlType = ConfigControlType.SignalList;
            self.configurationButtonIconClass = ko.unwrap(self.settings.configurationButtonIconClass);

            self.signals = ko.observable();
            self.connector = new signalsConnector();
            self.showDialog = ko.observable(false);

            self.tooltipText = (ko.unwrap(self.connector.translate(self.settings.tooltipText)()) || "").stringPlaceholderResolver(self.objectID);
            self.signalsDefinitions = [];
            self.signalsFilter = _.map(ko.unwrap(self.settings.signalsFilter), function (item) {
                return (item || "").stringPlaceholderResolver(self.objectID);
            });
            self.groupsFilter = _.map(ko.unwrap(self.settings.groupsFilter), function (item) {
                return (item || "").stringPlaceholderResolver(self.objectID);
            });
            self.getSignalsDefinitionsForAllSignals(0);

            self.allSignals = ko.observableArray([]);
            self.allGroups = ko.observableArray([]);
            self.selectedSignal = ko.observable();
            self.selectedSignal.subscribe(function (value) {
                if (value)
                    self.filtersChanged(true);
            }, self);
            self.items = ko.observableArray();
            self.selectedGroup = ko.observable();
            self.selectedGroup.subscribe(self.groupChanged, self);

            self.title = (ko.unwrap(self.settings.title) || "Online Values").stringPlaceholderResolver(self.objectID);
            self.format = ko.unwrap(self.settings.format) || '0,0.[00]';
            self.valueDisplayClass = ko.unwrap(self.settings.valueDisplayClass) || "label-info";

            self.addButtonCssClass = ko.unwrap(self.settings.addButtonCssClass) || "btn btn-success";
            self.listButtonCssClass = ko.unwrap(self.settings.listButtonCssClass) || "btn btn-default";
            self.listValidationCssClass = ko.unwrap(self.settings.listValidationCssClass) || "btn btn-info";

            self.listTemplate = ko.unwrap(self.settings.listTemplate) || "wf-value-display"; //wf-value-display, wf-value, 

            self.hasData = ko.computed(function () {
                return ko.unwrap(self.signals) && ko.unwrap(self.signals).length > 0;
            });

            self.isDisabledAddItemButton = ko.computed(function () {
                if (!ko.unwrap(self.items))
                    return false;

                if (!_.contains(ko.unwrap(self.items), ko.unwrap(self.selectedSignal)))
                    return false;

                return true;
            });

            self.filtersChanged = ko.observable(false);
            self.isAlphanumeric = ko.observable(false);

            self.settingsButtonBarCssClass = ko.computed(function () {
                return self.filtersChanged() ? "btn btn-warning" : "btn btn-success";
            }, self);

            self.visualSecurityService = new visualSecurityService(self.settings, self.connector);
            self.visualSecurityService.initialize();
            self.isVisible = self.visualSecurityService.isVisible;
        };

        wfSignalList.prototype = {
            groupChanged: function (newGroup) {
                var self = this;

                var signalOfGroup = $.grep(Object.keys(self.signalsDefinitions), function (signal) {
                    return self.signalsDefinitions[signal].Group.Name === newGroup;
                });

                self.allSignals(signalOfGroup);

                //var firstSignal = null;
                //if (signalOfGroup.length > 0)
                //    firstSignal = signalOfGroup[0];

                //self.selectedSignal(firstSignal);
            },
            showSettings: function () {
                var self = this;

                self.showDialog(true);
                if (self.allGroups().length > 0)
                    self.selectedGroup(self.allGroups()[0]);

                if (!self.filtersChanged())
                    self.items(ko.unwrap(self.signals) ?
                       _.map(ko.unwrap(self.signals), function (signal) { 
                           return {
                               signalName: signal.signalName, 
                               isAlphanumeric: signal.isAlphanumeric !== undefined ? signal.isAlphanumeric: false
                           }; 
                       }).sort(function (el1, el2) {
                           return el1.signalName > el2.signalName;
                       })
                       : []);
            },

            closeSettings: function () {
                var self = this;

                self.showDialog(false);
            },
            applyFilterSettings: function () {
                var self = this;
                self.showDialog(false);
                self.filtersChanged(false);

                var temp = _.map(self.items(), function (item) {

                    var signal = _.findWhere(ko.unwrap(self.settings.signals), { signalName: item });

                    return {
                        signalName: item.signalName,
                        signalLabel: signal ? signal.signalLabel : '',
                        staticUnitText: signal ? signal.staticUnitText : '',
                        isAlphanumeric: item.isAlphanumeric,
                    };
                });

                self.signals(temp);
            },
            addItem: function () {
                var self = this;

                if (!self.selectedSignal()) return;
                if (_.findIndex(self.items(), { signalName: self.selectedSignal() }) > -1) return;

                var t = self.items();

                t.push({signalName: self.selectedSignal(), isAlphanumeric: self.isAlphanumeric()});
                t.sort(function (el1, el2) {
                    return el1.signalName > el2.signalName;
                });

                self.items(t);
                self.selectedSignal(null);
                self.isAlphanumeric(false);
            },

            removeItem: function (removedItem) {
                var self = this;

                var newValues = $.grep(self.items(), function (e) {
                    return e != removedItem;
                });

                self.items(newValues.sort());
                self.filtersChanged(true);
            },
            getSignalsDefinitionsForAllSignals: function (start) {
                var self = this;

                return self.connector.getSignalsDefinitions(self.signalsFilter)
                        .then(function (definitions) {
                            definitions.forEach(function (definition) {
                                if (self.groupsFilter.length === 0 || (self.groupsFilter.length > 0 && _.contains(self.groupsFilter, definition.Group.Name)))
                                    self.signalsDefinitions[definition.AliasName] = definition;
                            });

                            var signalNames = Object.keys(self.signalsDefinitions);

                            for (var i = 0; i < signalNames.length; i++)
                                if (self.allGroups().indexOf(self.signalsDefinitions[signalNames[i]].Group.Name) === -1)
                                    self.allGroups.push(self.signalsDefinitions[signalNames[i]].Group.Name);

                            self.allGroups(self.allGroups().sort());
                            self.loadInitialConfiguration();
                        })
                        .fail(self.connector.handleError(self));
            },
            dispose: function () {
                var self = this;

                //clear dialogs
                var settingsDialog = $(document).find('#modal-settings-' + self.id());
                var settingsBackContainer = $(document).find('#modal-settings-back-container-' + self.id());

                $(settingsDialog).appendTo(settingsBackContainer);
            },

            resolvePlaceHolder: function (signalNames, objectID) {
                var self = this;

                for (var i = 0; i < signalNames.length; i++) {
                    signalNames[i].signalName = (ko.unwrap(signalNames[i].signalName) || "").stringPlaceholderResolver(self.objectID);
                    signalNames[i].signalLabel = (ko.unwrap(signalNames[i].signalLabel) || "").stringPlaceholderResolver(self.objectID);
                    signalNames[i].staticUnitText = (ko.unwrap(signalNames[i].staticUnitText) || "").stringPlaceholderResolver(self.objectID);

                }

                return signalNames;
            },

            getConfig: function () {
                var self = this;
                var content = {
                    signals: self.signals
                }

                return content;
            },

            loadConfig: function (content) {
                var self = this;

                self.signals(content.signals);
            },

            loadInitialConfiguration: function () {
                var self = this;

                self.connector.getControlConfigurationsByName(self.initialConfiguration, self.configurationNamespace, self.controlType)
                        .then(function (config) {
                            if (config)
                                self.loadConfig(JSON.parse(config.Content));
                            else
                                self.signals(self.filterSignalsByRange(ko.unwrap(self.settings.signals)));
                        });
            },

            filterSignalsByRange: function (signals) {
                var self = this;

                //by signal range
                signals = _.filter(self.resolvePlaceHolder(ko.unwrap(self.settings.signals), self.objectID), function (item) {
                    return self.signalsFilter.length > 0 ? _.contains(self.signalsFilter, item.signalName) : true;
                });

                //by group range
                signals = _.filter(signals, function (item) {
                    return self.groupsFilter.length > 0 ? _.contains(Object.keys(self.signalsDefinitions), item.signalName) : true;
                });

                return signals;
            }
        };

        return wfSignalList;
    });