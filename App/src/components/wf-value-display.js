﻿define(['../services/connector', "../services/securedService", "../services/changedFieldAnimationService", "../services/visualSecurityService", "../services/statesService"],
    function (signalsConnector, securedService, changedFieldAnimationService, visualSecurityService, statesService) {
        //var signalsConnector = require('../services/connector');

        var wfValueDisplay = function (params) {
            var self = this;
            self.connector = new signalsConnector();

            self.settings = params;
            self.objectID = ko.unwrap(self.settings.objectID);

            self.projectAuthorization = (ko.unwrap(params.projectAuthorization) || "").stringPlaceholderResolver(self.objectID);
            self.securedService = new securedService(self.projectAuthorization);
            self.hasAuthorization = self.securedService.hasAuthorization;

            self.tooltipText = (ko.unwrap(self.connector.translate(self.settings.tooltipText)()) || "").stringPlaceholderResolver(self.objectID);
            self.format = ko.unwrap(self.settings.format) ? ko.unwrap(self.settings.format) : "0,0.[00]";
            self.isAlphanumeric = ko.unwrap(self.settings.isAlphanumeric) !== undefined ? ko.unwrap(self.settings.isAlphanumeric) : false;

            self.isDateTime = ko.unwrap(self.settings.isDateTime) !== undefined ? ko.unwrap(self.settings.isDateTime) : false;
            self.dateTimeFormat = ko.unwrap(self.settings.dateTimeFormat) ? ko.unwrap(self.settings.dateTimeFormat) : "";

            self.iconClass = ko.unwrap(self.settings.iconClass) || null;
            self.displayClass = ko.unwrap(self.settings.displayClass) || '';
            self.displaySize = ko.unwrap(self.settings.displaySize) ? "input-group-" + ko.unwrap(self.settings.displaySize) : "";


            self.label = (ko.unwrap(self.settings.label) || '').stringPlaceholderResolver(self.objectID);
            self.signalNameLabel = ko.unwrap(self.settings.signalNameLabel) !== undefined ? ko.unwrap(self.settings.signalNameLabel) : false;
            self.unitLabel = ko.unwrap(self.settings.unitLabel) !== undefined ? ko.unwrap(self.settings.unitLabel) : false;
            self.staticUnitText = (ko.unwrap(self.settings.staticUnitText) || '').stringPlaceholderResolver(self.objectID);

            self.iconStyle = ko.unwrap(self.settings.iconStyle) || '';
            self.textStyle = ko.unwrap(self.settings.textStyle) || '';

            self.signalName = (ko.unwrap(self.settings.signalName) || '').stringPlaceholderResolver(self.objectID);

            self.signalValue = '';
            self.cssClass = "";

            self.initializeVisualSecurity();

            self.isArray = ko.unwrap(self.settings.isArray) !== undefined ? ko.unwrap(self.settings.isArray) : false;
            self.arrayIndex = ko.unwrap(self.settings.arrayIndex) || 0;
            self.arrayDelimiter = ko.unwrap(self.settings.arrayDelimiter) || ",";

            // Stop here and return if no signalName was configured
            if (!self.signalName) {
                return;
            }

            self.signal = self.connector.getSignal(self.signalName);
            if (self.isArray) {
                self.signalValue = ko.computed(function () {
                    var self = this;
                    var array = self.signal.value().toString().split(self.arrayDelimiter);

                    if (array.length < self.arrayIndex + 1)
                        return "n/a";

                    return array[self.arrayIndex];
                }, self);
            } else if (self.isAlphanumeric) {
                self.signalValue = self.signal.value;
            } else if (self.isDateTime) {
                self.signalValue = self.signal.value.extend({
                    date: {
                        format: self.dateTimeFormat
                    }
                });
            } else {
                self.signalValue = self.signal.value.extend({
                    numeralNumber: self.format
                });
            }

            self.initializeVisualStates();
            self.initializeChangedFieldAnimation();

            self.connector.getOnlineUpdates().fail(self.connector.handleError(self));
        };

        wfValueDisplay.prototype.initializeVisualSecurity = function () {
            var self = this;
            self.visualSecurityService = new visualSecurityService(self.settings, self.connector);
            self.visualSecurityService.initialize();
            self.isVisible = self.visualSecurityService.isVisible;
            self.isDisabled = self.visualSecurityService.isDisabled;
        }

        wfValueDisplay.prototype.initializeChangedFieldAnimation = function () {
            var self = this;
            self.changedFieldAnimationService = new changedFieldAnimationService(self.settings, self.signalValue, self.cssDisplayClass);
            self.changedFieldAnimationService.initialize();
            self.cssClass = ko.computed(function () {
                return self.changedFieldAnimationService ? self.changedFieldAnimationService.cssClass() || "" : "";
            });
        }

        wfValueDisplay.prototype.initializeVisualStates = function () {
            var self = this;
            var cssClassNames = [self.settings.cssClassNormalState || "normal"];

            if (_.any(self.settings.states)) {
                _.each(self.settings.states, function (state) {
                    cssClassNames.push(state.cssClassName);
                });
            } else if (!Array.isArray(self.settings.cssClassStates)) {
                cssClassNames.push(self.settings.cssClassState1 || "state1");
                cssClassNames.push(self.settings.cssClassState2 || "state2");
                cssClassNames.push(self.settings.cssClassState3 || "state3");
                cssClassNames.push(self.settings.cssClassState4 || "state4");
                cssClassNames.push(self.settings.cssClassState5 || "state5");
                cssClassNames.push(self.settings.cssClassState6 || "state6");
                cssClassNames.push(self.settings.cssClassState7 || "state7");
                cssClassNames.push(self.settings.cssClassState8 || "state8");
            } else {
                cssClassNames.push.apply(cssClassNames, self.settings.cssClassStates);
            }

            self.states = new statesService(self.settings);

            self.statusCssClass = ko.computed(function () {
                var stateNumber = ko.unwrap(self.states.currentStateIndex);

                var cssClass = _.isNaN(stateNumber) ||
                    stateNumber >= cssClassNames.length ?
                    cssClassNames[0] :
                    cssClassNames[stateNumber];

                return cssClass;
            }, self);

            self.css = ko.computed(function () {
                return self.states.currentState() + " " + self.statusCssClass();
            }, self);

            self.cssDisplayClass = ko.computed(function () {
                return self.css() + " " + self.displayClass || "";
            }, self);
        }


        wfValueDisplay.prototype.dispose = function () {
            var self = this;

            if (!self.signal)
                return;

            self.changedFieldAnimationService.dispose();
            return self.connector.unregisterSignals(self.signal);
        };

        return wfValueDisplay;
    });