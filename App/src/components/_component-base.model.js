define(["require", "exports", "../services/connector", "../services/securedService"], function (require, exports, Connector, SecuredService) {
    "use strict";
    var ComponentBaseModel = /** @class */ (function () {
        function ComponentBaseModel(params) {
            this.connector = new Connector();
            this.id = ko.observable(uuid.v4());
            this.settings = params;
            this.initializeSettings();
        }
        ComponentBaseModel.prototype.initializeSettings = function () {
            this.objectID = ko.unwrap(this.settings.objectID);
            this.projectAuthorization = (ko.unwrap(this.settings.projectAuthorization) || "").stringPlaceholderResolver(this.objectID);
            this.securedService = new SecuredService(this.projectAuthorization);
            this.hasAuthorization = this.securedService.hasAuthorization;
            this.tooltipText = (ko.unwrap(this.connector.translate(this.settings.tooltipText)()) || "").stringPlaceholderResolver(this.objectID);
        };
        return ComponentBaseModel;
    }());
    return ComponentBaseModel;
});
//# sourceMappingURL=_component-base.model.js.map