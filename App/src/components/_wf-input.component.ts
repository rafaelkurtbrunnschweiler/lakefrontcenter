import Connector = require("../services/connector");
import SecuredService = require("../services/securedService");
import Signal = require("../services/models/signal");

import ComponentBaseModel = require("./_component-base.model");
import VisualSecurityService = require("../services/visualSecurityService");
import ChangedFieldAnimationService = require("../services/changedFieldAnimationService");

declare var numeral;

/**
 * This interface contains the HTML parameters for the WfInputComponent.
 * 
 * @interface IWfInputParams
 * @extends {IComponentBaseParams}
 * @extends {IVisualSecurityParams}
 * @extends {IChangedFieldAnimationParams}
 */
interface IWfInputParams extends IComponentBaseParams, IVisualSecurityParams, IChangedFieldAnimationParams {
    format: string;
    isAlphanumeric: boolean;
    unitLabel: boolean;
    staticUnitText: string;
    signalName: string;
    isArray: boolean;
    arrayIndex: number;
    arrayDelimiter: string;

    iconClass: string;
    displayClass: string;
    displaySize: string;
    label: string;
    signalNameLabel: boolean;
    iconStyle: string;
    textStyle: string;

    isBufferedClass: string;
    inputSize: string;
    writeToBuffer: boolean;
}

class WfInputComponent extends ComponentBaseModel<IWfInputParams> {

    private displayClassNames: KnockoutComputed<string>;
    private isBuffered: KnockoutComputed<boolean>;
    private writeToBuffer: boolean;
    private inputSize: string;
    private isBufferedClass: string;
    private inputSignalValue: KnockoutObservable<any>;
    private inputSignal: Signal;
    private isSelected: KnockoutObservable<boolean>;
    private isEditing: KnockoutObservable<boolean>;
    private signalValue: any;
    private uncommittedValue: KnockoutObservable<any>;
    private signalName: string;
    private staticUnitText: string;
    private unitLabel: boolean;
    private isAlphanumeric: boolean;
    private format: string;

    private iconClass: string;
    private displayClass: string;
    private label: string;
    private signalNameLabel: boolean;
    private iconStyle: string;
    private textStyle: string;

    protected visualSecurityService: VisualSecurityService;
    protected isVisible: KnockoutComputed<boolean>;
    protected isDisabled: KnockoutComputed<boolean>;

    protected changedFieldAnimationService: ChangedFieldAnimationService;
    protected cssClass: KnockoutComputed<string> | string;

    constructor(params: IWfInputParams) {
        super(params)
        this.initializeVisualSecurity();

        // Stop here and return if no signalName was configured
        if (!this.signalName) {
            return null;
        }

        this.inputSignal = this.connector.getSignal(ko.unwrap(this.signalName));

        if (this.settings.isAlphanumeric) {
            this.inputSignalValue = this.inputSignal.value;
        } else {
            this.inputSignalValue = this.inputSignal.value.extend({ numeralNumber: this.format });
        }

        this.initializeComputeds();
        this.initializeChangedFieldAnimation();
        this.connector.getOnlineUpdates().fail(this.connector.handleError(this));

    }

    private initializeVisualSecurity() {
        this.visualSecurityService = new VisualSecurityService(this.settings, this.connector);
        this.visualSecurityService.initialize();
        this.isVisible = this.visualSecurityService.isVisible;
        this.isDisabled = this.visualSecurityService.isDisabled;
    }


    private initializeChangedFieldAnimation() {
        this.changedFieldAnimationService = new ChangedFieldAnimationService(this.settings, this.signalValue as KnockoutObservable<any>, this.displayClassNames);
        this.changedFieldAnimationService.initialize();
        this.cssClass = ko.computed(() => {
            return this.changedFieldAnimationService ? this.changedFieldAnimationService.cssClass() || "" : "";
        });
    }

    protected initializeSettings() {
        super.initializeSettings();

        this.format = ko.unwrap(this.settings.format) ? ko.unwrap(this.settings.format) : "0,0.[000]";
        this.isAlphanumeric = ko.unwrap(this.settings.isAlphanumeric) !== undefined ? ko.unwrap(this.settings.isAlphanumeric) : false;

        this.iconClass = ko.unwrap(this.settings.iconClass) || null;
        this.displayClass = ko.unwrap(this.settings.displayClass) || null;
        this.isBufferedClass = ko.unwrap(this.settings.isBufferedClass) || "label-info";
        this.inputSize = ko.unwrap(this.settings.inputSize) ? "input-group-" + ko.unwrap(this.settings.inputSize) : "";

        this.label = (ko.unwrap(this.settings.label) || '').stringPlaceholderResolver(this.objectID);
        this.signalNameLabel = ko.unwrap(this.settings.signalNameLabel) !== undefined ? ko.unwrap(this.settings.signalNameLabel) : false;
        this.unitLabel = ko.unwrap(this.settings.unitLabel) !== undefined ? ko.unwrap(this.settings.unitLabel) : false;
        this.staticUnitText = (ko.unwrap(this.settings.staticUnitText) || '').stringPlaceholderResolver(this.objectID);

        this.iconStyle = ko.unwrap(this.settings.iconStyle) || '';
        this.textStyle = ko.unwrap(this.settings.textStyle) || '';

        this.signalName = (ko.unwrap(this.settings.signalName) || '').stringPlaceholderResolver(this.objectID);

        this.uncommittedValue = ko.observable<any>();
        this.signalValue = null;

        this.isEditing = ko.observable(false);
        this.isSelected = ko.observable(false);

        this.writeToBuffer = ko.unwrap(this.settings.writeToBuffer) !== undefined ? ko.unwrap(this.settings.writeToBuffer) : false;
    }

    protected initializeComputeds() {
        this.isBuffered = ko.computed(() => {
            if (!this.writeToBuffer)
                return false;
            return this.connector.existSignalInBuffer(this.signalName) && !this.connector.signalBufferIsEmpty();
        });

        this.displayClassNames = ko.computed(() => {
            return this.isBuffered() == true ? this.isBufferedClass : this.displayClass;

        });

        this.signalValue = ko.computed({
            read: () => {
                if (!this.isEditing() && !this.isBuffered()) {
                    return ko.unwrap(this.inputSignalValue);
                } else if (!this.isEditing() && this.isBuffered()) {
                    var value = this.connector.readSignalsFromBuffer([this.signalName]);
                    return value.length > 0 ? value[0] : null;
                } else {
                    return ko.unwrap(this.uncommittedValue);
                }
            },
            write: (value) => {
                this.uncommittedValue(value);
                this.isEditing(true);
            }
        });


    }

    private async writeInputValue() {
        var values = {} as SignalValue[];

        if (!this.signalName) return;

        values[this.signalName] = this.settings.isAlphanumeric ? ko.unwrap(this.uncommittedValue) : numeral(ko.unwrap(this.uncommittedValue)).value();

        if (this.writeToBuffer) {
            this.connector.writeSignalsToBuffer(values);
            this.isEditing(false);
        }
        else {
            const result = await this.connector.writeSignals(values)
            // Write signal values, warning if an error will be returned
            this.isEditing(false);
            if (result) {
                this.connector.warn("WriteSignal result", result as any);
            }
        }
    }

    private resetInputValue() {
        this.isEditing(false);
    }

    private keyupEventHandler(data, event) {
        if (event.which === 13) {
            this.writeInputValue();
        }
    }

    /**
     *  Place here signal cleanup functionality.
     * 
     * @protected
     * @returns 
     * 
     * @memberOf WfInputComponent
     */
    protected async dispose() {
        if (!this.inputSignal) return;
        this.changedFieldAnimationService.dispose();
        return this.connector.unregisterSignals(this.inputSignal);
    }
}

export = WfInputComponent;
