import Connector = require("../services/connector");
import WfValueComponent = require("./_wf-value.component");
import ChangedFieldAnimationService = require("../services/changedFieldAnimationService");

/**
 * This interface contains the HTML parameters for the WfValueDisplayComponent.
 * 
 * @interface IWfValueDisplayParams
 * @extends {IComponentBaseParams}
 * @extends {IVisualSecurityParams}
 * @extends {IChangedFieldAnimationParams}
 */
interface IWfValueDisplayParams extends IComponentBaseParams, IVisualSecurityParams, IChangedFieldAnimationParams, IState, ICssClassStateParams {
    format: string;
    isAlphanumeric: boolean;
    unitLabel: boolean;
    staticUnitText: string;
    signalName: string;
    isArray: boolean;
    arrayIndex: number;
    arrayDelimiter: string;
    displayClass: string;

    iconClass: string;
    displaySize: string;
    label: string;
    signalNameLabel: boolean;
    iconStyle: string;
    textStyle: string;
}


class WfValueDisplayComponent extends WfValueComponent<IWfValueDisplayParams> {

    private iconClass: string;
    private displaySize: string;
    private label: string;
    private signalNameLabel: boolean;
    private iconStyle: string;
    private textStyle: string;

    constructor(params: IWfValueDisplayParams) {
        super(params)
    }

    protected initializeSettings() {
        super.initializeSettings();

        this.iconClass = ko.unwrap(this.settings.iconClass) || null;
        this.displaySize = ko.unwrap(this.settings.displaySize) ? "input-group-" + ko.unwrap(this.settings.displaySize) : "";
        this.label = (ko.unwrap(this.settings.label) || '').stringPlaceholderResolver(this.objectID);
        this.signalNameLabel = ko.unwrap(this.settings.signalNameLabel) !== undefined ? ko.unwrap(this.settings.signalNameLabel) : false;
        this.unitLabel = ko.unwrap(this.settings.unitLabel) !== undefined ? ko.unwrap(this.settings.unitLabel) : false;
        this.staticUnitText = (ko.unwrap(this.settings.staticUnitText) || '').stringPlaceholderResolver(this.objectID);

        this.iconStyle = ko.unwrap(this.settings.iconStyle) || '';
        this.textStyle = ko.unwrap(this.settings.textStyle) || '';

        this.signalName = (ko.unwrap(this.settings.signalName) || '').stringPlaceholderResolver(this.objectID);
        this.signalValue = '';
    }

    /**
     * Place here signal cleanup functionality.
     * 
     * @protected
     * @returns 
     * 
     * @memberOf WfValueDisplayComponent
     */
    protected dispose() {
        return super.dispose();
    }
}

export = WfValueDisplayComponent;
