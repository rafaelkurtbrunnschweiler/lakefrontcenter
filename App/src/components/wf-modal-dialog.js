﻿define(["../services/connector",
        "../services/models/logValuesFilter",
        "../services/securedService",
        "../decorators/busyIndicator"],
    function (signalsConnector, logValuesFilter, securedService, busyIndicator) {

        var wfModalDialog = function (params) {
            var self = this;
            self.connector = new signalsConnector();

            self.settings = params || {};
            self.id = ko.observable(uuid.v4());

            self.viewName = self.settings.viewName !== undefined ? ko.unwrap(self.settings.viewName) : "";
            self.viewPath = self.settings.viewPath !== undefined ? ko.unwrap(self.settings.viewPath) : "App/src/views/dialogs/";//viewPathPrefix + self.settings.viewName + ".html";
            self.draggable = self.settings.draggable !== undefined ? ko.unwrap(self.settings.draggable) : true;
            self.title = self.settings.title !== undefined ? ko.unwrap(self.settings.title) : "";

            self.show = ko.observable(false);

            var isViewModelObservable = self.settings.viewModel !== undefined && typeof self.settings.viewModel === "function";
            self.viewModel = isViewModelObservable ? self.settings.viewModel : ko.observable();
            self.viewModel.subscribe(function (newValue) {
                if (!newValue)
                    return;

                self.applyViewModel(newValue);
            }, self);
            if (!isViewModelObservable)
                self.viewModel(self.settings.viewModel);
        };
       
        wfModalDialog.prototype.close = function () {
            var self = this;
            self.show(false);
        };

        wfModalDialog.prototype.handleShowDialog = function () {
            var self = this;
            self.show(true);
        };

        wfModalDialog.prototype.applyViewModel = function (viewModel) {
            var self = this;

            if (!self.viewName)
                return;

            if (_.last(self.viewPath) != '/')
                self.viewPath = self.viewPath + "/";

            var viewPath = self.viewPath + self.viewName + ".html";

            $.get(viewPath, function (data) {
                if (!data)
                    return;

                var content = $("#modal-dialog-" + self.id()).find(".modal-body").first();
                if (content.length === 0)
                    return;

                $(content).html(data);

                var extendmodel = _.extend(viewModel, { connector: self.connector, show: self.show});
                ko.cleanNode(content[0]);
                ko.applyBindings(extendmodel, content[0]);
            });
        };

        wfModalDialog.prototype.dispose = function () {
            var self = this;

            //clear dialogs
            var modalDialog = $(document).find('#modal-dialog-' + self.id());
            var modalDialogContainer = $(document).find('#modal-dialog-container-' + self.id());

            $(modalDialog).appendTo(modalDialogContainer);
        };

        return wfModalDialog;
    });