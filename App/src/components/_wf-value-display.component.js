var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "./_wf-value.component"], function (require, exports, WfValueComponent) {
    "use strict";
    var WfValueDisplayComponent = /** @class */ (function (_super) {
        __extends(WfValueDisplayComponent, _super);
        function WfValueDisplayComponent(params) {
            return _super.call(this, params) || this;
        }
        WfValueDisplayComponent.prototype.initializeSettings = function () {
            _super.prototype.initializeSettings.call(this);
            this.iconClass = ko.unwrap(this.settings.iconClass) || null;
            this.displaySize = ko.unwrap(this.settings.displaySize) ? "input-group-" + ko.unwrap(this.settings.displaySize) : "";
            this.label = (ko.unwrap(this.settings.label) || '').stringPlaceholderResolver(this.objectID);
            this.signalNameLabel = ko.unwrap(this.settings.signalNameLabel) !== undefined ? ko.unwrap(this.settings.signalNameLabel) : false;
            this.unitLabel = ko.unwrap(this.settings.unitLabel) !== undefined ? ko.unwrap(this.settings.unitLabel) : false;
            this.staticUnitText = (ko.unwrap(this.settings.staticUnitText) || '').stringPlaceholderResolver(this.objectID);
            this.iconStyle = ko.unwrap(this.settings.iconStyle) || '';
            this.textStyle = ko.unwrap(this.settings.textStyle) || '';
            this.signalName = (ko.unwrap(this.settings.signalName) || '').stringPlaceholderResolver(this.objectID);
            this.signalValue = '';
        };
        /**
         * Place here signal cleanup functionality.
         *
         * @protected
         * @returns
         *
         * @memberOf WfValueDisplayComponent
         */
        WfValueDisplayComponent.prototype.dispose = function () {
            return _super.prototype.dispose.call(this);
        };
        return WfValueDisplayComponent;
    }(WfValueComponent));
    return WfValueDisplayComponent;
});
//# sourceMappingURL=_wf-value-display.component.js.map