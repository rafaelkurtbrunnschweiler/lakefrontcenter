﻿define(["../../services/connector"],
    function (signalsConnector) {

        var wfWriteSecureSignal = function (params) {
            var self = this;

            self.settings = params;
            self.id = ko.observable(uuid.v4());
            
            self.draggable = self.settings.draggable !== undefined ? self.settings.draggable : ko.observable(true);
            self.showWriteSecure = self.settings.show !== undefined ? self.settings.show : ko.observable(false);

            if (typeof self.showWriteSecure === "function")
                self.showWriteSecure.subscribe(function (newvalue) {
                    if (newvalue === false)
                        self.handleCancelWriteSecure();
                }, self);

            self.signalName = self.settings.signalName !== undefined ? self.settings.signalName : null;
            self.signalValue = self.settings.signalValue !== undefined ? self.settings.signalValue : null;
            self.writeFromBuffer = self.settings.writeFromBuffer !== undefined ? self.settings.writeFromBuffer : false;

            self.successCalback = self.settings.successCalback !== undefined ? self.settings.successCalback : null;
            self.cancelCalback = self.settings.cancelCalback !== undefined ? self.settings.cancelCalback : null;

            self.reinforcementPassword = ko.observable();

            self.connector = new signalsConnector();
        };


        wfWriteSecureSignal.prototype = {

            handleCancelWriteSecure: function () {
                var self = this;

                self.closeWriteSecure();

                if (self.cancelCalback)
                    self.cancelCalback();
            },

            closeWriteSecure: function () {
                var self = this;

                self.reinforcementPassword(null);
                self.showWriteSecure(false);
            },

            handleWriteSecure: function () {
                var self = this;
                var values = {};

                if (self.writeFromBuffer)
                    return self.connector.writeSignalsFromBufferSecure(self.reinforcementPassword())
                               .then(function (result) {
                                   if (result) {
                                       self.closeWriteSecure();

                                       if (self.successCalback)
                                           self.successCalback();
                                   }

                               });

                if (!self.signalName || !self.signalValue)
                    return self.closeWriteSecure();

                values[ko.unwrap(self.signalName)] = ko.unwrap(self.signalValue);

                return self.connector.writeSignalsSecure(self.reinforcementPassword(), values)
                    .then(function (response) {
                        if (response !== undefined) {
                            self.closeWriteSecure();

                            if (self.successCalback)
                                self.successCalback();
                        }
                    }).catch(function (e) {
                        self.connector.error(self, self.connector.translate("{0} {1}".format("Webservice operation error. Exception:", e.responseJSON.Message))());
                    });
            },

            dispose: function () {
                var self = this;

                //clear dialogs
                var configDialog = $(document).find('#wf-write-secure-dialog-' + self.id());
                var configBackContainer = $(document).find('#wf-write-secure-dialog-back-container-' + self.id());

                $(configDialog).appendTo(configBackContainer);
            },
        }

        return wfWriteSecureSignal;
    });
