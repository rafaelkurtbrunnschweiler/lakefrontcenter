﻿define(["../services/connector", "../services/securedService"],

    function(signalsConnector, securedService) {

        var wfAlarmAckButton = function(params) {
            var self = this;
            self.connector = new signalsConnector();
            self.settings = params;
            self.objectID = ko.unwrap(params.objectID);

            self.projectAuthorization = (ko.unwrap(params.projectAuthorization) || "").stringPlaceholderResolver(self.objectID);
            self.securedService = new securedService(self.projectAuthorization);
            self.hasAuthorization = self.securedService.hasAuthorization;

            self.alarmId = ko.unwrap(self.settings.alarmId);
            self.ackComment = self.settings.ackComment;
            self.buttonText = (ko.unwrap(self.settings.buttonText) || 'I4SCADA_Acknowledge').stringPlaceholderResolver(self.objectID);;
            self.iconClass = ko.unwrap(self.settings.iconClass) || "wf wf-check";
            self.cssClass = ko.unwrap(self.settings.cssClass) || "btn btn-warning";

            self.callback = self.settings.callback;
        };

        wfAlarmAckButton.prototype.acknowledgeAlarm = function() {
            var self = this;
            return self.connector.acknowledgeAlarms([self.alarmId], self.ackComment()).then(
                    function(result) {

                        self.ackComment(null);
                        if (result.Result === true) {
                            self.connector.info(self, "Alarm acknowledged successfully");
                        } else {
                            self.connector.error(self, "Alarm acknowledgment failed: " + result.ErrorCodes[0]);
                        }
                        if (self.callback)
                            Q(self.callback());

                        return result;
                    })
                .fail(self.connector.handleError(self));
        };

        return wfAlarmAckButton;
    });