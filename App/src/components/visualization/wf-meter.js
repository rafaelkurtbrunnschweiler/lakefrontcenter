﻿define(['../../services/connector', "../../services/securedService", "../../services/visualSecurityService"],
    function (signalsConnector, securedService, visualSecurityService) {

        var wfMeter = function (params) {
            var self = this;
            self.id = ko.observable(uuid.v4());
            self.settings = params;
            self.objectID = ko.unwrap(self.settings.objectID);

            self.connector = new signalsConnector();
            self.projectAuthorization = (ko.unwrap(params.projectAuthorization) || "").stringPlaceholderResolver(self.objectID);
            self.securedService = new securedService(self.projectAuthorization);
            self.hasAuthorization = self.securedService.hasAuthorization;

            self.signalName = (ko.unwrap(self.settings.signalName) || '').stringPlaceholderResolver(self.objectID);

            self.tooltipText = (ko.unwrap(self.connector.translate(self.settings.tooltipText)()) || "").stringPlaceholderResolver(self.objectID);
            self.label = (ko.unwrap(self.settings.label) || '').stringPlaceholderResolver(self.objectID);
            self.unitLabel = ko.unwrap(self.settings.unitLabel) !== undefined ? ko.unwrap(self.settings.unitLabel) : true;
            self.staticUnitText = (ko.unwrap(self.settings.staticUnitText) || '').stringPlaceholderResolver(self.objectID);

            self.visualSecurityService = new visualSecurityService(self.settings, self.connector);
            self.visualSecurityService.initialize();
            self.isVisible = self.visualSecurityService.isVisible;

            if (!self.signalName) {
                return null;
            }

            self.signal = self.connector.getSignal(self.signalName);

            self.fullNumbers = ko.computed(function () {
                if (self.signal.value() === undefined || self.signal.value() === null || self.signal.value() === "n/a") return "";

                var value = self.signal.value().toString().split(".")[0];

                if (value !== "n/a") {
                    return value.toString().lpad("0", 7);
                }
            }, self);

            self.decimalNumbers = ko.computed(function () {
                var value = self.signal.value();
                if (value === undefined || value === null || self.signal.value() === "n/a") return "";
                value = (Math.round(value * 100) / 100).toFixed(2).toString().split(".")[1];
                return value;
            }, self);

            self.connector.getOnlineUpdates().fail(self.connector.handleError(self));
        };

        wfMeter.prototype.dispose = function () {
            var self = this;

            if (self.visualSecurityService)
                self.visualSecurityService.dispose();

            if (!self.signal)
                return;
            return self.connector.unregisterSignals(self.signal);
        };

        return wfMeter;

    });