define(["require", "exports", "./componentRegistry"], function (require, exports, ComponentRegistry) {
    "use strict";
    var CustomComponentRegistry = /** @class */ (function () {
        function CustomComponentRegistry() {
        }
        CustomComponentRegistry.register = function () {
            // Add here custom component registrations following the pattern:
            // return new ComponentRegistry("src/customComponents")
            //              .mapComponent("componentName")
            return new ComponentRegistry("src/customComponents")
                .mapComponent("custom-value")
                .mapComponent("wf-demo-bhkw")
                .mapComponent("wf-demo-hvac");
        };
        return CustomComponentRegistry;
    }());
    return CustomComponentRegistry;
});
//# sourceMappingURL=customComponentRegistry.js.map