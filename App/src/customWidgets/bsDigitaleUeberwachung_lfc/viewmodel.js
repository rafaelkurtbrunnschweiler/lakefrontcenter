﻿/*-------------------------------------------------------------------------------------------------------------------*
 *                                        Digitale Überwachung
 *-------------------------------------------------------------------------------------------------------------------
 * Erstellt am: 20. April 2017
 * Firma:       Bühler + Scherler
 * Ersteller:   B. Meier
 * Version:     1.0
 *-------------------------------------------------------------------------------------------------------------------
 * Widgetkonfiguration:
 * signalName: Name des zu verbindenden Signals, Bsp.: 'PLC1.IrgendwasAusGCPro'
 * dialogtitle: Titel für das zugehörige Dialogfenster, Bsp.: 'ZUL Ventilator 145M1'
 * authorisierung: Name der Berechtigung, Bsp.: 'Administration'
 * einheit: Die anzuzeigende Einheit, % - ist default, Bsp.: '%'
 * (winkel: bestimmt die Drehung -360° bis 360°, default ist 0, Bsp.: '-45')
 * (sensortyp: Der anzuzeigende Typ wie P, T, F, /, ∆P, ∆T, ∆F, ∆T - ist default, Bsp.: 'T')
 * (stiellaenge:  bestimmt die Länge des Fühlerstiels, default ist 20, Bsp.: '20')
 *-------------------------------------------------------------------------------------------------------------------*/
define(
    [
        '../../services/connector',
        '../../services/securedService',
    ],

    function (signalsConnector, securedService) {
        var digUeberw = function () {
            var self = this;
        };
        $(document).ready(function () {
            digUeberw.prototype = {
                activate: function (settings) {
                    var self = this;
                    self.connector = new signalsConnector();
                    // unwrapper
                    self.stiellaenge = ko.unwrap(settings.stiellaenge) || 20;
                    self.winkel = ko.unwrap(settings.winkel) || 0;
                    self.sensortyp = ko.unwrap(settings.sensortyp) || '∆T';
                    self.modulname = ko.unwrap(settings.signalName);
                    self.signalNameBetriebswahl = self.modulname + ".R_dwBetriebswahl";
                    self.dialogtitle = ko.unwrap(settings.dialogtitle) || 'Dialog Titel undefiniert.';
                    self.authorisierung = ko.unwrap(settings.authorisierung);
                    self.type = ko.unwrap(settings.projectType) || "standard";

                    // connector
                    self.dwZustand = self.connector.getSignal(self.modulname);

                    // read value
                    self.zustand = self.dwZustand.value;

                    self.symbol = ko.computed(function () {
                        return self.glyph;
                    });

                    self.status = ko.computed(function () {
                        self.css = self.symbol();
                        if (self.type == "standard") {
                            switch (self.zustand()) {
                                case 0:
                                    return 'bs_zustand_ein';
                                case 1:
                                    return 'bs_zustand_fehler';
                                default:
                                    return 'bs_zustand_default';
                            }
                        } else {
                            if (self.zustand() != 0) {
                                return 'bs_zustand_ein';
                            } else {
                                return 'bs_zustand_aus';
                            }
                        }
                    });
                    return self.connector.getOnlineUpdates().fail(self.connector.handleError(self));
                },


                detached: function () {
                    var self = this;
                    return self.connector.unregisterSignals(
                        self.dwZustand
                        );
                }
            }
        });
        return digUeberw;
    });