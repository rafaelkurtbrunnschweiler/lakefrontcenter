﻿/*-------------------------------------------------------------------------------------------------------------------*
 *                                              4-PunkteKurve-Punkt
 *-------------------------------------------------------------------------------------------------------------------
 * Erstellt am: 07. November 2017
 * Firma:       Bühler + Scherler
 * Ersteller:   B. Meier
 * Version:     1.0
 *-------------------------------------------------------------------------------------------------------------------
 * Widgetkonfiguration:
 * Button mit Modal-Dialog
 *-------------------------------------------------------------------------------------------------------------------*/
define(
    [
        '../../services/connector',
        '../../services/securedService',
        'plugins/dialog',
        'src/viewModels/dialogs/widgets/vierPunkteKurvePunktDialog'
    ],
    function (signalsConnector, securedService, dialog, vierPunkteKurvePunktDialog) {
        var vierPunkteKurvePunkt = function () {
            var self = this;
        };

        vierPunkteKurvePunkt.prototype = {
            activate: function (settings) {
                var self = this;
                self.settings = settings;
                // unwrap
                self.projectAuthorization = ko.unwrap(settings.projectAuthorization) || "";
                self.securedService = new securedService(self.projectAuthorization);
                self.hasAuthorization = self.securedService.hasAuthorization;

                self.modulname = ko.unwrap(self.settings.signalName);
                self.autorisierung = ko.unwrap(self.settings.authorisierung);
                self.dialogtitle = ko.unwrap(self.settings.dialogTitle) || "Dialog Titel";
                self.links = ko.unwrap(self.settings.handLinks) || "15";
                self.oben = ko.unwrap(self.settings.handOben) || "25";
                self.einheitX = ko.unwrap(self.settings.einheitX) || "°C";
                self.einheitY = ko.unwrap(self.settings.einheitY) || "°C";
                self.groesseX = ko.unwrap(self.settings.groesseX) || "X-Achse";
                self.groesseY = ko.unwrap(self.settings.groesseY) || "Y-Achse";
                self.abszisse1 = ko.unwrap(self.settings.abszisse1) || "Punkt";
                self.abszisse2 = ko.unwrap(self.settings.abszisse2) || "Punkt";
                self.abszisse3 = ko.unwrap(self.settings.abszisse3) || "Punkt";
                self.abszisse4 = ko.unwrap(self.settings.abszisse4) || "Punkt";
                self.ordinate1 = ko.unwrap(self.settings.ordinate1) || "Punkt";
                self.ordinate2 = ko.unwrap(self.settings.ordinate2) || "Punkt";
                self.ordinate3 = ko.unwrap(self.settings.ordinate3) || "Punkt";
                self.ordinate4 = ko.unwrap(self.settings.ordinate4) || "Punkt";
                self.einheit = ko.unwrap(self.settings.einheit) || "°C";

                // define signal names
                self.signalNameAktuellerWert = self.modulname + ".W_rAktuellerWert";
                self.signalNameBetriebswahl = self.modulname + ".R_dwBetriebswahl";
                self.signalNameAbszisse1 = self.modulname + "_Daten.R_rX1";
                self.signalNameOrdinate1 = self.modulname + "_Daten.R_rY1";
                self.signalNameAbszisse2 = self.modulname + "_Daten.R_rX2";
                self.signalNameOrdinate2 = self.modulname + "_Daten.R_rY2";
                self.signalNameAbszisse3 = self.modulname + "_Daten.R_rX3";
                self.signalNameOrdinate3 = self.modulname + "_Daten.R_rY3";
                self.signalNameAbszisse4 = self.modulname + "_Daten.R_rX4";
                self.signalNameOrdinate4 = self.modulname + "_Daten.R_rY4";

                // assign signalname to connector
                self.connector = new signalsConnector();
                self.dwZustand = self.connector.getSignal(self.modulname + ".W_dwZustand");
                self.handuebersteuerung = self.connector.getSignal(self.modulname + ".W_xHanduebersteuert");
                self.betriebswahl = self.connector.getSignal(self.modulname + ".R_dwBetriebswahl");
                self.alarmmeldungen = self.connector.getSignal(self.modulname + ".W_wError");
                self.conOrdinate1 = self.connector.getSignal(self.signalNameOrdinate1);
                self.conOrdinate2 = self.connector.getSignal(self.signalNameOrdinate2);
                self.conOrdinate3 = self.connector.getSignal(self.signalNameOrdinate3);
                self.conOrdinate4 = self.connector.getSignal(self.signalNameOrdinate4);
                self.conAbszisse1 = self.connector.getSignal(self.signalNameAbszisse1);
                self.conAbszisse2 = self.connector.getSignal(self.signalNameAbszisse2);
                self.conAbszisse3 = self.connector.getSignal(self.signalNameAbszisse3);
                self.conAbszisse4 = self.connector.getSignal(self.signalNameAbszisse4);

                // read signal value into variable
                self.zustandValue = self.dwZustand.value;
                self.handuebersteuerungValue = self.handuebersteuerung.value;
                self.betriebswahlValue = self.betriebswahl.value;
                self.alarme = self.alarmmeldungen.value;
                self.ordinate1Value = self.conOrdinate1.value;
                self.ordinate2Value = self.conOrdinate2.value;
                self.ordinate3Value = self.conOrdinate3.value;
                self.ordinate4Value = self.conOrdinate4.value;
                self.abszisse1Value = self.conAbszisse1.value;
                self.abszisse2Value = self.conAbszisse2.value;
                self.abszisse3Value = self.conAbszisse3.value;
                self.abszisse4Value = self.conAbszisse4.value;
                
                self.btnColor = ko.computed(function () {
                    switch (self.zustandValue()) {
                        case 0:
                            return "bs-button-initialisieren"
                        case 2:
                            return "bs-button-error"
                        case 4:
                            return "bs-button-passiv";
                        case 8:
                            return "bs-button-aus";
                        case 16:
                            return "bs-button-notbetrieb";
                        case 32:
                            return "bs-button-ein";
                        case 64:
                            return "bs-button-ein";
                        default:
                            return "bs-button-aus";
                    }
                    return "";
                }),

                self.handbetrieb = ko.computed(function () {
                    if (self.handuebersteuerungValue() != 0) {
                        return 'visible';
                    }
                    else {
                        return 'hidden';
                    }
                });
                return self.connector.getOnlineUpdates().fail(self.connector.handleError(self));
            },

            setBetriebswahl: function (value) {
                var self = this;
                var data = {};
                data[self.signalNameBetriebswahl] = value;
                self.connector.writeSignals(data);
            },

            getAlarmunterdr: function () {
                var self = this;
                var betrwahl = self.betriebswahlValue();
                if ((betrwahl & 1073741824) == 1073741824) {
                    return true;
                }
                else {
                    return false;
                }
            },

            changeBit: function (value) {
                var self = this;
                var data = {};
                var mask = 1 << value;
                var betrValue = self.betriebswahlValue();
                betrValue ^= mask;
                betrValue = betrValue >>> 0;
                data[self.signalNameBetriebswahl] = betrValue;
                self.connector.writeSignals(data);
            },

            setAlarmQuit: function (value) {
                var self = this;
                var data = {};
                var mask = 1 << value;
                var betrValue = self.betriebswahlValue();
                betrValue |= mask;
                betrValue = betrValue >>> 0;
                data[self.signalNameBetriebswahl] = betrValue;
                self.connector.writeSignals(data);
            },

            openDialog: function () {
                var self = this;
                if (self.zustandValue() != 4) {
                    dialog.show(new vierPunkteKurvePunktDialog(self)).then(function (dialogResult) {
                        console.log(dialogResult);
                    });
                }
            },

            detached: function () {
                var self = this;
                return self.connector.unregisterSignals(
                    self.dwZustand,
                    self.handuebersteuerung,
                    self.betriebswahl,
                    self.alarmmeldungen,
                    self.conOrdinate1,
                    self.conOrdinate2,
                    self.conOrdinate3,
                    self.conOrdinate4,
                    self.conAbszisse1,
                    self.conAbszisse2,
                    self.conAbszisse3,
                    self.conAbszisse4
                    );
            }
        };
        return vierPunkteKurvePunkt;
    });