﻿/*-------------------------------------------------------------------------------------------------------------------*
 *                                              Anlageschalter
 *-------------------------------------------------------------------------------------------------------------------
 * Erstellt am: 23. Mai 2017
 * Firma:       Bühler + Scherler
 * Ersteller:   B. Meier
 * Version:     1.0
 *-------------------------------------------------------------------------------------------------------------------
 * Widgetkonfiguration:
 * signalName: Name des zu verbindenden Signals, Bsp.: 'PLC1.IrgendwasAusGCPro'
 * (signalNameHws: Name des zu verbindenden Signals, Bsp.: 'PLC1.IrgendwasAusGCPro')
 *-------------------------------------------------------------------------------------------------------------------*/
define(
    [
        '../../services/connector',
        'plugins/dialog',
        'src/viewModels/dialogs/widgets/anlageschalter_2Stufen_lfcDialog'
    ],

    function (signalsConnector, dialog, anlageschalter_2Stufen_lfcDialog) {
        var anlageschalter = function () {
            var self = this;
        };

        anlageschalter.prototype = {
            activate: function (settings) {
                var self = this;



                self.settings = settings;
                // unwrap
                self.dialogtitle = ko.unwrap(settings.dialogTitle) || "Betriebswahlschalter";
                self.links = ko.unwrap(settings.handLinks) || "15px";
                self.oben = ko.unwrap(settings.handOben) || "25px";
                self.modulname = ko.unwrap(settings.signalName);
                self.strompfad = ko.unwrap(settings.stromPfad);
                self.modulnameHws = ko.unwrap(settings.signalNameHws) || "empty";
                self.signalNameBetriebswahl = ko.unwrap(settings.signalName) + ".Register.betriebsart_" + ko.unwrap(settings.stromPfad); // Betriebsart
                self.test = ko.unwrap(settings.Test);

                // assign signalname to connector
                self.connector = new signalsConnector();
                self.dwZustand = self.connector.getSignal(self.modulname + ".Register.betriebszustand_" + self.strompfad);
                //self.betriebswahl = self.connector.getSignal(self.modulname + ".Register.betriebsart_" + self.strompfad);
               // self.signalNameBetriebswahl = self.connector.getSignal(self.modulname + ".Register.betriebsart_" + self.stromPfad); // Betriebsart

                if (self.modulnameHws !== "empty") {
                    self.dwZustandHws = self.connector.getSignal(self.modulnameHws + ".W_dwZustand");
                    self.zustandHwsValue = self.dwZustandHws.value;
                }
                // read signal value into variable
                self.zustandValue = self.dwZustand.value;


                self.btnColor = ko.computed(function () {
                    switch (self.zustandValue()) {
                        case 0:
                            return "bs-button-aus"
                        case 1:
                            return "bs-button-ein"
                        case 2:
                            return "bs-button-ein";
                        case 3:
                            return "bs-button-ein";
                        default:
                            return "bs-button-aus";
                    }
                    return "";


                });


                return self.connector.getOnlineUpdates().fail(self.connector.handleError(self));
            },


            openDialog: function () {
                var self = this;
                if (self.zustandValue() != 4) {
                    dialog.show(new anlageschalter_2Stufen_lfcDialog(self)).then(function (dialogResult) {
                        console.log(dialogResult);
                    });
                }
            },

            detached: function () {
                var self = this;
                return self.connector.unregisterSignals(
                    self.dwZustand
                    );
            }
        };
        return anlageschalter;
    });