﻿/*-------------------------------------------------------------------------------------------------------------------*
 *                                Ventil 2-Weg, 3-Weg
 *-------------------------------------------------------------------------------------------------------------------
 * Erstellt am: 20. April 2017
 * Firma:       Bühler + Scherler
 * Ersteller:   B. Meier
 * Version:     1.0
 *-------------------------------------------------------------------------------------------------------------------
 * Widgetkonfiguration:
 * signalName: Name des zu verbindenden Signals, Bsp.: 'PLC1.IrgendwasAusGCPro'
 * dialogtitle: Titel für das zugehörige Dialogfenster, Bsp.: 'ZUL Ventilator 145M1'
 * authorisierung: Name der Berechtigung, Bsp.: 'Administration'
 * trendConfig: Name des zu ladenden Trends, Bsp.: 'myTrend1'
 * (symbol: Das anzuzeigende Symbol, default ist: bs bs-ventil fa-3x (B+S Symbol für ein 2-Weg Ventil, 3-fach vergrössert)
 *      für das 3-Weg Ventil gilt folgendes Symbol: bs bs-ventil3, Bsp.: 'bs bs-ventil3'
 * (einheit: Die anzuzeigende Einheit, °C - ist default, Bsp.: ' %')
 * (winkel: bestimmt die Drehung -360° bis 360°, default ist 0, Bsp.: '-45')
 * (anzeigefeldOben: Anzeige Abstand vom oberen Widget Rand, default ist 10, Bsp.: '10')
 * (anzeigefeldLinks: Anzeige Abstand vom linken Widget Rand, default ist -62, Bsp.: '-62')
 * (handLinks: Hand Abstand vom linken Widget Rand, default ist -10, Bsp.: '-10')
 * (handOben:  Hand Abstand vom oberen Widget Rand, default ist 10, Bsp.: '10')
 *-------------------------------------------------------------------------------------------------------------------*/
define(
    [
        '../../services/connector',
    ],

    function (signalsConnector) {
        var ventil = function () {
            var self = this;

        };
        $(document).ready(function () {
            ventil.prototype = {
                activate: function (settings) {
                    var self = this;
                    self.connector = new signalsConnector();
                    // unwrapper
                    self.glyph = ko.unwrap(settings.symbol) || "bs bs-ventil fa-3x";
                    self.modulname = ko.unwrap(settings.signalName);
                    self.handLinks = ko.unwrap(settings.handLinks) || -10;
                    self.handOben = ko.unwrap(settings.handOben) || 10;
                    self.anzeigefeldLinks = ko.unwrap(settings.anzeigefeldLinks) || -62;
                    self.anzeigefeldOben = ko.unwrap(settings.anzeigefeldOben) || 10;
                    self.winkel = ko.unwrap(settings.winkel) || 0;
                    self.unit = ko.unwrap(settings.einheit) || '%';
                    self.typ = ko.unwrap(settings.typ) || 'stetig';

                    self.signalNameBetriebswahl = self.modulname + ".R_dwBetriebswahl";
                    self.signalname = self.modulname + ".W_rAktuellerWert";


                    self.connector = new signalsConnector();
                    self.dwZustand = self.connector.getSignal(self.modulname);

                    self.zustand = self.dwZustand.value;

                    self.symbol = ko.computed(function () {
                        return self.glyph + ' fa-3x';
                    });



                    self.status = ko.computed(function () {
                        self.css = self.symbol();
                        if (self.zustand() > 0) {
                            return self.css + ' bs_ein';
                        } else {
                            return self.css + ' bs_aus';
                        }
                    });

                    self.digital = ko.computed(function () {
                        if (self.typ === 'digital')
                            return false;
                        return true;
                    });

                    return self.connector.getOnlineUpdates().fail(self.connector.handleError(self));
                },



                detached: function () {
                    var self = this;
                    return self.connector.unregisterSignals(
                        self.dwZustand
                        );
                }
            }

        })
        return ventil;
    });