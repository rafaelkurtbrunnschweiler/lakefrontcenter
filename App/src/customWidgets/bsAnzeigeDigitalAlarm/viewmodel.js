﻿/*
    Bühler+Scherler

    Anzeige Digital
    -----------------------

    Zeigt einen Text an und ein Rechteck, welches je nach Status die Farbe ändert.
    Mögliche Status sind 0 und nicht 0.


    Eigenschaften in Klammern sind optional.

    Eigenschaft             Typ         Default         Beschreibung
    --------------------------------------------------------------------------

    signalName              String                      Variable für den Status

    (label)                 String                      Angezeigter Text

    (colors)                array       [ '#7cff7c',    Array mit Farben.
                            of String     '#ff0000' ]   colors[0]: Farbe wenn Status = 0
                                                        colors[1]: Farbe wenn Status != 0

*/
define(
    [
        '../../services/connector',
        '../../services/securedService'
    ],
    function (
        signalsConnector,
        securedService
    ) {

        // constructor
        var AnzeigeDigital = function () {

        };

        AnzeigeDigital.prototype.activate = function (settings) {

            var self = this;

            self.connector = new signalsConnector();

            self.projectAuthorization = ko.unwrap(settings.projectAuthorization) || '';
            self.securedService = new securedService(self.projectAuthorization);
            self.hasAuthorization = self.securedService.hasAuthorization;

            self.settings = settings;
            self.signalName = ko.unwrap(settings.signalName) || null;
            self.label = ko.unwrap(settings.label) || null;


            // Stop here and return if no signalName was configured
            if (!self.signalName) {
                throw new Error('missing widget parameters');
            }

            self.signal = self.connector.getSignal(ko.unwrap(self.signalName));
            self.signalValue = self.signal.value;

            self.color = ko.computed(function () {
                
                var state = self.signalValue();
                
                if (state === undefined || state === null) {
                    return '#bfbfbf';
                } 
                if (state === 32) {
                    return '#21ff00';
                }
                if (state === 8) {
                    return '#bfbfbf';
                }
               
            });

            return self.connector.getOnlineUpdates().fail(self.connector.handleError(self));
        };

        AnzeigeDigital.prototype.detached = function () {

            var self = this;

            if (self.signal) {
                self.connector.unregisterSignals(self.signal);
            }
        };

        return AnzeigeDigital;
    });