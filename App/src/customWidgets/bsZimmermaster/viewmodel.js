﻿/*-------------------------------------------------------------------------------------------------------------------*
*                                              Zimmermaster
*-------------------------------------------------------------------------------------------------------------------
* Erstellt am: 12. Januar 2018
* Firma:       Bühler + Scherler
* Ersteller:   B. Meier / R. Brunnschweiler
* Version:     1.0
*-------------------------------------------------------------------------------------------------------------------
* Widgetkonfiguration:
* Button mit Modal-Dialog
* Im Dialog wird das aktuelle Zimmer angezeigt, bzw. festgelegt. 
 * 
 * Attribute
* dialogTitel: Titeltext des Modal-Dialoges
* modulName: Name des zu verbindenden Signals, Bsp.: 'PLC1.IrgendwasAusGCPro'
*
* Beispielkonfiguration
<div class="bs-heizbetrieb" data-bind="bsHeizbetrieb: {
    dialogTitle: 'Heizbetrieb',
       authorisierung: 'Administration',
       modulName: 'PLC1.plcObject',
       modulNameAT: 'PLC1.plcObjectAussentemperatur'
       }">
</div>
*-------------------------------------------------------------------------------------------------------------------*/
define(
       [
             '../../services/connector',
             '../../services/securedService',
             'plugins/dialog',
             'src/viewModels/dialogs/widgets/zimmermasterDialog'
       ],

       function (signalsConnector, securedService, dialog, zimmermasterDialog) {
           var zimmermaster = function () {
               var self = this;
           };

           zimmermaster.prototype = {
               activate: function (settings) {
                   var self = this;
                   self.settings = settings;
                   self.connector = new signalsConnector();

                   self.projectAuthorization = ko.unwrap(settings.authorisierung) || "none";
                   self.securedService = new securedService(self.projectAuthorization);
                   self.hasAuthorization = self.securedService.hasAuthorization;
                   // --------------------------------------------------------------------
                   // HTML Paremeters

                   //self.trendConfig = ko.unwrap(settings.trendConfig);
                   self.dialogTitle = ko.unwrap(settings.dialogTitle) || "Dialog Titel";
                   //self.sollwertText = ko.unwrap(settings.sollwertText) || "Aussentemperatur:";
                   //self.zeitraumMittelwertBildung = ko.unwrap(settings.zeitraumMittelwertBildung) || "Aussentemperatur 24h Mittelwert:";
                   self.einheit = ko.unwrap(settings.einheit) || '°C';
                   self.Kelvin = ko.unwrap(settings.Kelvin) || 'K';
                   self.prozent = ko.unwrap(settings.prozent) || '%';

                   // PLC Variable Name
                   self.mastername = ko.unwrap(settings.masterName);
                   self.roomcontrolname = ko.unwrap(settings.roomcontrolName);
                   self.roomnumber = ko.unwrap(settings.roomNumber);
                   self.basissollwert = ko.unwrap(settings.masterName) + ".Register.Allg.SW.Aktuell";
                   self.basissollwertwinter = ko.unwrap(settings.masterName) + ".Register.Allg.SW.Winter_von_GLS";
                   self.basissollwertsommer = ko.unwrap(settings.masterName) + ".Register.Allg.SW.Sommer";
                   self.outsidetempone = ko.unwrap(settings.masterName) + ".Register.A.HVC.AT_Schiebung.RefTo1";
                   self.outsidetemptwo = ko.unwrap(settings.masterName) + ".Register.A.HVC.AT_Schiebung.RefTo2";
                   self.setpointone = ko.unwrap(settings.masterName) + ".Register.A.HVC.AT_Schiebung.RefSP1";
                   self.setpointtwo = ko.unwrap(settings.masterName) + ".Register.A.HVC.AT_Schiebung.RefSP2";
                   self.heizventillock = ko.unwrap(settings.masterName) + ".Flag.Allg.Sperrung.H_Ventile";
                   self.kuehlventillock = ko.unwrap(settings.masterName) + ".Flag.Allg.Sperrung.K_Ventile";
                   self.heizventillocked = ko.unwrap(settings.roomcontrolName) + ".Flag.Z" + ko.unwrap(settings.roomNumber) + ".Sperrung.rcv_Heizventil";
                   self.kuehlventillocked = ko.unwrap(settings.roomcontrolName) + ".Flag.Z" + ko.unwrap(settings.roomNumber) + ".Sperrung.rcv_Kuehlventil";
                   self.sommerwinter = ko.unwrap(settings.masterName) + ".Flag.Allg.Sommerbetrieb";
                   self.outsidetempist = ko.unwrap(settings.masterName) + ".Register.Allg.AT_Ist";


                   // PLC Signals
                   self.plcheizventillocked = self.connector.getSignal(self.heizventillocked);
                   self.plckuehlventillocked = self.connector.getSignal(self.kuehlventillocked);



                   // PLC Values
                   self.plcheizventillockedValue = self.plcheizventillocked.value;
                   self.plckuehlventillockedValue = self.plckuehlventillocked.value;


                   // Object Configuration
               },

               openDialog: function () {
                   var self = this;
                   dialog.show(new zimmermasterDialog(self)).then(function (dialogResult) {
                       console.log(dialogResult);
                   });
               },

               detached: function () {
                   var self = this;
                   return self.connector.unregisterSignals(
                       self.plcheizventillocked,
                       self.plckuehlventillocked
               )
               }

           };
           return zimmermaster;
       });