/*-------------------------------------------------------------------------------------------------------------------*
 *                                              Bedarfsmeldungen
 *-------------------------------------------------------------------------------------------------------------------
 * Erstellt am: 27. Juni 2017
 * Firma:       Bühler + Scherler
 * Ersteller:   J. Scherrer / J.Koster
 * Version:     1.0
 *-------------------------------------------------------------------------------------------------------------------
 * Widgetkonfiguration:
 * Button mit Modal-Dialog
 * Im Dialog wird eine beliebige Anzahl von Einträgen angezeigt. Zu jedem Eintrag kann ein Signalname und der Text angegeben werden. Diese werden mit den Zusands-Parametern verglichen und deren Zustand in der entsprechenden Farbe signalisiert.
 *
 * Widgetkonfiguration:
 * dialogTitel: Titeltext des Modal-Dialoges
 * signale: Array mit Signalen die ausgewertet und aufgelistet werden sollen, bestehend aus:
 * signalName: PLC-Variable die ausgewertet wird
 * text: Text zum Signal (wird im Dialog angezeigt)
 * zustaende: Array mit Zuständen, mit denen die Signale verglichen werden, bestehend aus:
 * vergleichswert: Vergleichswert für Signal
 * farbe: Zum Vergleichswert zugehörige Farbe des Icons
 *
 * Beispielkonfiguration
 bsBedarfsmeldungen: {
 						 dialogTitle: 'Bedarfsmeldungen Lüftung',
 						 authorisierung: 'Administration',
 						 signale: [
 						 { signalName: 'PLC1.IrgendwasAusGCPro.W_dwZustand', text: 'Bedarfsmeldung 1.OG'},
 						 { signalName: 'PLC1.IrgendNochwasAusGCPro.W_dwZustand', text: 'Bedarfsmeldung 2.OG'},
 						 { signalName: 'PLC1.IrgendwasAusGCPro.W_dwZustand', text: 'Bedarfsmeldung 3.OG'},
 						 ],
 						 zustaende: [
 						 { vergleichswert: '0', farbe: 'color-initialisieren'},
 						 { vergleichswert: '4', farbe: 'color-fehler'},
						 { vergleichswert: '8', farbe: 'color-aus'},
 						 { vergleichswert: '16', farbe: 'color-notbetrieb'},
 						 { vergleichswert: '32', farbe: 'color-ein'},
 						 ]
 						 }
	 *--------------------------------------------------
 	* Farben:
 	* color-hellgrau: #ccc;
 	* color-handfarbe: #ff8555;
 	* color-initialisieren: #ff8500;
 	* color-fehler: #ff0000;
 	* color-ein: #00ff00;
 	* color-aus: #6e6e6e;
 	* color-service: #0000ff;
 	* color-notbetrieb: #ffff00;
 	* color-fahrend: #fdfdfd;
 	* color-akzent: #bebebe;
 	* color-normal: #000;
 	* color-invers: #fff;
 	* color-sollwert: #337ab7;
 	* color-berechnter-sollwert: #ffff99;
 	* color-border: #333;
 	* color-schaltuhr-aktiv: #0044ff;
 	* color-schaltuhr-zeit-ein: #aef1ae;
 	* color-schaltuhr-zeit-aus: #ffe7e7;
 	* color-nav-highlite: #b4d0d7;
 	* color-hover-notbetrieb: #ffe200;
	 *-------------------------------------------------------------------------------------------------------------------*/
define(
	[
		'../../services/connector',
		'../../services/securedService',
		'plugins/dialog',
		'src/viewModels/dialogs/widgets/bedarfsmeldungenDialog'
	],

	function(signalsConnector, securedService, dialog, bedarfsmeldungenDialog) {
		var bedarfsmeldungen = function() {
			var self = this;
		};

		function Bedarf(text, zustaende, wert) {
			var self = this;
			self.text = text;
			self.zustaende = zustaende;
			self.wert = wert.value;

			self.zustands_farbe = ko.computed(function() {
				var farbe = 'color-normal';
				zustaende.forEach(function(zustand){
					if (zustand.vergleichswert == self.wert()){
						farbe = zustand.farbe;
					}
				});
				return farbe;
			});
		};

		bedarfsmeldungen.prototype = {
			activate: function(settings) {
				var self = this;

				self.projectAuthorization = ko.unwrap(settings.projectAuthorization) || "";
				self.securedService = new securedService(self.projectAuthorization);
				self.hasAuthorization = self.securedService.hasAuthorization;

				self.settings = settings;
				self.connector = new signalsConnector();

				self.dialogTitle = ko.unwrap(settings.dialogTitle) || "Dialog Titel";
				self.signale = ko.unwrap(settings.signale);
				self.zustaende = ko.unwrap(settings.zustaende);
				self.bedarfsmeldungenObj = [];

				self.signale.forEach(function(sig) {
					self.wert = self.connector.getSignal(ko.unwrap(sig.signalName));
					self.bedarfsmeldungenObj.push(new Bedarf(sig.text, self.zustaende, self.wert));
				});
				return self.connector.getOnlineUpdates().fail(self.connector.handleError(self));
			},
			openDialog: function() {
				var self = this;
				if (true) {
					dialog.show(new bedarfsmeldungenDialog(self)).then(function(dialogResult) {
						console.log(dialogResult);
					});
				}
			},

			detached: function () {
					var self = this;

					var signals = [];

					self.bedarfsmeldungenObj.forEach(function(obj) {
						signals.push(obj.wert);
					});

					return self.connector.unregisterSignals(
								signals
							);
			}
		};
		return bedarfsmeldungen;
	});
