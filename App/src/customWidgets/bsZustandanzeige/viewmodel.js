﻿define(
    [
        '../../services/connector',
        '../../services/securedService',
        'plugins/dialog',
        'src/viewModels/dialogs/widgets/sensorDialog'
    ],

    function (signalsConnector, securedService, dialog, sensorDialog) {
        var zustandanzeige = function () {
            var self = this;
        };

        $(document).ready(function () {
            zustandanzeige.prototype = {
                activate: function (settings) {
                    var self = this;
                    self.connector = new signalsConnector();
                    // unwrapper
                    self.modulname = ko.unwrap(settings.signalName);
                    self.farbeAktiv = ko.unwrap(settings.farbeAktiv);
                    self.farbeInaktiv = ko.unwrap(settings.farbeInaktiv);
                    self.legende = ko.unwrap(settings.legende);
                    self.vergleichsKonstante = ko.unwrap(settings.vergleichsKonstante);
                    // Signale dem Connector zuweisen
                    self.zustand = self.connector.getSignal(self.modulname);
                    // Signalwert lesen
                    self.zustandValue = self.zustand.value;

                    self.sensorzustand = ko.computed(function () {
                        if (self.vergleichsKonstante === self.zustandValue()) {
                            return self.farbeAktiv;
                        }
                        else {
                            return self.farbeInaktiv;
                        }
                    });
                    return self.connector.getOnlineUpdates().fail(self.connector.handleError(self));
                }
            }
        });
        return zustandanzeige;
    });