﻿/*-------------------------------------------------------------------------------------------------------------------*
 *                                        Motor mehrstufig (max. 3 Stufen)
 *-------------------------------------------------------------------------------------------------------------------
 * Erstellt am: 20. April 2017
 * Firma:       Bühler + Scherler
 * Ersteller:   B. Meier
 * Version:     1.0
 *-------------------------------------------------------------------------------------------------------------------
 * Widgetkonfiguration:
 * signalName: Name des zu verbindenden Signals, Bsp.: 'PLC1.IrgendwasAusGCPro'
 * dialogTitle: Titel für das zugehörige Dialogfenster, Bsp.: 'ZUL Ventilator 145M1'
 * authorisierung: Name der Berechtigung, Bsp.: 'Administration'
 * (motorAusrichtung: dreht das Widget in die gewünschte Richtung, default ist oben, Bsp.: 'oben', 'rechts', 'unten', 'links')
 * (handLinks: Hand Abstand vom linken Widget Rand, default ist 20, Bsp.: '20')
 * (handOben:  Hand Abstand vom oberen Widget Rand, default ist 5, Bsp.: '5')
 * (motortyp: Typ des anzuzeigenden Symbols, default ist pumpe, Bsp.: 'ventilator', 'pumpe')
 * (stufe: Für mehrstufige Geräte von 0 bis 3, default ist 0, Bsp.: '0')
 *-------------------------------------------------------------------------------------------------------------------*/
define(
    [
        '../../services/connector',
        'plugins/dialog',
        'src/viewModels/dialogs/widgets/motorDialogStufen_lfc'
    ],

    function (signalsConnector, dialog, motorDialogStufen_lfc) {
        var bsMotor = function () {
            var self = this;
        };

        bsMotor.prototype = {
            activate: function (settings) {
                var self = this;

                self.settings = settings;
                self.modulname = ko.unwrap(settings.signalName);
                self.strompfad = ko.unwrap(settings.stromPfad);
                self.signalNameBetriebswahl = ko.unwrap(settings.signalName) + ".Register.betriebsart_" + ko.unwrap(settings.stromPfad); // Betriebsart

                self.signalNameZaehlerReset1 = ko.unwrap(settings.signalName) + ".Flag.reset_work_" + ko.unwrap(settings.stromPfad); // Reset;

                self.connector = new signalsConnector();
                self.dwZustand = self.connector.getSignal(self.modulname + ".Register.betriebszustand_" + self.strompfad);
                self.dwZustandMotor = self.connector.getSignal(self.modulname + ".Register.status_" + self.strompfad);
                self.betriebswahl = self.connector.getSignal(ko.unwrap(settings.signalName) + ".Register.betriebsart_" + ko.unwrap(settings.stromPfad));
                self.einschTotal = self.connector.getSignal(ko.unwrap(settings.signalName) + ".Register.work_" + ko.unwrap(settings.stromPfad) + "_04");
                self.betrStdTotal = self.connector.getSignal(ko.unwrap(settings.signalName) + ".Register.work_" + ko.unwrap(settings.stromPfad) + "_01");
                self.handbetrieb = self.connector.getSignal(self.modulname + ".W_xHanduebersteuert");

                self.objektZustand = self.dwZustand.value;
                self.objektZustandMotor = self.dwZustandMotor.value;
                self.betriebswahlValue =    self.betriebswahl.value;
                self.betrStdTotalValue =    self.betrStdTotal.value;
                self.einschTotalValue = self.einschTotal.value;
                self.handbetriebValue = self.handbetrieb.value;

                self.dialogtitle = ko.unwrap(settings.dialogTitle) || "Dialog Titel";
                self.motortyp = ko.unwrap(settings.motortyp) || "pumpe";
                self.objektAusrichtung = ko.unwrap(settings.motorAusrichtung) || "oben";
                self.mehrstufig = ko.unwrap(settings.stufe) || "0";
                self.links = ko.unwrap(settings.handLinks) || -10;
                self.oben = ko.unwrap(settings.handOben) || 10;
                self.einheit = ko.unwrap(settings.einheit) || " h";
                self.dezimalstellen = ko.unwrap(settings.dezimalstellen) || 0;

                self.roundbetrStdTotal = ko.computed(function () {
                    var wert = self.betrStdTotalValue();
                    if (self.dezimalstellen != 99) {
                        return result = (round(self.betrStdTotalValue(), self.dezimalstellen)).toFixed(self.dezimalstellen) + " " + self.einheit;
                    }
                    var result = (self.betrStdTotalValue() + " " + self.einheit);
                    return result;
                });

                self.stufen = ko.computed(function () {
                    if (self.mehrstufig != 0) {
                        return 'visible';
                    }
                    else {
                        return 'hidden';
                    }
                }),

                self.pumpe = ko.computed(function () {
                    if (self.motortyp == 'pumpe') {
                        return 'visible';
                    }
                    else {
                        return 'hidden';
                    }
                }),

                self.ventilator = ko.computed(function () {
                    if (self.motortyp == 'ventilator') {
                        return 'visible';
                    }
                    else {
                        return 'hidden';
                    }
                }),

                self.ausrichtung = ko.computed(function () {
                    switch (self.objektAusrichtung) {
                        case 'oben':
                            return 'wf-n';
                        case 'unten':
                            return 'wf-s';
                        case 'links':
                            return 'wf-w';
                        case 'rechts':
                            return 'wf-o';
                    }
                }),

                self.aktiv = ko.computed(function () {
                    if (self.objektZustand() == 4) {
                        return '1';
                    }
                    else {
                        return '1';
                    }
                }),

                self.zustand = ko.computed(function () {
                    switch (self.objektZustandMotor()) {
                        case 0:
                            return '#d6d6d6'; // Pumpe AUS
                        case 1:
                            return '#ffff00'; // Notbetrieb
                        case 2:
                            return '#00ff00'; // Pumpe läuft
                        case 5:
                            return '#ff0000'; // Störung
                        default:
                            return '#d6d6d6';
                    }

                }),

                self.betriebsstufe = ko.computed(function () {
                    switch (self.objektZustandMotor()) {
                        case 0:
                            return '0';
                        case 2:
                            return 'E';
                        case 4:
                            return '';
                        case 8:
                            return '0';
                        case 16:
                            return 'N';
                        case 32:
                            return '1';
                        case 64:
                            return '2';
                        case 128:
                            return '3';
                        default:
                            return '?';
                    }
                }),

                self.motorHand = ko.computed(function () {
                    if (self.handbetriebValue() != 0)
                        return 'visible';
                    else
                        return 'hidden';
                });

                return self.connector.getOnlineUpdates().fail(self.connector.handleError(self));
            },

            openDialog: function () {
                var self = this;
                dialog.show(new motorDialogStufen_lfc(self)).then(function (dialogResult) {
                    console.log(dialogResult);
                });
            },

            setBetriebswahl: function (value) {
                var self = this;
                var data = {};
                data[self.signalNameBetriebswahl] = value;
                self.connector.writeSignals(data);
            },

            resetZaehlerSt1: function (value) {
                var self = this;
                var data = {};
                data[self.signalNameZaehlerReset1] = value;
                self.connector.writeSignals(data);
            },


            changeBit: function (value) {
                var self = this;
                var data = {};
                var mask = 1 << value;
                var betrValue = self.betriebswahlValue();
                betrValue ^= mask;
                betrValue = betrValue >>> 0;
                data[self.signalNameBetriebswahl] = betrValue;
                self.connector.writeSignals(data);
            },


            detached: function () {
                var self = this;
                return self.connector.unregisterSignals(
                    self.signal,
                    self.dwZustand,
                    self.dwZustandMotor,
                    self.betriebswahl,
                    self.betrStdTotal,
                    self.einschTotal
                    );
            }
        };

        function round(wert, dez) {
            wert = parseFloat(wert);
            if (!wert) return 0;
            dez = parseInt(dez);
            if (!dez) dez = 0;
            var umrechnungsfaktor = Math.pow(10, dez);
            return Math.round(wert * umrechnungsfaktor) / umrechnungsfaktor;
        }

        return bsMotor;
    });