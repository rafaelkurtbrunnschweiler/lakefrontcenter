﻿/*-------------------------------------------------------------------------------------------------------------------*
 *                                  Motor mit Frequenzumformer (FU)
 * ------------------------------------------------------------------------------------------------------------------
 * Erstellt am: 19. April 2017
 * Firma:       Bühler + Scherler
 * Ersteller:   B. Meier
 * Version:     1.0
 * ------------------------------------------------------------------------------------------------------------------
 * Widgetkonfiguration:
 * signalName: Name des zu verbindenden Signals, Bsp.: 'PLC1.IrgendwasAusGCPro'
 * signalNameFu: Name des zu verbindenden Signals, Bsp.:'PLC1.IrgendwasAusGCProFU'
 * fuTyp: Anzuzeigende Beschriftung im FU - Feld, Bsp.: 'FU'
 * dialogTitle: Titel für das zugehörige Dialogfenster, Bsp.: 'ZUL Ventilator 145M1'
 * authorisierung: Name der Berechtigung, Bsp.: 'Administration'
 * trendConfig: Name des zu ladenden Trends, Bsp.: 'myTrend1'
 * (motorAusrichtung: dreht das Widget in die gewünschte Richtung, default ist oben, Bsp.: 'oben', 'rechts', 'unten', 'links')
 * (handLinks: Hand Abstand vom linken Widget Rand, default ist 20, Bsp.: '20')
 * (handOben:  Hand Abstand vom oberen Widget Rand, default ist 5, Bsp.: '5')
 * (motortyp: Typ des anzuzeigenden Symbols, default ist ventilator, Bsp.: 'ventilator', 'pumpe')
 * (fu: Ob das Anzeigefeld für den FU angezeigt werden soll, default ist nein, Bsp.: 'ja', 'nein')
 * (fuOben: FU - Anzeige Abstand vom oberen Widget Rand, default ist 33, Bsp.: '33')
 * (fuLinks: FU - Anzeige Abstand vom linken Widget Rand, default ist -20, Bsp.: '-20')
 * (einheit: Die anzuzeigende Einheit, default ist %, Bsp.: '%')
 * (handLinksFu: Hand Abstand vom linken Widget Rand, default ist 65, Bsp.: '65')
 * (handObenFu: Hand Abstand vom oberen Widget Rand, default ist 5, Bsp.: '5')
 *-------------------------------------------------------------------------------------------------------------------*/

define(
    [
        '../../services/connector',
        '../../services/securedService',
        'plugins/dialog',
        'src/viewModels/dialogs/widgets/motorDialogFU'
    ],

    function (signalsConnector, securedService, dialog, motorDialogFU) {
        var bsMotor = function () {
            var self = this;
        };

        bsMotor.prototype = {
            activate: function (settings) {
                var self = this;

                self.projectAuthorization = ko.unwrap(settings.projectAuthorization) || "";
                self.securedService = new securedService(self.projectAuthorization);
                self.hasAuthorization = self.securedService.hasAuthorization;

                self.settings = settings;
                self.modulname = ko.unwrap(settings.signalName);

                self.signalNameBetriebswahl = ko.unwrap(settings.signalName) + ".R_dwBetriebswahl";
                self.signalNameBetriebswahlFu = ko.unwrap(settings.signalNameFu) + ".R_dwBetriebswahl";
                self.signalNameFestwert = ko.unwrap(settings.signalNameFu) + ".R_rFestwert";
                self.signalNameTotzone = ko.unwrap(settings.signalNameFu) + ".R_CFG_rTotzone";
                self.signalNameUntergrenze = ko.unwrap(settings.signalNameFu) + ".R_rUntergrenze";
                self.signalNameObergrenze = ko.unwrap(settings.signalNameFu) + ".R_rObergrenze";
                self.signalNameZaehlerReset1 = ko.unwrap(settings.signalName) + "_Zaehler.R_xReset";
                self.autorisierung = ko.unwrap(settings.authorisierung);
                self.trendConfig = ko.unwrap(settings.trendConfig);

                self.connector = new signalsConnector();
                self.dwZustand = self.connector.getSignal(ko.unwrap(settings.signalName) + ".W_dwZustand");
                self.handbetrieb = self.connector.getSignal(ko.unwrap(settings.signalName) + ".W_xHanduebersteuert");
                self.alarmmeldungen = self.connector.getSignal(ko.unwrap(settings.signalName) + ".W_wError");
                self.betriebswahl = self.connector.getSignal(ko.unwrap(settings.signalName) + ".R_dwBetriebswahl");
                self.einsch1St = self.connector.getSignal(ko.unwrap(settings.signalName) + "_Zaehler.W_uiSchaltungen");
                self.einschTotal = self.connector.getSignal(ko.unwrap(settings.signalName) + "_Zaehler.W_uiSchaltungenTotal");
                self.betrStd1st = self.connector.getSignal(ko.unwrap(settings.signalName) + "_Zaehler.W_rBetriebsstunden");
                self.betrStdTotal = self.connector.getSignal(ko.unwrap(settings.signalName) + "_Zaehler.W_rBetriebsstundenTotal");

                self.betriebswahlFu = self.connector.getSignal(ko.unwrap(settings.signalNameFu) + ".R_dwBetriebswahl");
                self.dwZustandFu = self.connector.getSignal(ko.unwrap(settings.signalNameFu) + ".W_dwZustand");
                self.handbetriebFu = self.connector.getSignal(ko.unwrap(settings.signalNameFu) + ".W_xHanduebersteuert");
                self.rAktuellerWert = self.connector.getSignal(ko.unwrap(settings.signalNameFu) + ".W_rAktuellerWert");
                self.rFestwert = self.connector.getSignal(ko.unwrap(settings.signalNameFu) + ".R_rFestwert");
                self.rTotzone = self.connector.getSignal(ko.unwrap(settings.signalNameFu) + ".R_CFG_rTotzone");
                self.rUntergrenze = self.connector.getSignal(ko.unwrap(settings.signalNameFu) + ".R_rUntergrenze");
                self.rObergrenze = self.connector.getSignal(ko.unwrap(settings.signalNameFu) + ".R_rObergrenze");

                self.objektZustand =        self.dwZustand.value;
                self.objektHandbetrieb =    self.handbetrieb.value;
                self.alarme =               self.alarmmeldungen.value;
                self.motorleistung =        self.rAktuellerWert.value;
                self.fuHandbetrieb =        self.handbetriebFu.value;
                self.fuZustand =            self.dwZustandFu.value;
                self.betriebswahlValue =    self.betriebswahl.value;
                self.betriebswahlValueSwFu = self.betriebswahlFu.value;
                self.betrStd1stValue =      self.betrStd1st.value;
                self.betrStdTotalValue =    self.betrStdTotal.value;
                self.einsch1StValue =       self.einsch1St.value;
                self.einschTotalValue =     self.einschTotal.value;
                self.festwertValue =        self.rFestwert.value;
                self.totzoneValue =         self.rTotzone.value;
                self.untergrenzeValue =     self.rUntergrenze.value;
                self.obergrenzeValue =      self.rObergrenze.value;

                self.dialogtitle = ko.unwrap(settings.dialogTitle) || "Dialog Titel";
                self.objektAusrichtung = ko.unwrap(settings.motorAusrichtung) || "oben";
                self.handLinks = ko.unwrap(settings.handLinks) || 20;
                self.handOben = ko.unwrap(settings.handOben) || 5;
                self.motortyp = ko.unwrap(settings.motortyp) || "ventilator";
                self.fu = ko.unwrap(settings.fu) || "nein";
                self.fuLinks = ko.unwrap(settings.fuLinks) || -20;
                self.fuOben = ko.unwrap(settings.fuOben) || 33;
                self.handLinksFu = ko.unwrap(settings.handLinksFu) || 65;
                self.handObenFu = ko.unwrap(settings.handObenFu) || 5;
                self.einheit = ko.unwrap(settings.einheit) || "%";
                self.fuTyp = ko.unwrap(settings.fuTyp);

                /*
                self.ventilator = ko.computed(function () {
                    if (self.motortyp == 'ventilator') {
                        return 'visible';
                    }
                    else {
                        return 'hidden';
                    }
                }),

                self.pumpe = ko.computed(function () {
                    if (self.motortyp == 'pumpe') {
                        return 'visible';
                    }
                    else {
                        return 'hidden';
                    }
                }),
                */
                self.motor = ko.computed(function () {
                    if (self.motortyp == 'ventilator') {
                        switch (self.objektAusrichtung) {
                            case 'oben':
                                return 'bs bs-ventilator wf-n fa-3x';
                            case 'unten':
                                return 'bs bs-ventilator wf-s fa-3x';
                            case 'links':
                                return 'bs bs-ventilator wf-w fa-3x';
                            case 'rechts':
                                return 'bs bs-ventilator wf-o fa-3x';
                        }
                    } else if (self.motortyp == 'pumpe') {
                        switch (self.objektAusrichtung) {
                            case 'oben':
                                return 'bs bs-pumpe wf-n fa-3x';
                            case 'unten':
                                return 'bs bs-pumpe wf-s fa-3x';
                            case 'links':
                                return 'bs bs-pumpe wf-w fa-3x';
                            case 'rechts':
                                return 'bs bs-pumpe wf-o fa-3x';
                        }
                    }
                    else {
                        return 'bs bs-pumpe';
                    }
                }),

                self.ausrichtung = ko.computed(function () {
                    switch (self.objektAusrichtung) {
                        case 'oben':
                            return 'wf-n';
                        case 'unten':
                            return 'wf-s';
                        case 'links':
                            return 'wf-w';
                        case 'rechts':
                            return 'wf-o';
                    }
                }),

                self.aktiv = ko.computed(function () {
                    if (self.objektZustand() == 4) {
                        return '0.3';
                    }
                    else {
                        return '1';
                    }
                }),

                self.status = ko.computed(function () {
                    self.css = self.motor();
                    switch (self.objektZustand()) {
                        case 0:
                            return self.css + ' bs_initialisieren';
                        case 2:
                            return self.css + ' bs_fehler';
                        case 4:
                            return self.css + ' bs_passiv';
                        case 8:
                            return self.css + ' bs_aus';
                        case 16:
                            return self.css + ' bs_notbetrieb';
                        case 31:
                            return self.css + ' bs_fahrend';
                        case 32:
                            return self.css + ' bs_ein';
                        case 64:
                            return self.css + ' bs_ein';
                        case 128:
                            return self.css + ' bs_ein';
                        default:
                            return self.css + ' bs_aus';
                    }

                }),

                self.zustand = ko.computed(function () {
                    switch (self.objektZustand()) {
                        case 0:
                            return '#ff8500';
                        case 2:
                            return '#ff0000';
                        case 4:
                            return '#6e6e6e';
                        case 8:
                            return '#6e6e6e';
                        case 16:
                            return '#ffff00';
                        case 31:
                            return '#fdfdfd';
                        case 32:
                            return '#00ff00';
                        case 64:
                            return '#00ff00';
                        case 128:
                            return '#00ff00';
                        default:
                            return '#6e6e6e';
                    }

                }),

                self.motorHand = ko.computed(function () {
                    if (self.objektHandbetrieb() != 0)
                        return 'visible';
                    else
                        return 'hidden';
                }),

                self.fuanzeigen = ko.computed(function () {
                    if (self.fu == 'ja')
                        return 'visible';
                    else
                        return 'hidden';
                }),

                self.handFuAnzeigen = ko.computed(function () {
                    if ((self.fuHandbetrieb() == 1) || (self.fuHandbetrieb() == -1))
                        return 'visible';
                    else
                        return 'hidden';
                }),

                self.zustandFu = ko.computed(function () {
                    switch (self.fuZustand()) {
                        case 0:
                            return 'bs_zustand_init';
                        case 2:
                            return 'bs_zustand_fehler';
                        case 4:
                            return 'bs_zustand_aus';
                        case 8:
                            return 'bs_zustand_aus';
                        case 16:
                            return 'bs_zustand_notbetrieb';
                        case 31:
                            return 'bs_zustand_fahrend';
                        case 32:
                            return 'bs_zustand_ein';
                        case 64:
                            return 'bs_zustand_ein';
                        case 128:
                            return 'bs_zustand_ein';
                        default:
                            return 'bs_zustand_default';
                    }
                }),

                self.aktuellerwert = ko.computed(function () {
                    return self.motorleistung() + " " + self.einheit;
                });

                return self.connector.getOnlineUpdates().fail(self.connector.handleError(self));
            },

            openDialog: function () {
                var self = this;
                dialog.show(new motorDialogFU(self)).then(function (dialogResult) {
                    console.log(dialogResult);
                });
            },

            setBetriebswahl: function (value) {
                var self = this;
                var data = {};
                data[self.signalNameBetriebswahl] = value;
                self.connector.writeSignals(data);
            },

            setBetriebswahlSwFu: function (value) {
                var self = this;
                var data = {};
                data[self.signalNameBetriebswahlFu] = value;
                self.connector.writeSignals(data);
            },

            getAlarmunterdr: function () {
                var self = this;
                var betrwahl = self.betriebswahlValue();
                if ((betrwahl & 1073741824) == 1073741824) {
                    return true;
                }
                else {
                    return false;
                }
            },

            changeBit: function (value) {
                var self = this;
                var data = {};
                var mask = 1 << value;
                var betrValue = self.betriebswahlValue();
                betrValue ^= mask;
                betrValue = betrValue >>> 0;
                data[self.signalNameBetriebswahl] = betrValue;
                self.connector.writeSignals(data);
            },

            setAlarmQuit: function (value) {
                var self = this;
                var data = {};
                var mask = 1 << value;
                var betrValue = self.betriebswahlValue();
                betrValue |= mask;
                betrValue = betrValue >>> 0;
                data[self.signalNameBetriebswahl] = betrValue;
                self.connector.writeSignals(data);
            },

            resetZaehlerSt1: function (value) {
                var self = this;
                var data = {};
                data[self.signalNameZaehlerReset1] = value;
                self.connector.writeSignals(data);
            },

            setfestwert: function (value) {
                var self = this;
                var data = {};
                data[self.signalNameFestwert] = value;
                self.connector.writeSignals(data);
            },

            settotzone: function (value) {
                var self = this;
                var data = {};
                data[self.signalNameTotzone] = value;
                self.connector.writeSignals(data);
            },

            setuntergrenze: function (value) {
                var self = this;
                var data = {};
                data[self.signalNameUntergrenze] = value;
                self.connector.writeSignals(data);
            },

            setobergrenze: function (value) {
                var self = this;
                var data = {};
                data[self.signalNameObergrenze] = value;
                self.connector.writeSignals(data);
            },

            detached: function () {
                var self = this;
                return self.connector.unregisterSignals(
                    self.dwZustand,
                    self.handbetrieb,
                    self.alarmmeldungen,
                    self.rAktuellerWert,
                    self.handbetriebFu,
                    self.dwZustandFu,
                    self.betriebswahl,
                    self.betriebswahlFu,
                    self.betrStd1st,
                    self.betrStdTotal,
                    self.einsch1St,
                    self.einschTotal,
                    self.rFestwert,
                    self.rTotzone,
                    self.rUntergrenze,
                    self.rObergrenze
                    );
            }                                                                             
    };                                                                                
    return bsMotor;                                                                   
    });                                                                                   