﻿/*-------------------------------------------------------------------------------------------------------------------*
 *                                              Uhrzeit
 *-------------------------------------------------------------------------------------------------------------------
 * Erstellt am: 23. Mai 2017
 * Firma:       Bühler + Scherler
 * Ersteller:   B. Meier
 * Version:     1.0
 *-------------------------------------------------------------------------------------------------------------------
 * Widgetkonfiguration:
 * signalName: Name des zu verbindenden Signals, Bsp.: 'PLC1.IrgendwasAusGCPro'
 * (signalNameHws: Name des zu verbindenden Signals, Bsp.: 'PLC1.IrgendwasAusGCPro')
 *-------------------------------------------------------------------------------------------------------------------*/
define(
    [
        '../../services/connector',
    ],



    function (signalsConnector) {
        var ctor = function () {
            var self = this;
            self.settings = settings;
        };

       
            ctor.prototype = {
            activate: function (settings) {
                var self = this;
                self.settings = settings;
                self.connector = new signalsConnector();

                self.modulname = ko.unwrap(settings.modulName);

                self.time = self.connector.getSignal(self.modulname);



                return self.connector.getOnlineUpdates().fail(self.connector.handleError(self));
            },


            detached: function () {
                var self = this;
                return self.connector.unregisterSignals(

                    );
            }
        }



        return ctor;
    });