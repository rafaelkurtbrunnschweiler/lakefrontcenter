﻿/*-------------------------------------------------------------------------------------------------------------------*
 *                                              4-Fach Schaltuhr SAIA
 *-------------------------------------------------------------------------------------------------------------------
 * Erstellt am: 20. April 2017
 * Firma:       Bühler + Scherler
 * Ersteller:   B. Meier
 * Version:     1.0
 *-------------------------------------------------------------------------------------------------------------------
 * Widgetkonfiguration:
 * signalName: Name des zu verbindenden Signals, Bsp.: 'PLC1.IrgendwasAusGCPro'
 * dialogTitle: Titel für das zugehörige Dialogfenster, Bsp.: 'Freigabe Lüftung'
 *-------------------------------------------------------------------------------------------------------------------*/
define(
    [
        '../../services/connector',
        'plugins/dialog',
        'src/viewModels/dialogs/widgets/schaltuhrlfcDialog'
    ],

    function (signalsConnector, dialog, schaltuhrlfcDialog) {
        var schaltuhr = function () {
            var self = this;
        };

        schaltuhr.prototype = {
            activate: function (settings) {
                var self = this;
                self.settings = settings;
                self.dialogtitle = ko.unwrap(settings.dialogTitle) || "Dialog Titel";
                self.specialDayName = ko.unwrap(settings.specialDayName) || 'Feiertag';
                self.modulName = ko.unwrap(settings.signalName);
                self.modulNameUhr = ko.unwrap(settings.uhr);

                self.connector = new signalsConnector();

                self.ein_aus_signal = [33];
                self.ein_aus_signal_value = [33];                

                for (let i = 1; i <= 32; i++) {
                    if (i >= 9) {
                        index = "_0" + i;
                        self.ein_aus_signal[i] = self.connector.getSignal(self.modulName + '.Register.' + self.modulNameUhr + index);
                        self.ein_aus_signal_value[i] = self.ein_aus_signal[i].value;
                    }
                    else {
                        index = "_" + i;
                        self.ein_aus_signal[i] = self.connector.getSignal(self.modulName + '.Register.' + self.modulNameUhr + index);
                        self.ein_aus_signal_value[i] = self.ein_aus_signal[i].value;
                    }
                }
                

                if (!self.modulName) {
                    throw new Error('missing widget parameters');
                }

                self.days = [
                    { name: 'Montag', dayIndex: 1 },
                    { name: 'Dienstag', dayIndex: 2 },
                    { name: 'Mittwoch', dayIndex: 3 },
                    { name: 'Donnerstag', dayIndex: 4 },
                    { name: 'Freitag', dayIndex: 5 },
                    { name: 'Samstag', dayIndex: 6 },
                    { name: 'Sonntag', dayIndex: 0 },
                    { name: self.specialDayName, dayIndex: 7 },
                ];
                return self.connector.getOnlineUpdates().fail(self.connector.handleError(self));
            },

            openDialog: function () {
                var self = this;
                self.days.forEach(function (day) {
                    day.times = [];
                    var id = 1;
                    for (var i = 1; i <= 8; i++) {
                            day.times.push({
                                on: { signalName: self.modulName + '_Schaltzeiten[' + day.dayIndex + ',' + i + '].R_iZeit', id: id, color: 'bs-schaltung-aktiv' }
                            });                        
                        id++;
                    }
                });

                dialog.show(new schaltuhrlfcDialog(self)).then(function (dialogResult) {
                    console.log(dialogResult);
                });
            },

            detached: function () {
                var self = this;
                for (let i = 1; i <= 32; i++) {
                    self.connector.unregisterSignals(self.ein_aus_signal[i]);
                }
                return;
            }
        };
        return schaltuhr;
    });