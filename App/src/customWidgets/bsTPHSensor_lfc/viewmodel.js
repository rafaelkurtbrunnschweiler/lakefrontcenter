﻿/*-------------------------------------------------------------------------------------------------------------------*
 *                                TPH - Sensor (Temperatur, Druck u. Feuchte)
 *-------------------------------------------------------------------------------------------------------------------
 * Erstellt am: 20. April 2017
 * Firma:       Bühler + Scherler
 * Ersteller:   B. Meier
 * Version:     1.0
 *-------------------------------------------------------------------------------------------------------------------
 * Widgetkonfiguration:
 * signalName: Name des zu verbindenden Signals, Bsp.: 'PLC1.IrgendwasAusGCPro'
 * dialogtitle: Titel für das zugehörige Dialogfenster, Bsp.: 'ZUL Ventilator 145M1'
 * authorisierung: Name der Berechtigung, Bsp.: 'Administration'
 * trendConfig: Name des zu ladenden Trends, Bsp.: 'myTrend1'
 * (einheit: Die anzuzeigende Einheit, °C - ist default, Bsp.: ' %')
 * (winkel: bestimmt die Drehung -360° bis 360°, default ist 0, Bsp.: '-45')
 * (sensortyp: Der anzuzeigende Typ wie P, T, F, /, ∆P, ∆T, ∆F, ∆T - ist default, Bsp.: 'T')
 * (stiellaenge:  bestimmt die Länge des Fühlerstiels, default ist 20, Bsp.: '20')
 * (anzeigefeldOben: Anzeige Abstand vom oberen Widget Rand, default ist -26, Bsp.: '-26')
 * (anzeigefeldLinks: Anzeige Abstand vom linken Widget Rand, default ist -22, Bsp.: '-22')
 * (handLinks: Hand Abstand vom linken Widget Rand, default ist 12, Bsp.: '12')
 * (handOben:  Hand Abstand vom oberen Widget Rand, default ist 2, Bsp.: '2')
 *-------------------------------------------------------------------------------------------------------------------*/
define(
    [
        '../../services/connector',
        '../../services/securedService',
    ],

    function (signalsConnector, securedService) {
        var tphSensor = function () {
            var self = this;
        };

        $(document).ready(function () {
            tphSensor.prototype = {
                activate: function (settings) {
                    var self = this;
                    self.connector = new signalsConnector();
                    self.stiellaenge = ko.unwrap(settings.stiellaenge) || 20;
                    self.winkel = ko.unwrap(settings.winkel) || 0;
                    self.sensortyp = ko.unwrap(settings.sensortyp) || '∆T';
                    self.modulname = ko.unwrap(settings.signalName);
                    self.einheit = ko.unwrap(settings.einheit) || " °C";
                    self.anzeigefeldLinks = ko.unwrap(settings.anzeigefeldLinks) || -22;
                    self.anzeigefeldOben = ko.unwrap(settings.anzeigefeldOben) || -26;
                    self.handLinks = ko.unwrap(settings.handLinks) || 12;
                    self.handOben = ko.unwrap(settings.handOben) || 2;


                    self.dwZustand = self.connector.getSignal(self.modulname);



                    self.unit = self.einheit;



                    self.zustand        = self.dwZustand.value;





                    self.sensorzustand = ko.computed(function () {
                        switch (self.zustand()) {
                            case 0:
                                return 'bs_zustand_aus';
                            case 2:
                                return 'bs_zustand_aus';
                            case 4:
                                return 'bs_zustand_aus';
                            case 8:
                                return 'bs_zustand_aus';
                            case 16:
                                return 'bs_zustand_aus';
                            case 31:
                                return 'bs_zustand_aus';
                            case 32:
                                return 'bs_zustand_aus';
                            case 64:
                                return 'bs_zustand_aus';
                            case 128:
                                return 'bs_zustand_aus';
                            default:
                                return 'bs_zustand_aus';
                        }
                    })
                    return self.connector.getOnlineUpdates().fail(self.connector.handleError(self));
                },


                detached: function () {
                    var self = this;
                    return self.connector.unregisterSignals(
                        self.dwZustand
                        );
                }
            };
        });
        return tphSensor;
    });
