﻿/*-------------------------------------------------------------------------------------------------------------------*
 *                                        Motor mehrstufig (max. 3 Stufen)
 *-------------------------------------------------------------------------------------------------------------------
 * Erstellt am: 20. April 2017
 * Firma:       Bühler + Scherler
 * Ersteller:   B. Meier
 * Version:     1.0
 *-------------------------------------------------------------------------------------------------------------------
 * Widgetkonfiguration:
 * signalName: Name des zu verbindenden Signals, Bsp.: 'PLC1.IrgendwasAusGCPro'
 * dialogTitle: Titel für das zugehörige Dialogfenster, Bsp.: 'ZUL Ventilator 145M1'
 * authorisierung: Name der Berechtigung, Bsp.: 'Administration'
 * (motorAusrichtung: dreht das Widget in die gewünschte Richtung, default ist oben, Bsp.: 'oben', 'rechts', 'unten', 'links')
 * (handLinks: Hand Abstand vom linken Widget Rand, default ist 20, Bsp.: '20')
 * (handOben:  Hand Abstand vom oberen Widget Rand, default ist 5, Bsp.: '5')
 * (motortyp: Typ des anzuzeigenden Symbols, default ist pumpe, Bsp.: 'ventilator', 'pumpe')
 * (stufe: Für mehrstufige Geräte von 0 bis 3, default ist 0, Bsp.: '0')
 *-------------------------------------------------------------------------------------------------------------------*/
define(
    [
        '../../services/connector',
        '../../services/securedService',
        'plugins/dialog',
        'src/viewModels/dialogs/widgets/motorDialogStufen'
    ],

    function (signalsConnector, securedService, dialog, motorDialogStufen) {
        var bsMotor = function () {
            var self = this;
        };

        bsMotor.prototype = {
            activate: function (settings) {
                var self = this;

                self.projectAuthorization = ko.unwrap(settings.projectAuthorization) || "";
                self.securedService = new securedService(self.projectAuthorization);
                self.hasAuthorization = self.securedService.hasAuthorization;

                self.settings = settings;
                self.modulname = ko.unwrap(settings.signalName);
                self.signalNameBetriebswahl = ko.unwrap(settings.signalName) + ".R_dwBetriebswahl";

                self.signalNameZaehlerReset1 = ko.unwrap(settings.signalName) + "_Zaehler.R_xReset";
                self.signalNameZaehlerReset2 = ko.unwrap(settings.signalName) + "_ZaehlerSt2.R_xReset";
                self.signalNameZaehlerReset3 = ko.unwrap(settings.signalName) + "_ZaehlerSt3.R_xReset";

                self.connector = new signalsConnector();
                self.dwZustand = self.connector.getSignal(ko.unwrap(settings.signalName) + ".W_dwZustand");
                self.handbetrieb = self.connector.getSignal(ko.unwrap(settings.signalName) + ".W_xHanduebersteuert");
                self.alarmmeldungen = self.connector.getSignal(ko.unwrap(settings.signalName) + ".W_wError");
                self.betriebswahl = self.connector.getSignal(ko.unwrap(settings.signalName) + ".R_dwBetriebswahl");
                self.einsch1St = self.connector.getSignal(ko.unwrap(settings.signalName) + "_Zaehler.W_uiSchaltungen");
                self.einsch2St = self.connector.getSignal(ko.unwrap(settings.signalName) + "_ZaehlerSt2.W_uiSchaltungen");
                self.einsch3St = self.connector.getSignal(ko.unwrap(settings.signalName) + "_ZaehlerSt3.W_uiSchaltungen");
                self.einschTotal = self.connector.getSignal(ko.unwrap(settings.signalName) + "_Zaehler.W_uiSchaltungenTotal");
                self.betrStd1st = self.connector.getSignal(ko.unwrap(settings.signalName) + "_Zaehler.W_rBetriebsstunden");
                self.betrStd2st = self.connector.getSignal(ko.unwrap(settings.signalName) + "_ZaehlerSt2.W_rBetriebsstunden");
                self.betrStd3st = self.connector.getSignal(ko.unwrap(settings.signalName) + "_ZaehlerSt3.W_rBetriebsstunden");
                self.betrStdTotal = self.connector.getSignal(ko.unwrap(settings.signalName) + "_Zaehler.W_rBetriebsstundenTotal");
                self.autorisierung = ko.unwrap(settings.authorisierung);

                self.objektZustand =        self.dwZustand.value;
                self.objektHandbetrieb =    self.handbetrieb.value;
                self.alarme =               self.alarmmeldungen.value;
                self.betriebswahlValue =    self.betriebswahl.value;
                self.betrStd1stValue =      self.betrStd1st.value;
                self.betrStd2stValue =      self.betrStd2st.value;
                self.betrStd3stValue =      self.betrStd3st.value;
                self.betrStdTotalValue =    self.betrStdTotal.value;
                self.einsch1StValue =       self.einsch1St.value;
                self.einsch2StValue =       self.einsch2St.value;
                self.einsch3StValue =       self.einsch3St.value;
                self.einschTotalValue =     self.einschTotal.value;

                self.dialogtitle = ko.unwrap(settings.dialogTitle) || "Dialog Titel";
                self.motortyp = ko.unwrap(settings.motortyp) || "pumpe";
                self.objektAusrichtung = ko.unwrap(settings.motorAusrichtung) || "oben";
                self.mehrstufig = ko.unwrap(settings.stufe) || "0";
                self.links = ko.unwrap(settings.handLinks) || -10;
                self.oben = ko.unwrap(settings.handOben) || 10;

                self.stufen = ko.computed(function () {
                    if (self.mehrstufig != 0) {
                        return 'visible';
                    }
                    else {
                        return 'hidden';
                    }
                }),

                self.pumpe = ko.computed(function () {
                    if (self.motortyp == 'pumpe') {
                        return 'visible';
                    }
                    else {
                        return 'hidden';
                    }
                }),

                self.ventilator = ko.computed(function () {
                    if (self.motortyp == 'ventilator') {
                        return 'visible';
                    }
                    else {
                        return 'hidden';
                    }
                }),

                self.ausrichtung = ko.computed(function () {
                    switch (self.objektAusrichtung) {
                        case 'oben':
                            return 'wf-n';
                        case 'unten':
                            return 'wf-s';
                        case 'links':
                            return 'wf-w';
                        case 'rechts':
                            return 'wf-o';
                    }
                }),

                self.aktiv = ko.computed(function () {
                    if (self.objektZustand() == 4) {
                        return '0.3';
                    }
                    else {
                        return '1';
                    }
                }),

                self.zustand = ko.computed(function () {
                    switch (self.objektZustand()) {
                        case 0:
                            return '#ff8500';
                        case 2:
                            return '#ff0000';
                        case 4:
                            return '#d6d6d6';
                        case 8:
                            return '#d6d6d6';
                        case 16:
                            return '#ffff00';
                        case 31:
                            return '#fdfdfd';
                        case 32:
                            return '#00ff00';
                        case 64:
                            return '#00ff00';
                        case 128:
                            return '#00ff00';
                        default:
                            return '#d6d6d6';
                    }

                }),

                self.betriebsstufe = ko.computed(function () {
                    switch (self.objektZustand()) {
                        case 0:
                            return '0';
                        case 2:
                            return 'E';
                        case 4:
                            return '';
                        case 8:
                            return '0';
                        case 16:
                            return 'N';
                        case 32:
                            return '1';
                        case 64:
                            return '2';
                        case 128:
                            return '3';
                        default:
                            return '?';
                    }
                }),

                self.motorHand = ko.computed(function () {
                    if (self.objektHandbetrieb() != 0)
                        return 'visible';
                    else
                        return 'hidden';
                });

                return self.connector.getOnlineUpdates().fail(self.connector.handleError(self));
            },

            openDialog: function () {
                var self = this;
                dialog.show(new motorDialogStufen(self)).then(function (dialogResult) {
                    console.log(dialogResult);
                });
            },

            setBetriebswahl: function (value) {
                var self = this;
                var data = {};
                data[self.signalNameBetriebswahl] = value;
                self.connector.writeSignals(data);
            },

            getAlarmunterdr: function () {
                var self = this;
                var betrwahl = self.betriebswahlValue();
                if ((betrwahl & 1073741824) == 1073741824) {
                    return true;
                }
                else {
                    return false;
                }
            },

            changeBit: function (value) {
                var self = this;
                var data = {};
                var mask = 1 << value;
                var betrValue = self.betriebswahlValue();
                betrValue ^= mask;
                betrValue = betrValue >>> 0;
                data[self.signalNameBetriebswahl] = betrValue;
                self.connector.writeSignals(data);
            },

            setAlarmQuit: function (value) {
                var self = this;
                var data = {};
                var mask = 1 << value;
                var betrValue = self.betriebswahlValue();
                betrValue |= mask;
                betrValue = betrValue >>> 0;
                data[self.signalNameBetriebswahl] = betrValue;
                self.connector.writeSignals(data);
            },

            resetZaehlerSt1: function (value) {
                var self = this;
                var data = {};
                data[self.signalNameZaehlerReset1] = value;
                self.connector.writeSignals(data);
            },
            resetZaehlerSt2: function (value) {
                var self = this;
                var data = {};
                data[self.signalNameZaehlerReset2] = value;
                self.connector.writeSignals(data);
            },
            resetZaehlerSt3: function (value) {
                var self = this;
                var data = {};
                data[self.signalNameZaehlerReset3] = value;
                self.connector.writeSignals(data);
            },

            detached: function () {
                var self = this;
                return self.connector.unregisterSignals(
                    self.signal,
                    self.dwZustand,
                    self.handbetrieb,
                    self.alarmmeldungen,
                    self.betriebswahl,
                    self.betrStd1st,
                    self.betrStd2st,
                    self.betrStd3st,
                    self.betrStdTotal,
                    self.einsch1St,
                    self.einsch2St,
                    self.einsch3St,
                    self.einschTotal
                    );
            }
        };
        return bsMotor;
    });