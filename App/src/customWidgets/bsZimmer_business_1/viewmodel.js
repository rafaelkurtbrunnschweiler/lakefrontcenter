﻿/*-------------------------------------------------------------------------------------------------------------------*
*                                              Zimmer
*-------------------------------------------------------------------------------------------------------------------
* Erstellt am: 12. Januar 2018
* Firma:       Bühler + Scherler
* Ersteller:   B. Meier / R. Brunnschweiler
* Version:     1.0
*-------------------------------------------------------------------------------------------------------------------
* Widgetkonfiguration:
* Button mit Modal-Dialog
* Im Dialog wird das aktuelle Zimmer angezeigt, bzw. festgelegt. 
 * 
 * Attribute
* dialogTitel: Titeltext des Modal-Dialoges
* modulName: Name des zu verbindenden Signals, Bsp.: 'PLC1.IrgendwasAusGCPro'
*
* Beispielkonfiguration
<div class="bs-heizbetrieb" data-bind="bsHeizbetrieb: {
    dialogTitle: 'Heizbetrieb',
       authorisierung: 'Administration',
       modulName: 'PLC1.plcObject',
       modulNameAT: 'PLC1.plcObjectAussentemperatur'
       }">
</div>
*-------------------------------------------------------------------------------------------------------------------*/
define(
       [
             '../../services/connector',
             '../../services/securedService',
             'plugins/dialog',
             'src/viewModels/dialogs/widgets/zimmerbusiness_1_Dialog'
       ],

       function (signalsConnector, securedService, dialog, zimmerbusiness_1_Dialog) {
           var zimmer = function () {
               var self = this;
           };

           zimmer.prototype = {
               activate: function (settings) {
                   var self = this;
                   self.settings = settings;
                   self.connector = new signalsConnector();

                   self.projectAuthorization = ko.unwrap(settings.authorisierung) || "none";
                   self.securedService = new securedService(self.projectAuthorization);
                   self.hasAuthorization = self.securedService.hasAuthorization;
                   // --------------------------------------------------------------------
                   // HTML Paremeters

                   //self.trendConfig = ko.unwrap(settings.trendConfig);
                   self.dialogTitle = ko.unwrap(settings.dialogTitle) || "Dialog Titel";
                   //self.sollwertText = ko.unwrap(settings.sollwertText) || "Aussentemperatur:";
                   //self.zeitraumMittelwertBildung = ko.unwrap(settings.zeitraumMittelwertBildung) || "Aussentemperatur 24h Mittelwert:";
                   self.einheit = ko.unwrap(settings.einheit) || '°C';
                   self.Kelvin = ko.unwrap(settings.Kelvin) || 'K';
                   self.prozent = ko.unwrap(settings.prozent) || '%';

                   // PLC Variable Name
                   self.mastername = ko.unwrap(settings.masterName);
                   self.roomcontrolname = ko.unwrap(settings.roomcontrolName);
                   self.roomtemp = ko.unwrap(settings.masterName) + ".Register." + ko.unwrap(settings.roomcontrolName) + "_x_raumtemp";
                   self.basissollwert = ko.unwrap(settings.masterName) + ".Register." + ko.unwrap(settings.roomcontrolName) + "_sw_w_temp";
                   self.raumsollwert = ko.unwrap(settings.masterName) + ".Register." + ko.unwrap(settings.roomcontrolName) + "_sw_raumtemp";
                   self.raumsollwertkorrektur = ko.unwrap(settings.masterName) + ".Register." + ko.unwrap(settings.roomcontrolName) + "_cor_sw_temp";
                   self.heizventilstate = ko.unwrap(settings.masterName) + ".Register." + ko.unwrap(settings.roomcontrolName) + "_p1_status_hv_111y5";
                   self.kuehlventilstate = ko.unwrap(settings.masterName) + ".Register." + ko.unwrap(settings.roomcontrolName) + "_p2_status_kv_111y7";
                   self.roomvorwahlbereit = ko.unwrap(settings.masterName) + ".Flag." + ko.unwrap(settings.roomcontrolName) + "_en_use";
                   self.roomstate = ko.unwrap(settings.masterName) + ".Flag." + ko.unwrap(settings.roomcontrolName) + "_rm_use";
                   self.roomcontrolcommunication = ko.unwrap(settings.masterName) + ".Flag." + ko.unwrap(settings.roomcontrolName) + "_al_111u1";


                   // PLC Signals
                   self.plcroomtemp = self.connector.getSignal(self.roomtemp);
                   self.plcbasissollwert = self.connector.getSignal(self.basissollwert);
                   self.plcraumsollwert = self.connector.getSignal(self.raumsollwert);
                   self.plcraumsollwertkorrektur = self.connector.getSignal(self.raumsollwertkorrektur);
                   self.plcheizventilstate = self.connector.getSignal(self.heizventilstate);
                   self.plckuehlventilstate = self.connector.getSignal(self.kuehlventilstate);
                   self.plcroomvorwahlbereit = self.connector.getSignal(self.roomvorwahlbereit);
                   self.plcroomstate = self.connector.getSignal(self.roomstate);
                   self.plcroomcontrolcommunication = self.connector.getSignal(self.roomcontrolcommunication);


                   // PLC Values
                   self.plcroomtempValue = self.plcroomtemp.value;
                   self.plcbasissollwertValue = self.plcbasissollwert.value;
                   self.plcraumsollwertValue = self.plcraumsollwert.value;
                   self.plcraumsollwertkorrekturValue = self.plcraumsollwertkorrektur.value;
                   self.plcheizventilstateValue = self.plcheizventilstate.value;
                   self.plckuehlventilstateValue = self.plckuehlventilstate.value;
                   self.plcroomvorwahlbereitValue = self.plcroomvorwahlbereit.value;
                   self.plcroomstateValue = self.plcroomstate.value;
                   self.plcroomcontrolcommunicationValue = self.plcroomcontrolcommunication.value;


                   // Object Configuration
               },

               openDialog: function () {
                   var self = this;
                   dialog.show(new zimmerbusiness_1_Dialog(self)).then(function (dialogResult) {
                       console.log(dialogResult);
                   });
               },

               detached: function () {
                   var self = this;
                   return self.connector.unregisterSignals(
                        self.plcroomtemp,
                        self.plcbasissollwert,
                        self.plcraumsollwert,
                        self.plcraumsollwertkorrektur,
                        self.plcheizventilstate,
                        self.plckuehlventilstate,
                        self.plcroomvorwahlbereit,
                        self.plcroomstate,
                        self.plcroomcontrolcommunication
               )
               }

           };
           return zimmer;
       });