﻿/*
    Bühler+Scherler

    Anzeige Status
    -----------------------

    Zeigt ein Rechteck an, welches je nach Status die Farbe und den Text ändert.
    Mögliche Status sind 0=Aus, 1=Automatik, 2=Ein


    Eigenschaft             Typ     Default         Beschreibung
    --------------------------------------------------------------------------

    signalName              String                              Variable für den Status

*/
define(
    [
        '../../services/connector',
        '../../services/securedService'
    ],
    function (
        signalsConnector,
        securedService
    ) {

        // constructor
        var AnzeigeStatus = function () {

        };

        AnzeigeStatus.prototype.activate = function (settings) {

            var self = this;

            self.connector = new signalsConnector();

            self.projectAuthorization = ko.unwrap(settings.projectAuthorization) || '';
            self.securedService = new securedService(self.projectAuthorization);
            self.hasAuthorization = self.securedService.hasAuthorization;

            self.settings = settings;
            self.signalName = ko.unwrap(settings.signalName) || null;

            // Stop here and return if no signalName was configured
            if (!self.signalName ) {
                throw new Error('missing widget parameters');
            }

            self.signal = self.connector.getSignal(ko.unwrap(self.signalName));
            self.signalValue = self.signal.value;

            self.stateTexts = [
                'Server beendet',
                'Server startet',
                'Server läuft'
            ];

            self.stateClasses = [
               'beendet',
               'gestartet',
               'laeuft'
            ];

            self.stateText = ko.computed(function () {

                var state = self.signalValue();

                if (state === undefined || state === null) {
                    return 'WARTE';
                }

                if (state === 10) {
                    return 'STOPPED';
                }
                if (state === 12) {
                    return 'STARTING';
                }
                if (state === 13) {
                    return 'RUNNING';
                }
            });

            self.stateClass = ko.computed(function () {

                var state = self.signalValue();

                if (state === undefined) {
                    return 'beendet';
                }

                if (state === 10) {
                    return 'beendet';
                }
                if (state === 12) {
                    return 'gestartet';
                }
                if (state === 13) {
                    return 'laeuft';
                }
            });

            return self.connector.getOnlineUpdates().fail(self.connector.handleError(self));
        };

        AnzeigeStatus.prototype.detached = function () {

            var self = this;

            if (self.signal) {
                self.connector.unregisterSignals(self.signal);
            }
        };

        return AnzeigeStatus;
    });