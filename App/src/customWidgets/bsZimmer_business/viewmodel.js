﻿/*-------------------------------------------------------------------------------------------------------------------*
*                                              Zimmer
*-------------------------------------------------------------------------------------------------------------------
* Erstellt am: 12. Januar 2018
* Firma:       Bühler + Scherler
* Ersteller:   B. Meier / R. Brunnschweiler
* Version:     1.0
*-------------------------------------------------------------------------------------------------------------------
* Widgetkonfiguration:
* Button mit Modal-Dialog
* Im Dialog wird das aktuelle Zimmer angezeigt, bzw. festgelegt. 
 * 
 * Attribute
* dialogTitel: Titeltext des Modal-Dialoges
* modulName: Name des zu verbindenden Signals, Bsp.: 'PLC1.IrgendwasAusGCPro'
*
* Beispielkonfiguration
<div class="bs-heizbetrieb" data-bind="bsHeizbetrieb: {
    dialogTitle: 'Heizbetrieb',
       authorisierung: 'Administration',
       modulName: 'PLC1.plcObject',
       modulNameAT: 'PLC1.plcObjectAussentemperatur'
       }">
</div>
*-------------------------------------------------------------------------------------------------------------------*/
define(
       [
             '../../services/connector',
             '../../services/securedService',
             'plugins/dialog',
             'src/viewModels/dialogs/widgets/zimmerbusinessDialog'
       ],

       function (signalsConnector, securedService, dialog, zimmerbusinessDialog) {
           var zimmer = function () {
               var self = this;
           };

           zimmer.prototype = {
               activate: function (settings) {
                   var self = this;
                   self.settings = settings;
                   self.connector = new signalsConnector();

                   self.projectAuthorization = ko.unwrap(settings.authorisierung) || "none";
                   self.securedService = new securedService(self.projectAuthorization);
                   self.hasAuthorization = self.securedService.hasAuthorization;
                   // --------------------------------------------------------------------
                   // HTML Paremeters

                   //self.trendConfig = ko.unwrap(settings.trendConfig);
                   self.dialogTitle = ko.unwrap(settings.dialogTitle) || "Dialog Titel";
                   //self.sollwertText = ko.unwrap(settings.sollwertText) || "Aussentemperatur:";
                   //self.zeitraumMittelwertBildung = ko.unwrap(settings.zeitraumMittelwertBildung) || "Aussentemperatur 24h Mittelwert:";
                   self.einheit = ko.unwrap(settings.einheit) || '°C';
                   self.Kelvin = ko.unwrap(settings.Kelvin) || 'K';
                   self.prozent = ko.unwrap(settings.prozent) || '%';

                   // PLC Variable Name
                   self.mastername = ko.unwrap(settings.masterName);
                   self.roomcontrolname = ko.unwrap(settings.roomcontrolName);
                   self.roomnumber = ko.unwrap(settings.roomNumber);
                   self.roomtemp = ko.unwrap(settings.roomcontrolName) + ".Register.Z" + ko.unwrap(settings.roomNumber) + ".RTemp.Istwert";
                   self.basissollwert = ko.unwrap(settings.roomcontrolName) + ".Register.Z" + ko.unwrap(settings.roomNumber) + ".RTemp.SW.rcv_Raum";
                   self.raumsollwert = ko.unwrap(settings.roomcontrolName) + ".Register.Z" + ko.unwrap(settings.roomNumber) + ".RTemp.SW.Aktuell";
                   self.raumsollwertkorrektur = ko.unwrap(settings.roomcontrolName) + ".Register.Z" + ko.unwrap(settings.roomNumber) + ".Raumkorrektur";
                   self.heizventilstate = ko.unwrap(settings.roomcontrolName) + ".Flag.Z" + ko.unwrap(settings.roomNumber) + ".Frg_Heizventil";
                   self.kuehlventilstate = ko.unwrap(settings.roomcontrolName) + ".Flag.Z" + ko.unwrap(settings.roomNumber) + ".Frg_Kuehlventil";
                   self.heizventillocked = ko.unwrap(settings.roomcontrolName) + ".Flag.Z" + ko.unwrap(settings.roomNumber) + ".Sperrung.rcv_Heizventil";
                   self.kuehlventillocked = ko.unwrap(settings.roomcontrolName) + ".Flag.Z" + ko.unwrap(settings.roomNumber) + ".Sperrung.rcv_Kuehlventil";
                   self.roomvorwahlbereit = ko.unwrap(settings.masterName) + ".Flag.Zimmer" + ko.unwrap(settings.roomNumber) + ".Set_Bereit";
                   self.roomvorwahleco = ko.unwrap(settings.masterName) + ".Flag.Zimmer" + ko.unwrap(settings.roomNumber) + ".Set_Eco";
                   self.roomstate = ko.unwrap(settings.masterName) + ".Register.Zimmer" + ko.unwrap(settings.roomNumber) + ".Status_Zimmer";
                   self.roomcard = ko.unwrap(settings.roomcontrolName) + ".Flag.Z" + ko.unwrap(settings.roomNumber) + ".Karte";
                   self.roomwindow = ko.unwrap(settings.roomcontrolName) + ".Flag.Z" + ko.unwrap(settings.roomNumber) + ".Fenster_Zu";
                   self.ventistate = ko.unwrap(settings.roomcontrolName) + ".Register.Z" + ko.unwrap(settings.roomNumber) + ".Venti.Status";
                   self.roomcontrolcommunication = ko.unwrap(settings.masterName) + ".Flag.Zimmer" + ko.unwrap(settings.roomNumber) + ".Komm_rm_Alarm";
                   self.roomcontrolstate = ko.unwrap(settings.roomcontrolName) + ".Register.Z" + ko.unwrap(settings.roomNumber) + ".Status_Bedienung";
                   self.sommerwinter = ko.unwrap(settings.masterName) + ".Flag.Allg.Sommerbetrieb";


                   // PLC Signals
                   self.plcroomtemp = self.connector.getSignal(self.roomtemp);
                   self.plcbasissollwert = self.connector.getSignal(self.basissollwert);
                   self.plcraumsollwert = self.connector.getSignal(self.raumsollwert);
                   self.plcraumsollwertkorrektur = self.connector.getSignal(self.raumsollwertkorrektur);
                   self.plcheizventilstate = self.connector.getSignal(self.heizventilstate);
                   self.plckuehlventilstate = self.connector.getSignal(self.kuehlventilstate);
                   self.plcheizventillocked = self.connector.getSignal(self.heizventillocked);
                   self.plckuehlventillocked = self.connector.getSignal(self.kuehlventillocked);
                   self.plcroomvorwahlbereit = self.connector.getSignal(self.roomvorwahlbereit);
                   self.plcroomvorwahleco = self.connector.getSignal(self.roomvorwahleco);
                   self.plcroomstate = self.connector.getSignal(self.roomstate);
                   self.plcroomcard = self.connector.getSignal(self.roomcard);
                   self.plcroomwindow = self.connector.getSignal(self.roomwindow);
                   self.plcventistate = self.connector.getSignal(self.ventistate);
                   self.plcroomcontrolcommunication = self.connector.getSignal(self.roomcontrolcommunication);
                   self.plcroomcontrolstate = self.connector.getSignal(self.roomcontrolstate);
                   self.plcsommerwinter = self.connector.getSignal(self.sommerwinter);


                   // PLC Values
                   self.plcroomtempValue = self.plcroomtemp.value;
                   self.plcbasissollwertValue = self.plcbasissollwert.value;
                   self.plcraumsollwertValue = self.plcraumsollwert.value;
                   self.plcraumsollwertkorrekturValue = self.plcraumsollwertkorrektur.value;
                   self.plcheizventilstateValue = self.plcheizventilstate.value;
                   self.plckuehlventilstateValue = self.plckuehlventilstate.value;
                   self.plcheizventillockedValue = self.plcheizventillocked.value;
                   self.plckuehlventillockedValue = self.plckuehlventillocked.value;
                   self.plcroomvorwahlbereitValue = self.plcroomvorwahlbereit.value;
                   self.plcroomvorwahlecoValue = self.plcroomvorwahleco.value;
                   self.plcroomstateValue = self.plcroomstate.value;
                   self.plcroomcardValue = self.plcroomcard.value;
                   self.plcroomwindowValue = self.plcroomwindow.value;
                   self.plcventistateValue = self.plcventistate.value;
                   self.plcroomcontrolcommunicationValue = self.plcroomcontrolcommunication.value;
                   self.plcroomcontrolstateValue = self.plcroomcontrolstate.value;
                   self.plcsommerwinterValue = self.plcsommerwinter.value;


                   // Object Configuration

                   self.sperre = ko.computed(function() {
                    if (self.plcroomwindowValue() == 1) {
                      return "wf wf-unlock";
                    } else {
                      return "wf wf-lock";
                    }
                  });
               },

               openDialog: function () {
                   var self = this;
                   dialog.show(new zimmerbusinessDialog(self)).then(function (dialogResult) {
                       console.log(dialogResult);
                   });
               },

               detached: function () {
                   var self = this;
                   return self.connector.unregisterSignals(
                        self.plcroomtemp,
                        self.plcbasissollwert,
                        self.plcraumsollwert,
                        self.plcraumsollwertkorrektur,
                        self.plcheizventilstate,
                        self.plckuehlventilstate,
                        self.plcheizventillocked,
                        self.plckuehlventillocked,
                        self.plcroomvorwahlbereit,
                        self.plcroomvorwahleco,
                        self.plcroomstate,
                        self.plcroomcard,
                        self.plcroomwindow,
                        self.plcventistate,
                        self.plcroomcontrolcommunication,
                        self.plcroomcontrolstate,
                        self.plcsommerwinter
               );
               }

           };
           return zimmer;
       });