﻿/*-------------------------------------------------------------------------------------------------------------------*
 *                                            Klappe, Ventil, BSK
 *-------------------------------------------------------------------------------------------------------------------
 * Erstellt am: 23. Mai 2017
 * Firma:       Bühler + Scherler
 * Ersteller:   B. Meier
 * Version:     1.0
 *-------------------------------------------------------------------------------------------------------------------
 * Widgetkonfiguration:
 * signalName: Name des zu verbindenden Signals, Bsp.: 'PLC1.IrgendwasAusGCPro'
 * symbol: 'bs bs-vav', 'bs bs-klappe-fahrend', 'bs bs-bsk-fahrend'
 * dialogtitle: Titel für das zugehörige Dialogfenster, Bsp.: 'ZUL Ventilator 145M1'
 * trendConfig: Name der zu ladenden Trendkonfiguration, Bsp.: 'myTrend2'
 * authorisierung: Name der Berechtigung, Bsp.: 'Administration'
 * (handLinks: Hand Abstand vom linken Widget Rand, default ist -10, Bsp.: '-10')
 * (handOben:  Hand Abstand vom oberen Widget Rand, default ist 10, Bsp.: '10')
 * (anzeigefeldOben: Anzeige Abstand vom oberen Widget Rand, default ist 10, Bsp.: '10')
 * (anzeigefeldLinks: Anzeige Abstand vom linken Widget Rand, default ist -62, Bsp.: '-62')
 * (winkel: bestimmt die Drehung -360° bis 360°, default ist 0, Bsp.: '-45'
 * (einheit: Die anzuzeigende Einheit, % - ist default, Bsp.: '%')
 * (typ: 'stetig' oder 'digital', bei digital wird das Ausgabefeld nicht angezeigt, stetig ist default, Bsp.: 'stetig')
 *-------------------------------------------------------------------------------------------------------------------*/
define(
    [
        '../../services/connector',
        '../../services/securedService',
    ],

    function (signalsConnector, securedService) {
        var bsKlappe = function () {
            var self = this;
        };
        $(document).ready(function () {
            bsKlappe.prototype = {
                activate: function (settings) {
                    var self = this;
                    self.connector = new signalsConnector();
                    // unwrapper
                    self.glyph = ko.unwrap(settings.symbol);
                    self.modulname = ko.unwrap(settings.signalName);
                    self.klappenposition = ko.unwrap(settings.position);
                    self.handLinks = ko.unwrap(settings.handLinks) || -10;
                    self.handOben = ko.unwrap(settings.handOben) || 10;
                    self.anzeigefeldLinks = ko.unwrap(settings.anzeigefeldLinks) || -62;
                    self.anzeigefeldOben = ko.unwrap(settings.anzeigefeldOben) || 10;
                    self.winkel = ko.unwrap(settings.winkel) || 0;
                    self.unit = ko.unwrap(settings.einheit) || '%';
                    self.autorisierung = ko.unwrap(settings.authorisierung);
                    self.trendConfig = ko.unwrap(settings.trendConfig);
                    self.typ = ko.unwrap(settings.typ) || 'digital';

                    self.signalname = self.klappenposition;

                    self.dwZustand = self.connector.getSignal(self.modulname);

                    self.zustand = self.dwZustand.value;

                    self.symbol = ko.computed(function () {
                        return self.glyph + ' fa-3x';
                    });

                    self.status = ko.computed(function () {
                        self.css = self.symbol();
                        if (self.glyph.includes("klappe")) {
                            switch (self.zustand()) {
                                case 0:
                                    return self.css + ' bs_fahrend';
                                case 1:
                                    return self.css + ' bs_notbetrieb';
                                case 2:
                                    return self.css + ' bs_fehler';
                                case 3:
                                    return self.css + ' bs_aus bs-klappe-zu';
                                case 4:
                                    return self.css + ' bs_ein bs-bsk-offen';
                                default:
                                    return self.css + ' bs_aus';
                            }
                        } else {
                            switch (self.zustand()) {
                                case 0:
                                    return self.css + ' bs_fahrend';
                                case 1:
                                    return self.css + ' bs_notbetrieb';
                                case 2:
                                    return self.css + ' bs_fehler';
                                case 3:
                                    return self.css + ' bs_aus bs-klappe-zu';
                                case 4:
                                    return self.css + ' bs_ein bs-bsk-offen';
                                default:
                                    return self.css + ' bs_aus';
                            }
                        }
                    });


                    self.digital = ko.computed(function () {
                        if (self.typ === 'digital')
                            return false;
                        return true;
                    });
                    return self.connector.getOnlineUpdates().fail(self.connector.handleError(self));
                },

                detached: function () {
                    var self = this;
                    return self.connector.unregisterSignals(
                        self.dwZustand,
                        self.xhandbetrieb,
                        self.alarmmeldungen,
                        self.dwBetriebswahl
                        );
                }
            }

        })
        return bsKlappe;
    });