﻿/*-------------------------------------------------------------------------------------------------------------------*
 *                                Ventil 2-Weg, 3-Weg
 *-------------------------------------------------------------------------------------------------------------------
 * Erstellt am: 20. April 2017
 * Firma:       Bühler + Scherler
 * Ersteller:   B. Meier
 * Version:     1.0
 *-------------------------------------------------------------------------------------------------------------------
 * Widgetkonfiguration:
 * signalName: Name des zu verbindenden Signals, Bsp.: 'PLC1.IrgendwasAusGCPro'
 * dialogtitle: Titel für das zugehörige Dialogfenster, Bsp.: 'ZUL Ventilator 145M1'
 * authorisierung: Name der Berechtigung, Bsp.: 'Administration'
 * trendConfig: Name des zu ladenden Trends, Bsp.: 'myTrend1'
 * (symbol: Das anzuzeigende Symbol, default ist: bs bs-ventil fa-3x (B+S Symbol für ein 2-Weg Ventil, 3-fach vergrössert)
 *      für das 3-Weg Ventil gilt folgendes Symbol: bs bs-ventil3, Bsp.: 'bs bs-ventil3'
 * (einheit: Die anzuzeigende Einheit, °C - ist default, Bsp.: ' %')
 * (winkel: bestimmt die Drehung -360° bis 360°, default ist 0, Bsp.: '-45')
 * (anzeigefeldOben: Anzeige Abstand vom oberen Widget Rand, default ist 10, Bsp.: '10')
 * (anzeigefeldLinks: Anzeige Abstand vom linken Widget Rand, default ist -62, Bsp.: '-62')
 * (handLinks: Hand Abstand vom linken Widget Rand, default ist -10, Bsp.: '-10')
 * (handOben:  Hand Abstand vom oberen Widget Rand, default ist 10, Bsp.: '10')
 *-------------------------------------------------------------------------------------------------------------------*/
define(
    [
        '../../services/connector',
        'plugins/dialog',
        'src/viewModels/dialogs/widgets/ventilDialog'
    ],

    function (signalsConnector, dialog, ventilDialog) {
        var ventil = function () {
            var self = this;

        };
        $(document).ready(function () {
            ventil.prototype = {
                activate: function (settings) {
                    var self = this;
                    self.connector = new signalsConnector();
                    // unwrapper
                    self.glyph = ko.unwrap(settings.symbol) || "bs bs-ventil fa-3x";
                    self.dialogtitle = ko.unwrap(settings.dialogtitle) || "Ventil Dialog";
                    self.modulname = ko.unwrap(settings.signalName);
                    self.handLinks = ko.unwrap(settings.handLinks) || -10;
                    self.handOben = ko.unwrap(settings.handOben) || 10;
                    self.anzeigefeldLinks = ko.unwrap(settings.anzeigefeldLinks) || -62;
                    self.anzeigefeldOben = ko.unwrap(settings.anzeigefeldOben) || 10;
                    self.winkel = ko.unwrap(settings.winkel) || 0;
                    self.unit = ko.unwrap(settings.einheit) || '%';
                    self.autorisierung = ko.unwrap(settings.authorisierung);
                    self.trendConfig = ko.unwrap(settings.trendConfig);

                    self.signalNameBetriebswahl = self.modulname + ".R_dwBetriebswahl";
                    self.signalname = self.modulname + ".W_rAktuellerWert";

                    self.untergrenze = self.modulname + ".R_rUntergrenze";
                    self.obergrenze = self.modulname + ".R_rObergrenze";
                    self.festwert = self.modulname + ".R_rFestwert";
                    self.totzone = self.modulname + ".R_CFG_rTotzone";

                    self.connector = new signalsConnector();
                    self.dwZustand = self.connector.getSignal(self.modulname + ".W_dwZustand");
                    self.xhandbetrieb = self.connector.getSignal(self.modulname + ".W_xHanduebersteuert");
                    self.alarmmeldungen = self.connector.getSignal(self.modulname + ".W_wError");
                    self.dwBetriebswahl = self.connector.getSignal(self.modulname + ".R_dwBetriebswahl");
                   

                    self.alarme = self.alarmmeldungen.value;
                    self.betriebswahl = self.dwBetriebswahl.value;
                    self.zustand = self.dwZustand.value;
                    self.handbetrieb = self.xhandbetrieb.value;

                    self.symbol = ko.computed(function () {
                        return self.glyph + ' fa-3x';
                    });

                    self.status = ko.computed(function () {
                        self.css = self.symbol();
                        switch (self.zustand()) {
                            case 0:
                                return self.css + ' bs_initialisieren';
                            case 2:
                                return self.css + ' bs_fehler';
                            case 4:
                                return self.css + ' bs_passiv';
                            case 8:
                                return self.css + ' bs_aus';
                            case 16:
                                return self.css + ' bs_notbetrieb';
                            case 31:
                                return self.css + ' bs_fahrend';
                            case 32:
                                return self.css + ' bs_ein';
                            case 64:
                                return self.css + ' bs_ein';
                            case 128:
                                return self.css + ' bs_ein';
                            default:
                                return self.css + ' bs_aus';
                        }
                    });

                    self.hand = ko.computed(function () {
                        if (self.handbetrieb() != 0)
                            return 'visible';
                        else
                            return 'hidden';
                    });
                    return self.connector.getOnlineUpdates().fail(self.connector.handleError(self));
                },

                setAlarmQuit: function (value) {
                    var self = this;
                    var data = {};
                    var mask = 1 << value;
                    var betrValue = self.betriebswahl();
                    betrValue |= mask;
                    betrValue = betrValue >>> 0;
                    data[self.modulname + ".R_dwBetriebswahl"] = betrValue;
                    self.connector.writeSignals(data);
                },

                setBetriebswahl: function (value) {
                    var self = this;
                    var data = {};
                    data[self.signalNameBetriebswahl] = value;
                    self.connector.writeSignals(data);
                },

                getAlarmunterdr: function () {
                    var self = this;
                    var betrwahl = self.betriebswahl();
                    if ((betrwahl & 1073741824) == 1073741824) {
                        return true;
                    }
                    else {
                        return false;
                    }
                },

                changeBit: function (value) {
                    var self = this;
                    var data = {};
                    var mask = 1 << value;
                    var betrValue = self.betriebswahl();
                    betrValue ^= mask;
                    betrValue = betrValue >>> 0;
                    data[self.signalNameBetriebswahl] = betrValue;
                    self.connector.writeSignals(data);
                },

                openDialog: function () {
                    var self = this;
                    dialog.show(new ventilDialog(self)).then(function (dialogResult) {
                        console.log(dialogResult);
                    });
                },

                detached: function () {
                    var self = this;
                    return self.connector.unregisterSignals(
                        self.dwZustand,
                        self.xhandbetrieb,
                        self.alarmmeldungen,
                        self.dwBetriebswahl
                        );
                }
            }

        })
        return ventil;
    });