﻿/*-------------------------------------------------------------------------------------------------------------------*
 *                                            Ausgabefeld
 * ------------------------------------------------------------------------------------------------------------------
 * Erstellt am: 19. April 2017
 * Firma:       Bühler + Scherler
 * Ersteller:   B. Meier
 * Version:     1.0
 * ------------------------------------------------------------------------------------------------------------------
 * Widgetkonfiguration:
 * signalName: Name des zu verbindenden Signals, Bsp.: 'PLC1.IrgendwasAusGCPro.W_rAktuellerWert'
 * (typ: 'istwert' oder 'berechnetersollwert', istwert ist default und wird angenommen wenn nichts angegeben wird)
 *-------------------------------------------------------------------------------------------------------------------*/
define(
    [
        '../../services/connector'
    ],

    function (signalsConnector) {
        var ausgabefeld = function () {
            var self = this;
        };
        $(document).ready(function () {
            ausgabefeld.prototype = {
                activate: function (settings) {
                    var self = this;
                    self.connector = new signalsConnector();

                    self.modulname = ko.unwrap(settings.signalName);
                    self.type = ko.unwrap(settings.typ) || 'istwert';
                    self.einheit = ko.unwrap(settings.einheit) || " °C";
                    self.rAktuellerWert = self.connector.getSignal(self.modulname);
                    self.messwert = self.rAktuellerWert.value;
                    self.aktuellerwert = ko.computed(function () {
                        var result = (self.messwert() + " " + self.einheit);
                        return result;
                    })

                    self.feldtyp = ko.computed(function () {
                        if (self.type == 'istwert')
                            return 'bs-aktuellerwert';
                        if (self.type == 'berechnetersollwert')
                            return 'bs-bsw';
                        if (self.type == 'vav')
                            return 'bs-vav-ausgabefeld';
                        else if (self.type == 'sollwert')
                            return 'bs-sollwert'
                    })
                    return self.connector.getOnlineUpdates().fail(self.connector.handleError(self));
                },

                detached: function () {
                    var self = this;
                    return self.connector.unregisterSignals(
                        self.rAktuellerWert
                        );
                }
            };
        });
        return ausgabefeld;
    });