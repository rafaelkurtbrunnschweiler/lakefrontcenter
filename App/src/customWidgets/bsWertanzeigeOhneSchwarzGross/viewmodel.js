﻿/*
    Bühler+Scherler

    Wertanzeige
    -----------

    Zeigt einen Istwert und/oder einen Sollwert an.

    
    Eigenschaften in Klammern sind optional.

    Eigenschaft     Typ     Default     Beschreibung
    ---------------------------------------------------

    (signalNameIW)    String                Variable für den Istwert

    (signalNameSW)    String                Variable für den Sollwert

    (format)          String  '0,0.[00]'	Format für die numerische Anzeige. Weitere Formate sind auf der Dokumentationsseite von Numeral.js zu 
                                            finden: http://adamwdraper.github.io/Numeral-js/

    (isAlphanumeric)  Boolean false         Erlaubt die Darstellung eines alphanumerischen Wertes. Wenn diese Eigenschaft true ist, wird die 
                                            Eigenschaft format nicht berücksichtigt.

    (unitLabel)       Boolean false         Definiert ob die Signaleinheit angezeigt wird

    (staticUnitText)  String                Ersetzt die Signaleinheit durch den vorgegebene statischen String. Die unitLabel Property muss auf
                                            true gesetzt sein damit staticUnitText angezeigt wird

*/
define(
    [
        '../../services/connector',
        '../../services/securedService'
    ],
    function (
        signalsConnector,
        securedService
    ) {

        // constructor
        var Wertanzeige = function () {

        };

        Wertanzeige.prototype.activate = function (settings) {

            var self = this;

            self.connector = new signalsConnector();

            self.projectAuthorization = ko.unwrap(settings.projectAuthorization) || '';
            self.securedService = new securedService(self.projectAuthorization);
            self.hasAuthorization = self.securedService.hasAuthorization;

            self.settings = settings;
            self.format = ko.unwrap(settings.format) ? ko.unwrap(settings.format) : "0,0.[00]";
            self.isAlphanumeric = ko.unwrap(settings.isAlphanumeric) || false;
            self.signalNameIW = ko.unwrap(settings.signalNameIW) || null;
            self.signalNameSW = ko.unwrap(settings.signalNameSW) || null;
            self.unitLabel = ko.unwrap(self.settings.unitLabel) !== undefined ? ko.unwrap(self.settings.unitLabel) : false;
            self.staticUnitText = ko.unwrap(self.settings.staticUnitText) || null;

            // Stop here and return if no signalNameIW and no signalNameSW were configured.
            if (!self.signalNameIW && !self.signalNameSW) {
                throw new Error('missing widget parameters');
            }

            if (self.signalNameIW) {
                self.signalIW = self.connector.getSignal(ko.unwrap(self.signalNameIW));
                if (self.isAlphanumeric) {
                    self.signalValueIW = self.signalIW.value;
                } else {
                    self.signalValueIW = self.signalIW.value.extend({ numeralNumber: self.format });
                }
            }

            if (self.signalNameSW) {
                self.signalSW = self.connector.getSignal(ko.unwrap(self.signalNameSW));
                if (self.isAlphanumeric) {
                    self.signalValueSW = self.signalSW.value;
                } else {
                    self.signalValueSW = self.signalSW.value.extend({ numeralNumber: self.format });
                }
            }

            return self.connector.getOnlineUpdates().fail(self.connector.handleError(self));
        };

        Wertanzeige.prototype.detached = function () {

            var self = this;

            if (self.signalIW) {
                self.connector.unregisterSignals(self.signalIW);
            }
            if (self.signalSW) {
                self.connector.unregisterSignals(self.signalSW);
            }
        };
        return Wertanzeige;
    });