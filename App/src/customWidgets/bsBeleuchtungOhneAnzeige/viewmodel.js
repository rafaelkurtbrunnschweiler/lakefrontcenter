﻿/*-------------------------------------------------------------------------------------------------------------------*
 *                                            Beleuchtung
 *-------------------------------------------------------------------------------------------------------------------
 * Erstellt am: 28. August 2017
 * Firma:       Bühler + Scherler
 * Ersteller:   R. Brunnschweiler
 * Version:     1.0
 *-------------------------------------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------------------------------------*/
define(
    [
        '../../services/connector',
        '../../services/securedService',
    ],




    function (signalsConnector, securedService) {

        var bsBeschattung = function () {
            var self = this;
        };
        $(document).ready(function () {
            bsBeschattung.prototype = {
                activate: function (settings) {
                    var self = this;
                    self.connector = new signalsConnector();
                    // unwrapper
                    self.glyph = ko.unwrap(settings.symbol);
                    self.dialogtitle = ko.unwrap(settings.dialogtitle) || "Beleuchtung";
                    self.modulname = ko.unwrap(settings.signalName);
                    self.handLinks = ko.unwrap(settings.handLinks) || -10;
                    self.handOben = ko.unwrap(settings.handOben) || 10;
                    self.anzeigefeldLinks = ko.unwrap(settings.anzeigefeldLinks) || -62;
                    self.anzeigefeldOben = ko.unwrap(settings.anzeigefeldOben) || 10;
                    self.unit = ko.unwrap(settings.einheit) || '%';
                    self.autorisierung = ko.unwrap(settings.authorisierung);
                    self.trendConfig = ko.unwrap(settings.trendConfig);

                    self.signalname = self.modulname + ".W_rHelligkeit";

                    self.position = self.connector.getSignal(self.modulname + ".W_rHelligkeit");

                    self.positionValue = self.position.value;


                    self.symbol = ko.computed(function () {
                        return self.glyph + ' fa-3x';
                    });

                    //self.status = ko.computed(function () {
                    //    self.css = self.symbol();
                    //    switch (self.zustand()) {
                    //        case 0:
                    //            return self.css + ' bs_initialisieren';
                    //        case 2:
                    //            return self.css + ' bs_fehler';
                    //        case 4:
                    //            return self.css + ' bs_passiv';
                    //        case 8:
                    //            return self.css + ' bs_aus';
                    //        case 16:
                    //            return self.css + ' bs_notbetrieb';
                    //        case 31:
                    //            return self.css + ' bs_fahrend';
                    //        case 32:
                    //            return self.css + ' bs_ein';
                    //        case 64:
                    //            return self.css + ' bs_ein';
                    //        case 128:
                    //            return self.css + ' bs_aus';
                    //        case 256:
                    //            return self.css + ' bs_ein';
                    //        case 512:
                    //            return self.css + ' bs_ein';
                    //        default:
                    //            return self.css + ' bs_aus';
                    //    }
                    //});

                    self.status = ko.computed(function () {

                        self.css = self.symbol();
                        if (self.positionValue() > 0) {
                            return self.css + ' bs_beleuchtung_ein';
                        } else {
                            return self.css + ' bs_beleuchtung_aus';
                        }
                    });

                    return self.connector.getOnlineUpdates().fail(self.connector.handleError(self));
                },

                //setAlarmQuit: function (value) {
                //    var self = this;
                //    var data = {};
                //    var mask = 1 << value;
                //    var betrValue = self.betriebswahl();
                //    betrValue |= mask;
                //    betrValue = betrValue >>> 0;
                //    data[self.modulname + ".R_dwBetriebswahl"] = betrValue;
                //    self.connector.writeSignals(data);
                //},

                //setBetriebswahl: function (value) {
                //    var self = this;
                //    var data = {};
                //    data[self.signalNameZustand] = value;
                //    self.connector.writeSignals(data);
                //},

                //getAlarmunterdr: function () {
                //    var self = this;
                //    var betrwahl = self.betriebswahl();
                //    if ((betrwahl & 1073741824) == 1073741824) {
                //        return true;
                //    }
                //    else {
                //        return false;
                //    }
                //},

                //changeBit: function (value) {
                //    var self = this;
                //    var data = {};
                //    var mask = 1 << value;
                //    var betrValue = self.betriebswahl();
                //    betrValue ^= mask;
                //    betrValue = betrValue >>> 0;
                //    data[self.signalNameZustand] = betrValue;
                //    self.connector.writeSignals(data);
                //},

                //openDialog: function () {
                //    var self = this;
                //    dialog.show(new beschattungDialog(self)).then(function (dialogResult) { console.log(dialogResult); });

                //},

                detached: function () {
                    var self = this;
                    return self.connector.unregisterSignals(
                        self.dwZustand,
                        self.xhandbetrieb,
                        self.alarmmeldungen,
                        self.dwBetriebswahl,
                        self.position
                        );

                }
            }

        })
        return bsBeschattung;
    });