﻿/*
    Bühler+Scherler

    Anzeige Status
    -----------------------

    Zeigt ein Rechteck an, welches je nach Status die Farbe und den Text ändert.
    Mögliche Status sind 0=Aus, 1=Automatik, 2=Ein


    Eigenschaft             Typ     Default         Beschreibung
    --------------------------------------------------------------------------

    signalName              String                              Variable für den Status

*/
define(
    [
        '../../services/connector',
        '../../services/securedService'
    ],
    function (
        signalsConnector,
        securedService
    ) {

        // constructor
        var AnzeigeStatus = function () {

        };

        AnzeigeStatus.prototype.activate = function (settings) {

            var self = this;

            self.connector = new signalsConnector();

            self.projectAuthorization = ko.unwrap(settings.projectAuthorization) || '';
            self.securedService = new securedService(self.projectAuthorization);
            self.hasAuthorization = self.securedService.hasAuthorization;

            self.settings = settings;
            self.signalName = ko.unwrap(settings.signalName) || null;

            // Stop here and return if no signalName was configured
            if (!self.signalName ) {
                throw new Error('missing widget parameters');
            }

            self.signal = self.connector.getSignal(ko.unwrap(self.signalName));
            self.signalValue = self.signal.value;

            self.stateTexts = [
                'Inaktiv',
                'Aktiv'
            ];

            self.stateText = ko.computed(function () {

                var state = self.signalValue();

                if(state === 0) {
                    return self.stateTexts[0];
                }
                if (state !== 0){
                    return self.stateTexts[1];
                }
            });


            return self.connector.getOnlineUpdates().fail(self.connector.handleError(self));
        };

        AnzeigeStatus.prototype.detached = function () {

            var self = this;

            if (self.signal) {
                self.connector.unregisterSignals(self.signal);
            }
        };

        return AnzeigeStatus;
    });