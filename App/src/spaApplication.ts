﻿import system = require('durandal/system');
import Logger = require("./services/logger");
import app = require('durandal/app');
import viewLocator = require('durandal/viewLocator');
import router = require('plugins/router');
import PortableConsole = require("./services/portableConsole");
import PluginRegistry = require("./pluginRegistry");
import CustomComponentRegistry = require("./customComponentRegistry");
import CustomWidgetRegistry = require("./customWidgetRegistry");

export class SpaApplication {
    constructor() {
        this.setupLogging();
        Logger.info(this, 'Application starting...');
        Logger.info(this, "Framework version " + window.appVersion);
        if (!this.checkProtocol()) {
            Logger.error(this, "Application was started outside the webserver. Please publish the application and run it from the server.");
            return;
        }

        this.setupAsyncSupport();
        this.setupAppTitle();
        this.configurePlugins();
        this.registerComponents();
        CustomComponentRegistry.register();

        Q.all<any>([])
            .then(() => {
                return app.start().then(() => {
                    Logger.info(this, "Application started");
                    viewLocator.useConvention();
                    return this.loadShell();
                });
            })
            .fail(Logger.handleError(this))
            .done();
    }

    private setupLogging() {
        //system.debug(window.debug);
        //if (window.debug) {
        var dynamicSystem: any = system;
        dynamicSystem.log = PortableConsole.info;
        dynamicSystem.warn = PortableConsole.warn;
        dynamicSystem.error = PortableConsole.error;
        Q.longStackSupport = true;
        //}

        toastr.options.toastClass = 'toast'; //hidden-xs
        toastr.options.positionClass = 'toast-bottom-right';
        toastr.options.backgroundpositionClass = 'toast-bottom-right';

        Logger.info(this, "Logging support enabled: {0}", window.debug);
    }

    private checkProtocol(): boolean {
        Logger.info(this, 'Check used Protocol');
        switch (window.location.protocol) {
            case 'http:':
            case 'https:':
                return true;
            case 'file:':
                return false;
            default:
                return false;
        }
    }

    private setupAsyncSupport() {
        Logger.info(this, "Setting up support for async operations");

        // we need to patch in the Q promises instead of jQuery
        // unfortunately this doesn't work directly with TS so we need to hack it
        var dynamicSystem: any = system;
        dynamicSystem.defer = (action: any) => {
            var deferred: any = Q.defer();
            action.call(deferred, deferred);
            var promise = deferred.promise;
            deferred.promise = () => promise;

            return deferred;
        };

        Q.longStackSupport = true;
    }

    private setupAppTitle() {
        app.title = null;
        router.updateDocumentTitle = () => {
        };
    }

    private configurePlugins() {
        Logger.info(this, "Configuring framework plugins");
        var standardWidgets = this.registerStandardWidgets();
        var customWidgets = CustomWidgetRegistry.register();

        var allWidgets = standardWidgets.concat(customWidgets);

        app.configurePlugins({
            router: true,
            dialog: true,
            widget: {
                kinds: allWidgets
            }
        });

        var dynRouter: any = router;
        dynRouter.handleInvalidRoute = (route: string) => {
            Logger.error(router, "Route '{0}' not found", null, route);
        };
    }

    private registerStandardWidgets() {
        return new PluginRegistry("src/widgets")
            .map("wfValue")
            .map("wfValueDisplay")
            .map("wfValueBarGraph")
            .map("wfValueGauge")
            .map("wfValueArc")
            .map("wfSignalInformation")
            .map("wfInputValue")
            .map("wfSwitchValue")
            .map("wfSwitchValue3States")
            .map("wfWriteValueButton")
            .map("wfWriteValueCombobox")
            .map("wfStateIndicator")
            .map("wfStateCssClass")
            .map("wfSensorValue")
            .map("wfStateText")
            .map("wfStateDisplay")
            .map("wfLanguageDropdown")
            .map("wfSymbolicText")
            .map("wfLogTagTrend")
            .map("wfLogTagTable")
            .map("wfUserLogin")
            .map("wfUserAuthorizationsList")
            .map("wfAckAlarmButton")
            .map("wfAlarmViewer")
            .map("tmplAlarmList")
            .map("tmplAlarmTable")
            .map("wfSecuredContainer")
            .map("wfSlider")
            .map("wfWatchdog")
            .map("demoSignalWriteButtonGroup2")
            .map("demoProductionUnitInformation")
            .map("demoLightControl")
            .map("demoWidgetsList")
            .map("wfLogbook")
            .map("wfUserInformation")
            .map("wfLogbookViewer")
            .map("wfDateTimePicker")
            .map("wfMeter")
            .map("wfLogTagAnalytics")
            .map("wfVisibilityContainer")
            .map("wfEnableContainer")
            .map("wfBufferButton")
            .map("wfToggleButton")
            .map("wfSignalsBufferViewer")
            .map("wfRadioButtons")
            .kinds;

        //.map("demoAlarmList")
        //.map("demoAlarmTable")
    }

    private registerComponents() {
        return new PluginRegistry("src/components")
            .mapComponent("wf-secured-container")
            .mapComponent("wf-value")
            .mapComponent("wf-value-display")
            .mapComponent("wf-gauge-1")
            .mapComponent("wf-arc")
            .mapComponent("wf-chart-1")
            .mapComponent("wf-log-table")
            .mapComponent("wf-bargraph")
            .mapComponent("wf-signal-information")
            .mapComponent("wf-state-text")
            .mapComponent("wf-state-indicator")
            .mapComponent("wf-state-symbol")
            .mapComponent("wf-input")
            .mapComponent("wf-button")
            .mapComponent("wf-switch")
            .mapComponent("wf-combobox")
            .mapComponent("wf-slider")
            .mapComponent("wf-symbolictext")
            .mapComponent("wf-language-selector")
            .mapComponent("wf-user-login")
            .mapComponent("wf-alarm-viewer")
            .mapComponent("wf-watchdog")
            .mapComponent("wf-alarm-ack-button")
            .mapComponent("wf-logbook")
            .mapComponent("wf-sensor")
            .mapComponent("wf-user-information")
            .mapComponent("wf-switch-3-states")
            .mapComponent("wf-logbook-viewer")
            .mapComponent("wf-signal-list")
            .mapComponent("wf-configuration")
            .mapComponent("wf-signal-browser")
            .mapComponent("wf-meter")
            .mapComponent("wf-logtag-analytics")
            .mapComponent("wf-visibility-container")
            .mapComponent("wf-enable-container")
            .mapComponent("wf-buffer-button")
            .mapComponent("wf-toggle-button")
            .mapComponent("wf-signals-buffer-viewer")
            .mapComponent("wf-rotation-container")
            .mapComponent("wf-write-secure-signal")
            .mapComponent("wf-radio-buttons");
    }

    private loadShell() {
        Logger.info(this, "Starting shell");
        app.setRoot('src/viewModels/shell', 'entrance');
    }
}

export var application = new SpaApplication();