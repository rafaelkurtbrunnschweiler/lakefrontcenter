define(["require", "exports", "durandal/composition"], function (require, exports, Composition) {
    "use strict";
    var ESmartZoom = /** @class */ (function () {
        function ESmartZoom() {
        }
        ESmartZoom.init = function () {
            Composition.addBindingHandler('esmartzoom');
        };
        return ESmartZoom;
    }());
    return ESmartZoom;
});
//# sourceMappingURL=eSmartZoom.js.map